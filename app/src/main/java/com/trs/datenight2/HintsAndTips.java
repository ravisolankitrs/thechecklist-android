package com.trs.datenight2;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Sandeep on 1/24/2018.
 */
public class HintsAndTips extends Activity {
    public static final String BOTTOMNAV = "Bottom_Nav";
    static String POST = "posts";
    static String POST_ID = "id";
    static String POST_TITLE = "title";
    static String POST_CONTENT = "content";
    static String POST_URL = "url";
    static String THUMBNAIL_IMAGE = "thumbnail_images";
    static String THUMBNAIL_MEDIUM = "medium_large";
    static String THUMBNAIL_URL = "thumbnail_url";
    View view;
    RecyclerView listView;
    HintsAndTipsAdapter adapter;
    ArrayList<HashMap<String, String>> arraylist;
    ProgressDialog progress;
    SharedPreference_main sharedPreference_main;
    ImageLoader imageLoader;
    Bundle bundle;
    FirebaseAnalytics firebaseAnalytics;
    GridLayoutManager layoutManager;
    TextView title;

    TextView home_text, find_text, nearby_text, save_text, more_text;
    RelativeLayout home_lay, find_lay, nearby_lay, save_lay, more_lay;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hints_and_tips);
        bundle=new Bundle();
        firebaseAnalytics=FirebaseAnalytics.getInstance(this);
        sharedPreference_main = new SharedPreference_main(getApplicationContext());
        init();
        onclick();
        imageLoader = new ImageLoader(getApplicationContext());
        // Create a grid layout with two columns
        layoutManager = new GridLayoutManager(getApplicationContext(), 2);
        // Create a custom SpanSizeLookup where the first item spans both columns
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return position == 0 ? 2 : 1;
            }
        });

        Typeface hanken_light = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Hanken-Light.ttf");
        title.setTypeface(hanken_light);

        home_text.setTypeface(hanken_light);
        find_text.setTypeface(hanken_light);
        nearby_text.setTypeface(hanken_light);
        save_text.setTypeface(hanken_light);
        more_text.setTypeface(hanken_light);

        new DownloadJSON().execute();
    }


    private void init() {
        title = (TextView) findViewById(R.id.title);
        listView = (RecyclerView) findViewById(R.id.list_view);

        home_text = (TextView) findViewById(R.id.home_text);
        find_text = (TextView) findViewById(R.id.find_text);
        nearby_text = (TextView) findViewById(R.id.nearby_text);
        save_text = (TextView) findViewById(R.id.save_text);
        more_text = (TextView) findViewById(R.id.more_text);

        home_lay = (RelativeLayout) findViewById(R.id.home_lay);
        find_lay = (RelativeLayout) findViewById(R.id.find_lay);
        nearby_lay = (RelativeLayout) findViewById(R.id.nearby_lay);
        save_lay = (RelativeLayout) findViewById(R.id.save_lay);
        more_lay = (RelativeLayout) findViewById(R.id.more_lay);


        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    private void onclick() {

        home_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle.putString("Home"," ");
                firebaseAnalytics.logEvent(BOTTOMNAV,bundle);
                firebaseAnalytics.setUserProperty(BOTTOMNAV, "Home");
                startActivity(new Intent(getApplicationContext(), Home.class));
            }
        });

        find_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle.putString("The_List"," ");
                firebaseAnalytics.logEvent(BOTTOMNAV,bundle);
                firebaseAnalytics.setUserProperty(BOTTOMNAV, "The_List");
                startActivity(new Intent(getApplicationContext(), Events.class));
            }
        });


        nearby_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle.putString("NearBy"," ");
                firebaseAnalytics.logEvent(BOTTOMNAV,bundle);
                firebaseAnalytics.setUserProperty(BOTTOMNAV, "NearBy");
                startActivity(new Intent(getApplicationContext(), Nearby.class));
            }
        });

        save_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle.putString("Save"," ");
                firebaseAnalytics.logEvent(BOTTOMNAV,bundle);
                firebaseAnalytics.setUserProperty(BOTTOMNAV, "Save");
                startActivity(new Intent(getApplicationContext(), SavedDate.class));
            }
        });

        more_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle.putString("More"," ");
                firebaseAnalytics.logEvent(BOTTOMNAV,bundle);
                firebaseAnalytics.setUserProperty(BOTTOMNAV, "More");
                startActivity(new Intent(getApplicationContext(), More.class));
            }
        });

    }

    private void progressBar() {
        progress = new ProgressDialog(this);
        progress.setMessage("Loading...");
        progress.setIndeterminate(true);
        progress.setCanceledOnTouchOutside(false);
        progress.show();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(), Home.class));
    }

    // DownloadJSON AsyncTask
    private class DownloadJSON extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar();
        }

        @Override
        protected Void doInBackground(Void... params) {
            arraylist = new ArrayList<HashMap<String, String>>();
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url("http://datenight.london/rapi/get_recent_posts/")
                    .addHeader("cache-control", "no-cache")
                    .build();

            try {
                Response response = client.newCall(request).execute();
                Log.d("responstr", String.valueOf(response.code()));
                String fulljson = response.body().string();
                Log.d("body_hints", fulljson);

                JSONObject JSONObject = new JSONObject(fulljson);
                JSONArray jsonArray = JSONObject.getJSONArray(POST);
                for (int i = 0; i < jsonArray.length(); i++) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    map.put(POST_ID, jsonObject1.getString(POST_ID));
                    map.put(POST_TITLE, jsonObject1.getString(POST_TITLE));
                    map.put(POST_CONTENT, jsonObject1.getString(POST_CONTENT));
                    map.put(POST_URL, jsonObject1.getString(POST_URL));
                    JSONObject jsonObject2 = jsonObject1.getJSONObject(THUMBNAIL_IMAGE);
                    JSONObject jsonObject3 = jsonObject2.getJSONObject(THUMBNAIL_MEDIUM);
                    map.put(THUMBNAIL_URL, jsonObject3.getString("url"));
                    arraylist.add(map);
                }
                Log.d("array_list", arraylist.toString());

            } catch (JSONException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getApplicationContext(), R.dimen.item_offset);
            listView.addItemDecoration(itemDecoration);
            adapter = new HintsAndTipsAdapter(getApplicationContext(), arraylist);
            listView.setLayoutManager(layoutManager);
            listView.setAdapter(adapter);
            progress.dismiss();
        }
    }
}

