package com.trs.datenight2;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.github.ksoichiro.android.observablescrollview.ObservableListView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.nineoldandroids.view.ViewHelper;
import com.nineoldandroids.view.ViewPropertyAnimator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by trs on 06-Jul-18.
 */

public class Nearby extends Activity implements ObservableScrollViewCallbacks, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {


    MapFragment mMapFragment;
    TextView title;
    public static final String BOTTOMNAV = "Bottom_Nav";
    TextView home_text, find_text, nearby_text, save_text, more_text;
    RelativeLayout home_lay, find_lay, nearby_lay, save_lay, more_lay;
    String latitued = "", longitude = "";
    Bundle bundle;
    Bundle bundle1;
    GoogleMap map;
    public GoogleApiClient mGoogleApiClient;
    public final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    Location location;
    public LocationRequest mLocationRequest;
    public double currentLatitude;
    public double currentLongitude;
    PendingResult<LocationSettingsResult> result;
    final static int REQUEST_LOCATION = 199;
    ListView view_list;
    private NearbyAdapter mNearbyAdapter;
    SharedPreference_main sharedPreference_main;
    ArrayList<HashMap<String, String>> arraylist;
    Marker marker1;
    String name;
    public static final String RESPONSECODE = "ResponseCode";
    public static final String RESPONSEBODY = "ResponseBody";
    public static final String NEARBY = "Nearby";
    public static final String ID = "id";
    public static String NAME = "name";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String ADDRESS = "address";
    public static final String MAINIMAGE = "mainimage";
    public static final String POSTCODE = "postcode";
    List<LatLng> myTagIDLat_long;
    List<String> myTagIDList;
    private ArrayList<String> TagCaptionList;
    private ArrayList<LatLng> listLatLng;

    Database database;
    ProgressDialog progress;
    FirebaseAnalytics firebaseAnalytics;
    TextView no_data;
    EventsAdapter adapter;
    RelativeLayout cardview_map;
    SQLiteDatabase db, db1;
    Cursor c, c1, c2;
    String val;
    ArrayList<String> stringArrayList;
    SearchListviewAdapter searchListviewAdapter;
    ListView Search_listview;
    ObservableListView listviewitems;
    Button Cross, search_icon;
    LinearLayout search_layout;
    RelativeLayout headerID;
    private int mBaseTranslationY;

    private ProgressDialog mProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nearby_activity);

        init();
        onclick();
        sharedPreference_main = new SharedPreference_main(getApplication());
        arraylist = new ArrayList<HashMap<String, String>>();
        stringArrayList = new ArrayList<String>();
        firebaseAnalytics=FirebaseAnalytics.getInstance(this);

        Typeface hanken_light = Typeface.createFromAsset(getApplication().getAssets(), "fonts/Hanken-Light.ttf");
        title.setTypeface(hanken_light);
        home_text.setTypeface(hanken_light);
        find_text.setTypeface(hanken_light);
        nearby_text.setTypeface(hanken_light);
        save_text.setTypeface(hanken_light);
        more_text.setTypeface(hanken_light);
        // stop opening keyboard in search bar aand hide the tablayout below keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        bundle = getIntent().getExtras();
        bundle1 = new Bundle();
        //latitued = bundle.getString(Events.LOCATION_LATITUDE);
        // longitude = bundle.getString(Events.LOCATION_LONGITUDE);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();


        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(30 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(5 * 1000);
        if (ActivityCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }

        myTagIDList = new ArrayList<>();
        myTagIDLat_long = new ArrayList<>();
        TagCaptionList = new ArrayList<>();
        listLatLng = new ArrayList<LatLng>();

        listviewitems.setScrollViewCallbacks(Nearby.this);

        ViewCompat.setElevation(headerID, getResources().getDimension(R.dimen.toolbar_elevation));

        LayoutInflater inflater = LayoutInflater.from(Nearby.this);
        //  listviewitems.addFooterView(inflater.inflate(R.layout.padding, listviewitems, false)); // toolbar

        searchAutocomplete();
    }

    // google search autocomplete and get latitude and longitude
    public void searchAutocomplete() {
        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.

                //get latitude and longitude from place object
                LatLng latLng = place.getLatLng();

                showProgressDialog();
                loadNearbyRestaurants(3, latLng.latitude, latLng.longitude);
                location(10, latLng.latitude, latLng.longitude);


            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.

            }
        });
    }

    private void init() {
        headerID = (RelativeLayout) findViewById(R.id.search_layout);
        search_layout = (LinearLayout) findViewById(R.id.listlay);
        listviewitems = (ObservableListView) findViewById(R.id.listviewitems);
        //   Cross = (Button) findViewById(R.id.button);
        // Search_listview = (ListView) findViewById(R.id.Search_listview);
        database = new Database(getApplicationContext());
        no_data = (TextView) findViewById(R.id.no_data);
        view_list = (ListView) findViewById(R.id.view_list);
        title = (TextView) findViewById(R.id.title);
        // simpleSearchView = (EditText) findViewById(R.id.edittext);
        cardview_map = (RelativeLayout) findViewById(R.id.cardview_map);

        home_text = (TextView) findViewById(R.id.home_text);
        find_text = (TextView) findViewById(R.id.find_text);
        nearby_text = (TextView) findViewById(R.id.nearby_text);
        save_text = (TextView) findViewById(R.id.save_text);
        more_text = (TextView) findViewById(R.id.more_text);

        home_lay = (RelativeLayout) findViewById(R.id.home_lay);
        find_lay = (RelativeLayout) findViewById(R.id.find_lay);
        nearby_lay = (RelativeLayout) findViewById(R.id.nearby_lay);
        save_lay = (RelativeLayout) findViewById(R.id.save_lay);
        more_lay = (RelativeLayout) findViewById(R.id.more_lay);


        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }

        mMapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.mapFragment);
        mMapFragment.getMapAsync(this);

    }


    private void onclick() {


        home_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle1.putString("Home", " ");
                firebaseAnalytics.logEvent(BOTTOMNAV,bundle1);
                firebaseAnalytics.setUserProperty(BOTTOMNAV, "Home");
                startActivity(new Intent(getApplicationContext(), Home.class));
            }
        });

        find_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle1.putString("The_List", " ");
                firebaseAnalytics.logEvent(BOTTOMNAV,bundle1);
                firebaseAnalytics.setUserProperty(BOTTOMNAV, "The_List");
                startActivity(new Intent(getApplicationContext(), Events.class));
            }
        });

        nearby_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        save_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle1.putString("Save", " ");
                firebaseAnalytics.logEvent(BOTTOMNAV,bundle1);
                firebaseAnalytics.setUserProperty(BOTTOMNAV, "Save");
                startActivity(new Intent(getApplicationContext(), SavedDate.class));
            }
        });

        more_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle1.putString("More", " ");
                firebaseAnalytics.logEvent(BOTTOMNAV,bundle1);
                firebaseAnalytics.setUserProperty(BOTTOMNAV, "More");
                startActivity(new Intent(getApplicationContext(), More.class));
            }
        });

    }


    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        if (dragging) {
            int toolbarHeight = search_layout.getHeight();
            if (firstScroll) {
                float currentHeaderTranslationY = ViewHelper.getTranslationY(headerID);
                if (-toolbarHeight < currentHeaderTranslationY) {
                    mBaseTranslationY = scrollY;
                }
            }
            float headerTranslationY = ScrollUtils.getFloat(-(scrollY - mBaseTranslationY), -toolbarHeight, 0);
            ViewPropertyAnimator.animate(headerID).cancel();
            ViewHelper.setTranslationY(headerID, headerTranslationY);
        }
    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
        mBaseTranslationY = 0;

        if (scrollState == ScrollState.DOWN) {
            showToolbar();
        } else if (scrollState == ScrollState.UP) {
            int toolbarHeight = search_layout.getHeight();
            int scrollY = listviewitems.getCurrentScrollY();
            if (toolbarHeight <= scrollY) {
                hideToolbar();
            } else {
                showToolbar();
            }
        } else {
            // Even if onScrollChanged occurs without scrollY changing, toolbar should be adjusted
            if (!toolbarIsShown() && !toolbarIsHidden()) {
                // Toolbar is moving but doesn't know which to move:
                // you can change this to hideToolbar()
                showToolbar();
            }
        }
    }

    private boolean toolbarIsShown() {
        return ViewHelper.getTranslationY(headerID) == 0;
    }

    private boolean toolbarIsHidden() {
        return ViewHelper.getTranslationY(headerID) == -search_layout.getHeight();
    }

    private void showToolbar() {
        float headerTranslationY = ViewHelper.getTranslationY(headerID);
        if (headerTranslationY != 0) {
            ViewPropertyAnimator.animate(headerID).cancel();
            ViewPropertyAnimator.animate(headerID).translationY(0).setDuration(200).start();
        }
    }

    private void hideToolbar() {
        float headerTranslationY = ViewHelper.getTranslationY(headerID);
        int toolbarHeight = search_layout.getHeight();
        if (headerTranslationY != -toolbarHeight) {
            ViewPropertyAnimator.animate(headerID).cancel();
            ViewPropertyAnimator.animate(headerID).translationY(-toolbarHeight).setDuration(200).start();
        }
    }


    private void progressBar() {
        progress = new ProgressDialog(this);
        progress.setMessage("Loading...");
        progress.setIndeterminate(true);
        progress.setCanceledOnTouchOutside(false);
        progress.show();
    }

    public void location(double radius, final double latitude, final double longitude) {
        String URL = "http://datenight.london/wp-json/api/v1/listing/userNearbyLocation";
        arraylist.clear();

        //   restaurant = new ArrayList<>();
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("radius", radius);
            jsonParams.put("latitude", latitude);//51.4927325
            jsonParams.put("longitude", longitude);//-0.1573708

        } catch (JSONException e) {
            e.printStackTrace();
        }


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URL, jsonParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            final JSONObject jsonObject = new JSONObject(String.valueOf(response));
                            String ResponseCode = jsonObject.getString(RESPONSECODE);
                            if (ResponseCode.equals("200")) {
                                JSONObject object = jsonObject.getJSONObject(RESPONSEBODY);
                                JSONArray restArray = object.getJSONArray(NEARBY);
                                if (restArray.length() != 0) {
                                    for (int i = 0; i < restArray.length(); i++) {
                                        JSONObject restObject = restArray.getJSONObject(i);
                                        String id = restObject.getString(ID);
                                        String name = restObject.getString(NAME);
                                        String latitude = restObject.getString(LATITUDE);
                                        String longitude = restObject.getString(LONGITUDE);
                                        String address = restObject.getString(ADDRESS);
                                        String mainimage = restObject.getString(MAINIMAGE);
                                        String postcode = restObject.getString(POSTCODE);

                                        HashMap<String, String> map = new HashMap<>();
                                        // adding each child node to HashMap key => value
                                        map.put(ID, id);
                                        map.put(NAME, name);
                                        map.put(LATITUDE, latitude);
                                        map.put(LONGITUDE, longitude);
                                        map.put(ADDRESS, address);

                                        if (restObject.has(MAINIMAGE)) {
                                            map.put(MAINIMAGE, mainimage);
                                        } else {
                                            map.put(MAINIMAGE, "");
                                        }

                                        map.put(POSTCODE, postcode);

                                        Location loc1 = new Location("Location A");
                                        loc1.setLatitude(currentLatitude);// current latitude
                                        loc1.setLongitude(currentLongitude);//current  Longitude

                                        Location loc2 = new Location("Location B");
                                        loc2.setLatitude(Double.parseDouble(latitude));
                                        loc2.setLongitude(Double.parseDouble(longitude));

                                        //  float s = loc1.distanceTo(loc2);
                                        int metres = Math.round(loc1.distanceTo(loc2));
                                        //     int km = metres / 0.001;
                                        DecimalFormat decimalFormat = new DecimalFormat("#.##");
                                        double miles = (metres) * 0.000621;
                                        String distance = decimalFormat.format(miles);
                                        double distmiles = Double.parseDouble(distance);

                                        map.put("radius", String.valueOf(distmiles));

                                        arraylist.add(map);
                                    }
                                }
                                view_list.setVisibility(View.VISIBLE);
                                NearbyAdapter adapter = new NearbyAdapter(getApplicationContext(), arraylist);

                                view_list.setAdapter(adapter);
                                cardview_map.setVisibility(View.VISIBLE);
                                //   progress.dismiss();
                                //SetZoomlevel(listLatLng, 40);
                                hideProgressDialog();

                            } else {
                                Toast.makeText(getApplicationContext(), "No Data Found", Toast.LENGTH_SHORT).show();
                                // Toast.makeText(getActivity(), "No Data Found", Toast.LENGTH_SHORT).show();
                                // mCustomProgressBar.showHideProgressBar(false, null);
                                view_list.setVisibility(View.GONE);
                                /*cardview_map.setVisibility(View.VISIBLE);
                                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 10);
                                map.animateCamera(cameraUpdate);*/
                                hideProgressDialog();


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();

                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("User-agent", System.getProperty("http.agent"));
                return headers;
            }
        };

        requestQueue.add(jsonObjectRequest);

    }


    private void loadNearbyRestaurants(int radius, final double latitude, final double longitude) {
        //  listLatLng.clear();
        // myTagIDLat_long.clear();
        // TagCaptionList.clear();
        //map.clear();
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JSONObject jsonParams = new JSONObject();
        String URL = "http://datenight.london/wp-json/api/v1/listing/userNearbyLocation";

        try {
            jsonParams.put("radius", String.valueOf(radius));
            jsonParams.put("latitude", latitude);//51.4927325
            jsonParams.put("longitude", longitude);//-0.1573708


        } catch (JSONException e) {
            e.printStackTrace();
        }


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URL, jsonParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            final JSONObject jsonObject = new JSONObject(String.valueOf(response));
                            String ResponseCode = jsonObject.getString(RESPONSECODE);
                            if (ResponseCode.equals("200")) {
                                JSONObject object = jsonObject.getJSONObject(RESPONSEBODY);
                                JSONArray restArray = object.getJSONArray(NEARBY);
                                if (restArray.length() != 0) {

                                    for (int i = 0; i < restArray.length(); i++) {
                                        JSONObject restObject = restArray.getJSONObject(i);

                                        String name = restObject.getString(NAME);
                                        String latitude = restObject.getString(LATITUDE);
                                        String longitude = restObject.getString(LONGITUDE);
                                        double latitu = Double.parseDouble((latitude));
                                        double longitu = Double.parseDouble(longitude);
                                        LatLng latLng = new LatLng(latitu, longitu);

                                        myTagIDLat_long.add(latLng);
                                        listLatLng.add(latLng);
                                        TagCaptionList.add(name);
                                        setMyMarker(listLatLng.get(i), i);
                                        //  mCustomProgressBar.showHideProgressBar(false, null);
                                    }

                                }
                                SetZoomlevel(listLatLng, 40);


                            } else {
                                cardview_map.setVisibility(View.VISIBLE);
                                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 10);
                                map.animateCamera(cameraUpdate);

                                hideProgressDialog();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();

                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgressDialog();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("User-agent", System.getProperty("http.agent"));
                return headers;
            }
        };
        requestQueue.add(jsonObjectRequest);
    }

    private void setMyMarker(LatLng location, int i) {
        MarkerOptions marker = new MarkerOptions().position(location).title(name);
        marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.rsz_pin));
        marker1 = map.addMarker(marker);
        //   mapHashMap.put(marker1, i);
    }

    public void SetZoomlevel(final ArrayList<LatLng> listLatLng, int Padding) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (int i = 0; i < listLatLng.size(); i++) {
            builder.include(listLatLng.get(i));
        }

        LatLngBounds bounds = builder.build();
        final CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, Padding);
        map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                map.animateCamera(cu);
            }
        });
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
//        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            //   mGoogleApiClient.disconnect();

        }
    }


    @Override
    public void onConnected(Bundle bundle) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions((this), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_LOCATION);
                return;
            }
        }
        location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

        } else {
            //If everything went fine lets get latitude and longitude
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();

            // Toast.makeText(getActivity(), "Latitude " + currentLatitude + " & " + "Longitude " + currentLongitude + "", Toast.LENGTH_LONG).show();
        }

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //...
                        showProgressDialog();
                        loadNearbyRestaurants(3, currentLatitude, currentLongitude);

                        location(10, currentLatitude, currentLongitude);

                        // relativelayout.setVisibility(View.VISIBLE); location(mUserId,currentLatitude,currentLongitude);itude,currentLongitude);
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Fragment fragment = getFragmentManager().findFragmentById(R.id.COMMON_fragment);
                            //  fragment.onActivityResult(REQUEST_LOCATION, 0, null);
                            //  startIntentSenderForResult(status.getResolution().getIntentSender(), REQUEST_LOCATION, null, 0, 0, 0, null);
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(Nearby.this, REQUEST_LOCATION);
                        } catch (Exception e) {
                            // Ignore the error.
                        }

                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //     super.onActivityResult(requestCode, resultCode, data);
        // requestCode=0;
        //final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        if (!mGoogleApiClient.isConnecting() && !mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
        switch (requestCode) {
            case REQUEST_LOCATION:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }

                        location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                        //  Utilities.showInformativeDialog(mAppName, "Location enabled.", mContext);
                        showProgressDialog();

                        loadNearbyRestaurants(3, currentLatitude, currentLongitude);
                        location(10, currentLatitude, currentLongitude);

                        // Toast.makeText(this, sharedPreference_main.getlatitude() + " & " + sharedPreference_main.getlongitude(), Toast.LENGTH_LONG).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(this, "Location not enabled, user cancelled.", Toast.LENGTH_LONG).show();

                        break;
                    default:
                        break;

                }
                break;
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {

            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onLocationChanged(Location location) {

        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.getUiSettings().setMyLocationButtonEnabled(false);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            map.setMyLocationEnabled(true);
            return;
        }

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();

        String provider = locationManager.getBestProvider(criteria, true);
        Location location = locationManager.getLastKnownLocation(provider);

        if (location != null) {
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();
        } else {


        }

        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(currentLatitude, currentLongitude), 10);
        map.animateCamera(cameraUpdate);
        map.getUiSettings().setAllGesturesEnabled(false);
        map.getUiSettings().setScrollGesturesEnabled(true);
        // map.moveCamera(CameraUpdateFactory.newLatLng(current));
    }
}
