package com.trs.datenight2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by Sandeep on 3/5/2018.
 */
public class PrivacyPolicy extends Activity {

    TextView text1;
    RelativeLayout back;
    TextView home_text, find_text, nearby_text, save_text, more_text;
    RelativeLayout home_lay, find_lay, nearby_lay, save_lay, more_lay;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.privacy_policy);
        init();
        onclick();

        text1.setText("Privacy Policy  \n" +
                "==============\n" +
                "\n" +
                "Last updated: February 12, 2018\n" +
                "\n" +
                "Christopher James Digital Ltd (\"us\", \"we\", or \"our\") operates the\n" +
                "http://datenight.london/ website and the `DateNight mobile application (the\n" +
                "\"Service\").\n" +
                "\n" +
                "This page informs you of our policies regarding the collection, use and\n" +
                "disclosure of Personal Information when you use our Service.\n" +
                "\n" +
                "We will not use or share your information with anyone except as described in\n" +
                "this Privacy Policy.\n" +
                "\n" +
                "We use your Personal Information for providing and improving the Service. By\n" +
                "using the Service, you agree to the collection and use of information in\n" +
                "accordance with this policy. Unless otherwise defined in this Privacy Policy,\n" +
                "terms used in this Privacy Policy have the same meanings as in our Terms and\n" +
                "Conditions.\n" +
                "\n" +
                "Information Collection And Use  \n" +
                "------------------------------\n" +
                "\n" +
                "While using our Service, we may ask you to provide us with certain personally\n" +
                "identifiable information that can be used to contact or identify you.\n" +
                "Personally identifiable information may include, but is not limited to, your\n" +
                "email address, name (\"Personal Information\").\n" +
                "\n" +
                "Log Data  \n" +
                "--------\n" +
                "\n" +
                "We may also collect information that your browser sends whenever you visit our\n" +
                "Service or when you access the Service by or through a mobile device (\"Log\n" +
                "Data\").\n" +
                "\n" +
                "This Log Data may include information such as your computer's Internet\n" +
                "Protocol (\"IP\") address, browser type, browser version, the pages of our\n" +
                "Service that you visit, the time and date of your visit, the time spent on\n" +
                "those pages and other statistics.\n" +
                "\n" +
                "When you access the Service by or through a mobile device, this Log Data may\n" +
                "include information such as the type of mobile device you use, your mobile\n" +
                "device unique ID, the IP address of your mobile device, your mobile operating\n" +
                "system, the type of mobile Internet browser you use and other statistics.\n" +
                "\n" +
                "In addition, we may use third party services such as Google Analytics that\n" +
                "collect, monitor and analyze this type of information in order to increase our\n" +
                "Service's functionality. These third party service providers have their own\n" +
                "privacy policies addressing how they use such information.\n" +
                "\n" +
                "Location Information  \n" +
                "--------------------\n" +
                "\n" +
                "We may use and store information about your location, if you give us\n" +
                "permission to do so. We use this information to provide features of our\n" +
                "Service, to improve and customize our Service. You can enable or disable\n" +
                "location services when you use our Service at anytime, through your mobile\n" +
                "device settings.\n" +
                "\n" +
                "Cookies  \n" +
                "-------\n" +
                "\n" +
                "Cookies are files with small amount of data, which may include an anonymous\n" +
                "unique identifier. Cookies are sent to your browser from a web site and stored\n" +
                "on your computer's hard drive.\n" +
                "\n" +
                "We use \"cookies\" to collect information. You can instruct your browser to\n" +
                "refuse all cookies or to indicate when a cookie is being sent. However, if you\n" +
                "do not accept cookies, you may not be able to use some portions of our\n" +
                "Service.\n" +
                "\n" +
                "DoubleClick Cookie  \n" +
                "------------------\n" +
                "\n" +
                "Google, as a third party vendor, uses cookies to serve ads on our Service.\n" +
                "Google's use of the DoubleClick cookie enables it and its partners to serve\n" +
                "ads to our users based on their visit to our Service or other web sites on the\n" +
                "Internet.\n" +
                "\n" +
                "You may opt out of the use of the DoubleClick Cookie for interest-based\n" +
                "advertising by visiting the Google Ads Settings web page:\n" +
                "<http://www.google.com/ads/preferences/>\n" +
                "\n" +
                "Service Providers  \n" +
                "-----------------\n" +
                "\n" +
                "We may employ third party companies and individuals to facilitate our Service,\n" +
                "to provide the Service on our behalf, to perform Service-related services or\n" +
                "to assist us in analyzing how our Service is used.\n" +
                "\n" +
                "These third parties have access to your Personal Information only to perform\n" +
                "these tasks on our behalf and are obligated not to disclose or use it for any\n" +
                "other purpose.\n" +
                "\n" +
                "Communications  \n" +
                "--------------\n" +
                "\n" +
                "We may use your Personal Information to contact you with newsletters,\n" +
                "marketing or promotional materials and other information that may be of\n" +
                "interest to you. You may opt out of receiving any, or all, of these\n" +
                "communications from us by following the unsubscribe link or instructions\n" +
                "provided in any email we send.\n" +
                "\n" +
                "Security  \n" +
                "--------\n" +
                "\n" +
                "The security of your Personal Information is important to us, but remember\n" +
                "that no method of transmission over the Internet, or method of electronic\n" +
                "storage is 100% secure. While we strive to use commercially acceptable means\n" +
                "to protect your Personal Information, we cannot guarantee its absolute\n" +
                "security. As such we make no warranties as to the level of security afforded\n" +
                "to your data, except that we will always act in accordance with the relevant\n" +
                "UK and EU legislation.\n" +
                "\n" +
                "Links To Other Sites  \n" +
                "--------------------\n" +
                "\n" +
                "Our Service may contain links to other sites that are not operated by us. If\n" +
                "you click on a third party link, you will be directed to that third party's\n" +
                "site. We strongly advise you to review the Privacy Policy of every site you\n" +
                "visit.\n" +
                "\n" +
                "We have no control over, and assume no responsibility for the content, privacy\n" +
                "policies or practices of any third party sites or services.\n" +
                "\n" +
                "Children's Privacy  \n" +
                "------------------\n" +
                "\n" +
                "Our Service does not address anyone under the age of 13 (\"Children\").\n" +
                "\n" +
                "We do not knowingly collect personally identifiable information from children\n" +
                "under 13. If you are a parent or guardian and you are aware that your Children\n" +
                "has provided us with Personal Information, please contact us. If we become\n" +
                "aware that we have collected Personal Information from a child under age 13\n" +
                "without verification of parental consent, we take steps to remove that\n" +
                "information from our servers.\n" +
                "\n" +
                "Changes To This Privacy Policy  \n" +
                "------------------------------\n" +
                "\n" +
                "We may update our Privacy Policy from time to time. We will notify you of any\n" +
                "changes by posting the new Privacy Policy on this page.\n" +
                "\n" +
                "You are advised to review this Privacy Policy periodically for any changes.\n" +
                "Changes to this Privacy Policy are effective when they are posted on this\n" +
                "page.\n" +
                "\n" +
                "If we make any material changes to this Privacy Policy, we will notify you\n" +
                "either through the email address you have provided us, or by placing a\n" +
                "prominent notice on our website.\n" +
                "\n" +
                "Jurisdiction  \n" +
                "------------\n" +
                "\n" +
                "This Policy shall be governed and construed in accordance with the laws of\n" +
                "England and Wales, without regard to its conflict of law provisions.\n" +
                "\n" +
                "Contact Us  \n" +
                "----------\n" +
                "\n" +
                "If you have any questions about this Privacy Policy, please contact us.\n" +
                "\n");
    }


    private void init() {

        text1 = (TextView) findViewById(R.id.text1);
        back = (RelativeLayout) findViewById(R.id.back);

        home_text = (TextView) findViewById(R.id.home_text);
        find_text = (TextView) findViewById(R.id.find_text);
        nearby_text = (TextView) findViewById(R.id.nearby_text);
        save_text = (TextView) findViewById(R.id.save_text);
        more_text = (TextView) findViewById(R.id.more_text);

        home_lay = (RelativeLayout) findViewById(R.id.home_lay);
        find_lay = (RelativeLayout) findViewById(R.id.find_lay);
        nearby_lay = (RelativeLayout) findViewById(R.id.nearby_lay);
        save_lay = (RelativeLayout) findViewById(R.id.save_lay);
        more_lay = (RelativeLayout) findViewById(R.id.more_lay);

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }
    }


    private void onclick() {

        home_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Home.class));
            }
        });

        find_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Events.class));
            }
        });


        nearby_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Nearby.class));
            }
        });

        save_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), SavedDate.class));
            }
        });

        more_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), More.class));
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
}