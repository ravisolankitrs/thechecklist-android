package com.trs.datenight2;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.widget.LoginButton;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;
import com.squareup.okhttp.internal.Util;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import javax.net.ssl.HttpsURLConnection;

public class SignWithEmail extends Activity {
    public static final String SIGNINVIAEMAIL = "SigninwithEmail";
    ProgressDialog progress;
    TextView back;
    EditText email, pass;
    Button sign_in;
    Bundle bundle1;
    String email_id, password;
    String status = "";
    SharedPreference_main sharedPreference_main;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_with_email);
        bundle1=new Bundle();
        sharedPreference_main = new SharedPreference_main(this);
        init();
        onclick();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    }

    private void init() {
        email = (EditText) findViewById(R.id.email);
        pass = (EditText) findViewById(R.id.pass);
        sign_in = (Button) findViewById(R.id.sign_in);
        back  = (TextView) findViewById(R.id.back);

        Typeface openSans_Light = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-Light.ttf");

        email.setTypeface(openSans_Light);
        pass.setTypeface(openSans_Light);
        sign_in.setTypeface(openSans_Light);

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.primaryDark));
        }
    }

    private void onclick() {

        sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                email_id = email.getText().toString();
                password = pass.getText().toString();
                if (email_id.isEmpty()) {
                    email.setError("Please insert email id");
                } else if (password.isEmpty()) {
                    pass.setError("Please insert password");
                } else {
                    new DownloadJSON().execute();
                }
                /*Bundle bundle = new Bundle();
                bundle.putString("Login", email_id);
                mFirebaseAnalytics.logEvent("SigninWithEmail", bundle);*/
             //   mFirebaseAnalytics.setUserProperty("SignWithEmail", email_id);

            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Login.class);
                startActivity(intent);
            }
        });


    }

    // DownloadJSON AsyncTask
    private class DownloadJSON extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar();
        }

        @Override
        protected Void doInBackground(Void... params) {

            String credentials = password;
            String base64Password = Base64.encodeToString(credentials.getBytes(), Base64.DEFAULT).replace("\n", "");
            Log.d("base64Password", base64Password);

            OkHttpClient client = new OkHttpClient();
            MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
            RequestBody body = RequestBody.create(mediaType, "email=" + email_id .replace("@","%40")+ "&password=" + base64Password.replace("==", "")+"%3D%3D");
            Log.d("send_data", "email=" + email_id .replace("@","%40")+ "&password=" + base64Password.replace("==", "")+"%3D%3D");

            Request request = new Request.Builder()
                    .url(Home.MAIN_APPLICATION_URL + Home.LOGIN_API)
                    .post(body)
                    .addHeader("Content-Type", "application/x-www-form-urlencoded")
                    .addHeader("Cache-Control", "no-cache")
                    .build();
            try {
                Response response = client.newCall(request).execute();
                Log.d("responstr", String.valueOf(response.code()));
                String fulljson = response.body().string();
                Log.d("body", fulljson);

                JSONArray jsonArray = new JSONArray(fulljson);
                JSONObject jsonObject = null;
                for (int i = 0; i < jsonArray.length(); i++) {
                    jsonObject = jsonArray.getJSONObject(i);
                    status = jsonObject.getString("status");
                }

                if (status.equals("1")) {
                    sharedPreference_main.setLogin("successful");
                    sharedPreference_main.setUser_Id(jsonObject.getString("user_id"));
                    sharedPreference_main.setUser_Details(jsonObject.getString("email"),jsonObject.getString("name"),jsonObject.getString("phone"),jsonObject.getString("address"),jsonObject.getString("zip"));
                } else if (status.equals("0")) {
                    sharedPreference_main.setUser_Id("");
                }

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {

            if (status.equals("1")) {
                bundle1.putString("Email", email_id);
                mFirebaseAnalytics.logEvent("Login",bundle1);
                mFirebaseAnalytics.setUserProperty("Login","Email");
                sharedPreference_main.setLogin("successful");
                Toast.makeText(getApplicationContext(), "Successfully login.", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), Home.class);
                startActivity(intent);
            } else if (status.equals("0")) {
                sharedPreference_main.setLogin("unsuccessful");
                Toast.makeText(getApplicationContext(), "Unsuccessfully login. Either email id or password is wrong.", Toast.LENGTH_LONG).show();
            }
            status = "";
            progress.dismiss();
        }
    }


    private void progressBar() {
        progress = new ProgressDialog(SignWithEmail.this);
        progress.setMessage("Loading...");
        progress.setIndeterminate(true);
        progress.setCanceledOnTouchOutside(false);
        progress.show();
    }

}
