package com.trs.datenight2;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class FullBlog extends Activity {

    TextView title, post_tittle, post_content;
    ImageView image, back_image;
    ImageLoader imageLoader;

    TextView home_text, find_text, nearby_text, save_text, more_text;
    RelativeLayout home_lay, find_lay, nearby_lay, save_lay, more_lay;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.full_blog);
        imageLoader = new ImageLoader(getApplicationContext());

        init();
        onclick();

        Bundle b = getIntent().getExtras();
        String b_title = b.getString("POST_TITLE");
        String b_image_url = b.getString("THUMBNAIL_URL");
        String b_content = b.getString("POST_CONTENT").replace("<p>", "\n").replace("</p>", "");

        Log.d("POST_TITLE", b_title);
        Log.d("THUMBNAIL_URL", b_image_url);
        Log.d("POST_CONTENT", b_content);

        post_tittle.setText(b_title);
        post_content.setText(b_content);
        imageLoader.DisplayImage(b_image_url, image);


    }

    private void init() {

        title = (TextView) findViewById(R.id.title);
        post_tittle = (TextView) findViewById(R.id.post_tittle);
        post_content = (TextView) findViewById(R.id.post_content);
        image = (ImageView) findViewById(R.id.image);
        back_image = (ImageView) findViewById(R.id.back_image);

        home_text = (TextView) findViewById(R.id.home_text);
        find_text = (TextView) findViewById(R.id.find_text);
        nearby_text = (TextView) findViewById(R.id.nearby_text);
        save_text = (TextView) findViewById(R.id.save_text);
        more_text = (TextView) findViewById(R.id.more_text);

        home_lay = (RelativeLayout) findViewById(R.id.home_lay);
        find_lay = (RelativeLayout) findViewById(R.id.find_lay);
        nearby_lay = (RelativeLayout) findViewById(R.id.nearby_lay);
        save_lay = (RelativeLayout) findViewById(R.id.save_lay);
        more_lay = (RelativeLayout) findViewById(R.id.more_lay);

        Typeface hanken_light = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Hanken-Light.ttf");
        title.setTypeface(hanken_light);
        post_tittle.setTypeface(hanken_light);
        post_content.setTypeface(hanken_light);

        home_text.setTypeface(hanken_light);
        find_text.setTypeface(hanken_light);
        nearby_text.setTypeface(hanken_light);
        save_text.setTypeface(hanken_light);
        more_text.setTypeface(hanken_light);

    }

    private void onclick() {

        back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        home_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Home.class));
            }
        });

        find_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Events.class));
            }
        });

        nearby_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Nearby.class));
            }
        });

        save_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), SavedDate.class));
            }
        });

        more_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // startActivity(new Intent(getApplicationContext(), More.class));
            }
        });


    }
}
