package com.trs.datenight2;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends PagerAdapter {
    // int[] image;
    LayoutInflater inflater;
    Context context;
    int position = 3;
    ArrayList<String> list;
    ImageLoader imageLoader;


    public ViewPagerAdapter(Details details, ArrayList<String> list) {
        this.list=list;
        this.context=details;
        imageLoader = new ImageLoader(context);
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView img;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemview = inflater.inflate(R.layout.item, container, false);
        img = (ImageView) itemview.findViewById(R.id.ima1);

        imageLoader.DisplayImage(list.get(position), img);
        //  Picasso.with(context).load(list.get(position)).into(img);
        //  img.setImageResource(list.get(position));
        //add item.xml to viewpager
        ((ViewPager) container).addView(itemview);
        return itemview;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove viewpager_item.xml from ViewPager
        ((ViewPager) container).removeView((RelativeLayout) object);
    }

}
