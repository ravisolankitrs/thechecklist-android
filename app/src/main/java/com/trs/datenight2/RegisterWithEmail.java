package com.trs.datenight2;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Created by Sandeep on 1/11/2018.
 */
public class RegisterWithEmail extends Activity {
    public static final String SIGNUPVIAEMAIL = "RegisterwithEmail";
    ProgressDialog progress;
    EditText name, email, pass;
    Button register;
    Bundle bundle1;
    String user_name, email_id, password;
    TextView already_account;
    String status = "";
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_with_email);
        bundle1=new Bundle();
        init();
        onclick();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    }

    private void init() {
        name = (EditText) findViewById(R.id.name);
        email = (EditText) findViewById(R.id.email);
        pass = (EditText) findViewById(R.id.pass);
        register = (Button) findViewById(R.id.register);
        already_account = (TextView) findViewById(R.id.already_account);


        Typeface hanken_Book = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Hanken-Book.ttf");
        Typeface hanken_Light = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Hanken-Light.ttf");
        Typeface openSans_Bold = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-Bold.ttf");
        Typeface openSans_BoldItalic = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-BoldItalic.ttf");
        Typeface openSans_ExtraBold = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-ExtraBold.ttf");
        Typeface openSans_ExtraBoldItalic = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-ExtraBoldItalic.ttf");
        Typeface openSans_Italic= Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-Italic.ttf");
        Typeface openSans_Light = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-Light.ttf");
        Typeface openSans_LightItalic = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-LightItalic.ttf");
        Typeface openSans_Regular = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-Regular.ttf");
        Typeface openSans_Semibold= Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-Semibold.ttf");
        Typeface openSans_SemiboldItalic = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-SemiboldItalic.ttf");


        name.setTypeface(openSans_Light);
        email.setTypeface(openSans_Light);
        pass.setTypeface(openSans_Light);
        register.setTypeface(openSans_Light);
        already_account.setTypeface(openSans_Light);


        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.primaryDark));
        }
    }

    private void onclick() {

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                user_name = name.getText().toString();
                email_id = email.getText().toString();
                password = pass.getText().toString();
                if (user_name.isEmpty()) {
                    name.setError("Please insert name");
                } else if (email_id.isEmpty()) {
                    email.setError("Please insert email id");
                } else if (password.isEmpty()) {
                    pass.setError("Please insert password");
                } else {
                    new DownloadJSON().execute();
                }

                Bundle bundle = new Bundle();
                bundle.putString("Register_user_name", user_name);
                bundle.putString("Register_email_id", email_id);
                mFirebaseAnalytics.logEvent("RegisterWithEmail", bundle);


                //  mFirebaseAnalytics.setUserProperty("Register_user_name", user_name);
                //   mFirebaseAnalytics.setUserProperty("Register_email_id", email_id);

            }
        });

        already_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Login.class);
                startActivity(intent);
            }
        });

    }


    // DownloadJSON AsyncTask
    private class DownloadJSON extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar();
        }

        @Override
        protected Void doInBackground(Void... params) {

            String credentials = password;
            String base64Password = Base64.encodeToString(credentials.getBytes(), Base64.DEFAULT).replace("\n", "");
            Log.d("base64Password", base64Password);

            OkHttpClient client = new OkHttpClient();
            MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
            RequestBody body = RequestBody.create(mediaType, "first_name=" + user_name + "&email=" + email_id + "&password=" + base64Password);
            Log.d("Body", String.valueOf(body));
            Request request = new Request.Builder()
                    .url( Home.MAIN_APPLICATION_URL + Home.REGISTER_API)
                    .post(body)
                    .addHeader("content-type", "application/x-www-form-urlencoded")
                    .addHeader("accept", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .build();

            try {
                Response response = client.newCall(request).execute();
                Log.d("responstr", String.valueOf(response.code()));
                String fulljson = response.body().string();
                Log.d("body", fulljson);

                JSONObject jsonObject = new JSONObject(fulljson);
                status = jsonObject.getString("status");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {

            if (status.equals("1")) {
                bundle1.putString("Email",email_id);
                mFirebaseAnalytics.logEvent("Sign_up",bundle1);
                mFirebaseAnalytics.setUserProperty("Sign_up","Email");
                Toast.makeText(getApplicationContext(), "Registration has been successfully completed.", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), Home.class);
                startActivity(intent);
            } else if (status.equals("0")) {
                Toast.makeText(getApplicationContext(), "Sorry, that username already exists!", Toast.LENGTH_LONG).show();
            }

            status = "";
            progress.dismiss();
        }
    }


    private void progressBar() {
        progress = new ProgressDialog(RegisterWithEmail.this);
        progress.setMessage("Loading...");
        progress.setIndeterminate(true);
        progress.setCanceledOnTouchOutside(false);
        progress.show();
    }
}
