package com.trs.datenight2;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daprlabs.aaron.swipedeck.SwipeDeck;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class ListingActivity extends AppCompatActivity {
    public static final String BOTTOMNAV = "Bottom_Nav";
    public static final String SWIPE = "Swipe";
    /*public static final String SWIPEFILTERCLICKED = "SelectedFilter";
    public static final String SelectedDate = "SelectedDate";
    public static final String SelectedLocation = "SelectedLocation";
    public static final String SelectedConfirm = "SelectedConfirm";
    public static final String SWIPESHARE = "Swipe_Share";*/
    String category1="";
    static int totalswipes = 0;
    static int rightswipes = 0;
    static String EVENT_NAME = "listing_name";
    static String COMPANY_WEBSITE = "company_website";
    static String LOCATION_ADDRESS = "geolocation_formatted_address";
    static String EVENT_IMAGE_URL = "listing_gallery_images";
    static String COMPANY_TAGLINE = "company_tagline";
    static String EVENT_ID = "listing_id";
    static String EVENT_LINK = "listing_link";
    static String EVENT_CONTENT = "listing_content";
    static String EVENT_OWNER = "listing_owner";
    static String TERMS = "listing_tags";
    static String COMMENTS = "comments";
    static String geolocation_data = "geolocation_data";
    static String LOCATION_STREET = "geolocation_street";
    static String LOCATION_POSTCODE = "geolocation_postcode";
    static String LOCATION_LATITUDE = "geolocation_lat";
    static String LOCATION_LONGITUDE = "geolocation_long";
    static String LOCATION_COUNTRY_SHORT = "geolocation_country_short";
    static String LOCATION_CITY = "geolocation_city";
    static String CUSTOM_FIELDS = "custom_fields";
    static String COST = "cost";
    static String IS_FAVORITE = "is_favorite";
    static String PRODUCT = "product";
    //for next activity
    static String MEDIA_GALLERY = "media_gallery";
    static String IMAGES = "images";
    static String VIDEO = "video_url";
    static String TERMS_NAME = "name";
    static Dialog dialogRefineType, dialogRefineDate;
    RelativeLayout home_lay, find_lay, nearby_lay, save_lay, more_lay, reload_page_lay;
    TextView home_text, find_text, nearby_text, save_text, more_text, title, res_name, share;
    ImageView reload_page, refine_search;
    RecyclerView term_recyclerView;
    DetailsTermsListingAdapter terms_adapter;
    ArrayList<HashMap<String, String>> arraylist;
    TextView no_card_left, no_data_found;
    SharedPreference_main sharedPreference_main;
    String status = "", status1 = "", status_message = "", listing_id, islike = "";
    ProgressDialog progress;
    TextView clear, view1;
    ArrayList<HashMap<String, String>> lisitngList;
    List<String> date_array_list = new ArrayList<>();
    Button list_button;
    Dialog dialog;
    Typeface openSans_Regular;
    Button date_button, confirm_icon;
    ListView listView1, listView2;
    ArrayList<String> stringArray;
    CategoriesAdapter adapter1;
    String selected_categories = "", selected_date = "";
    DateFormat formatter;
    DateFormat formatter1;
    Bundle bundle1, bundle2, bundle3, bundle4, bundle5, bundle6, bundle7,bundle8;
    FirebaseAnalytics firebaseAnalytics;
    DateAdapter date_adapter;
    String checkdata = "";
    SwipeDeck swipe_deck;
    RelativeLayout cancel_lay;
    int stableposition;
    private View btnCancel;
    private View btnLove;
    private ListingAdapter listingAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing);
        bundle1 = new Bundle();
        bundle2 = new Bundle();
        bundle3 = new Bundle();
        bundle4 = new Bundle();
        bundle5 = new Bundle();
        bundle6 = new Bundle();
        bundle7 = new Bundle();
        bundle8 = new Bundle();
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        init();
        onClick();

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }


        Intent intent = getIntent();

        if (intent.getExtras() != null) {
            //selected_type = intent.getStringExtra("selected_type");
            if (intent.getStringExtra("selected_type").equals("date")) {
                selected_date = intent.getStringExtra("selected_date");
            } else {
                selected_categories = intent.getStringExtra("selected_categories");
            }
            Log.d("selected_date", selected_date);
            Log.d("selected_categories", selected_categories);

        }

        new ListingJSON().execute();

    }

    public void init() {
        lisitngList = new ArrayList<HashMap<String, String>>();
        sharedPreference_main = new SharedPreference_main(getApplication());

        Typeface hanken_light = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Hanken-Light.ttf");
        Typeface hanken_book = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Hanken-Book.ttf");
        openSans_Regular = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-Regular.ttf");

        title = (TextView) findViewById(R.id.title);
        home_text = (TextView) findViewById(R.id.home_text);
        find_text = (TextView) findViewById(R.id.find_text);
        nearby_text = (TextView) findViewById(R.id.nearby_text);
        save_text = (TextView) findViewById(R.id.save_text);
        more_text = (TextView) findViewById(R.id.more_text);

        home_lay = (RelativeLayout) findViewById(R.id.home_lay);
        find_lay = (RelativeLayout) findViewById(R.id.find_lay);
        nearby_lay = (RelativeLayout) findViewById(R.id.nearby_lay);
        save_lay = (RelativeLayout) findViewById(R.id.save_lay);
        more_lay = (RelativeLayout) findViewById(R.id.more_lay);
        swipe_deck = (SwipeDeck) findViewById(R.id.swipe_deck);
        btnCancel = findViewById(R.id.rejectBtn);
        btnLove = findViewById(R.id.acceptBtn);
        reload_page = findViewById(R.id.reload_page);
        res_name = findViewById(R.id.res_name);
        refine_search = findViewById(R.id.refine_search);
        term_recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        share = (TextView) findViewById(R.id.share);
        reload_page_lay = (RelativeLayout) findViewById(R.id.reload_page_lay);
        no_card_left = (TextView) findViewById(R.id.no_card_left);
        no_data_found = (TextView) findViewById(R.id.no_data_found);
        list_button = findViewById(R.id.list_button);

        home_text.setTypeface(hanken_light);
        find_text.setTypeface(hanken_light);
        nearby_text.setTypeface(hanken_light);
        save_text.setTypeface(hanken_light);
        more_text.setTypeface(hanken_light);
        title.setTypeface(hanken_light);
        res_name.setTypeface(hanken_book);
        share.setTypeface(hanken_book);
        no_card_left.setTypeface(hanken_book);
        no_data_found.setTypeface(hanken_book);
        list_button.setTypeface(hanken_light);

        title.setText("Swipe & Save");

    }

    public void onClick() {

        home_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle1.putString("Home", " ");
                firebaseAnalytics.logEvent(BOTTOMNAV, bundle1);
                firebaseAnalytics.setUserProperty(BOTTOMNAV, "Home");
                startActivity(new Intent(getApplicationContext(), Home.class));//calling home activity
            }
        });

        find_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle1.putString("The_List", " ");
                firebaseAnalytics.logEvent(BOTTOMNAV, bundle1);
                firebaseAnalytics.setUserProperty(BOTTOMNAV, "The_List");
                startActivity(new Intent(getApplicationContext(), Events.class));//calling event activity
            }
        });

        nearby_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle1.putString("NearBy", " ");
                firebaseAnalytics.logEvent(BOTTOMNAV, bundle1);
                firebaseAnalytics.setUserProperty(BOTTOMNAV, "NearBy");
                startActivity(new Intent(getApplicationContext(), Nearby.class));//calling nearby activity
            }
        });
        save_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle1.putString("Save", " ");
                firebaseAnalytics.logEvent(BOTTOMNAV, bundle1);
                firebaseAnalytics.setUserProperty(BOTTOMNAV, "Save");
                startActivity(new Intent(getApplicationContext(), SavedDate.class));//calling savedate activity
            }
        });

        more_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle1.putString("More", " ");
                firebaseAnalytics.logEvent(BOTTOMNAV, bundle1);
                firebaseAnalytics.setUserProperty(BOTTOMNAV, "More");
                startActivity(new Intent(getApplicationContext(), More.class));//calling more activity
            }
        });

        list_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Events.class));
            }
        });

                refine_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle5.putString("Selected", " ");
                firebaseAnalytics.logEvent("Filter", bundle5);
                firebaseAnalytics.setUserProperty("Filter","Selected");
                showCategories_and_date_popup();
            }
        });


        swipe_deck.setCallback(new SwipeDeck.SwipeDeckCallback() {
            @Override
            public void cardSwipedLeft(long stableId) {
                totalswipes = totalswipes + 1;
                String total=String.valueOf(totalswipes);
                bundle2.putString("Swipes",total);
                bundle2.putString(total," ");
                firebaseAnalytics.logEvent(SWIPE, bundle2);
                firebaseAnalytics.setUserProperty(SWIPE, total);
                stableposition = (int) stableId;
                Log.d("stablepositionl", String.valueOf(stableposition));

                if (stableposition == lisitngList.size() - 1) {
                    res_name.setText("");
                    arraylist.clear();//clear terms array
                    terms_adapter.notifyDataSetChanged();
                    share.setVisibility(View.GONE);
                    no_card_left.setVisibility(View.VISIBLE);
                    no_data_found.setVisibility(View.GONE);
                } else {
                    listing_id = lisitngList.get(stableposition).get(EVENT_ID);
                    res_name.setText(lisitngList.get(stableposition + 1).get(EVENT_NAME));
                    termsAdapter();//calling terms adapter
                    islike = "0";
                    new SwipeListingJSON().execute();
                    share.setVisibility(View.VISIBLE);
                    no_card_left.setVisibility(View.GONE);
                    no_data_found.setVisibility(View.GONE);
                }
            }

            @Override
            public void cardSwipedRight(long stableId) {
                totalswipes = totalswipes + 1;
                rightswipes = rightswipes + 1;
                String total=String.valueOf(totalswipes);
                bundle2.putString("Swipes", total);
                bundle3.putString("Saved", " ");
                firebaseAnalytics.logEvent(SWIPE, bundle2);
                firebaseAnalytics.logEvent(SWIPE, bundle3);
                firebaseAnalytics.setUserProperty(SWIPE, total);
                firebaseAnalytics.setUserProperty(SWIPE, "Saved");
                stableposition = (int) stableId;
                Log.d("stablepositionr", String.valueOf(stableposition));

                if (stableposition == lisitngList.size() - 1) {
                    res_name.setText("");
                    arraylist.clear();//clear terms array
                    terms_adapter.notifyDataSetChanged();
                    share.setVisibility(View.GONE);
                    no_card_left.setVisibility(View.VISIBLE);
                    no_data_found.setVisibility(View.GONE);
                } else {
                    listing_id = lisitngList.get(stableposition).get(EVENT_ID);
                    res_name.setText(lisitngList.get(stableposition + 1).get(EVENT_NAME));
                    termsAdapter();//calling terms adapter
                    islike = "1";
                    new SwipeListingJSON().execute();
                    share.setVisibility(View.VISIBLE);
                    no_card_left.setVisibility(View.GONE);
                    no_data_found.setVisibility(View.GONE);
                }


            }

            @Override
            public boolean isDragEnabled(long itemId) {
                return true;
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                swipe_deck.swipeTopCardLeft(500);//swipe left side
            }
        });

        btnLove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                swipe_deck.swipeTopCardRight(500);//swipe right side
            }
        });

        reload_page_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (stableposition >= 0) {
                    res_name.setText(lisitngList.get(stableposition).get(EVENT_NAME));
                    termsAdapterReload();//calling terms adapter
                    stableposition = stableposition - 1;
                    swipe_deck.unSwipeCard();//reload stack

                    share.setVisibility(View.VISIBLE);
                    no_card_left.setVisibility(View.GONE);
                    no_data_found.setVisibility(View.GONE);
                }
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*bundle1.putString("Swipe_Share", "Clicked");
                firebaseAnalytics.logEvent(SWIPESHARE, bundle1);*/
                String share_url = lisitngList.get(stableposition + 1).get(EVENT_LINK);
                Intent share = new Intent(android.content.Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                share.putExtra(Intent.EXTRA_TEXT, share_url);
                startActivity(Intent.createChooser(share, "Share link!"));
            }
        });
    }


    public void termsAdapter() {
        arraylist = new ArrayList<HashMap<String, String>>();
        try {
            JSONArray jsonArray = new JSONArray(lisitngList.get(stableposition + 1).get(Events.TERMS));
            for (int i = 0; i < jsonArray.length(); i++) {
                HashMap<String, String> map = new HashMap<String, String>();
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                map.put(TERMS_NAME, jsonObject.getString(TERMS_NAME));
                arraylist.add(map);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        term_recyclerView.setLayoutManager(horizontalLayoutManagaer);
        terms_adapter = new DetailsTermsListingAdapter(getApplicationContext(), arraylist);
        term_recyclerView.setAdapter(terms_adapter);

    }

    public void termsAdapterReload() {
        arraylist = new ArrayList<HashMap<String, String>>();
        try {
            JSONArray jsonArray = new JSONArray(lisitngList.get(stableposition).get(Events.TERMS));
            for (int i = 0; i < jsonArray.length(); i++) {
                HashMap<String, String> map = new HashMap<String, String>();
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                map.put(TERMS_NAME, jsonObject.getString(TERMS_NAME));
                arraylist.add(map);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        term_recyclerView.setLayoutManager(horizontalLayoutManagaer);
        terms_adapter = new DetailsTermsListingAdapter(getApplicationContext(), arraylist);
        term_recyclerView.setAdapter(terms_adapter);

    }

    public void progressBar() {
        progress = new ProgressDialog(this);
        progress.setMessage("Loading...");
        progress.setIndeterminate(true);
        progress.setCanceledOnTouchOutside(false);
        progress.show();
    }

    public void showCategories_and_date_popup() {
        dialog = new Dialog(ListingActivity.this);
        dialog.setCanceledOnTouchOutside(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.categories_and_date_popup);

        dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

        TextView categories = dialog.findViewById(R.id.categories);
        TextView date = dialog.findViewById(R.id.date);
        clear = dialog.findViewById(R.id.clear);
        view1 = dialog.findViewById(R.id.view1);
        RelativeLayout bottomlayout = dialog.findViewById(R.id.bottomlayout);
        cancel_lay = dialog.findViewById(R.id.cancel_lay);
        TextView cancel_btn = dialog.findViewById(R.id.cancel_btn);

        categories.setTypeface(openSans_Regular);
        date.setTypeface(openSans_Regular);
        clear.setTypeface(openSans_Regular);
        cancel_btn.setTypeface(openSans_Regular);

        if (sharedPreference_main.getcheckdata().equals("1")) {
            clear.setVisibility(View.VISIBLE);
            view1.setVisibility(View.VISIBLE);
        } else {
            clear.setVisibility(View.GONE);
            view1.setVisibility(View.GONE);
        }

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selected_categories = "";
                selected_date = "";
                sharedPreference_main.setcheckdata("");
                dialog.dismiss();

                Intent intent = new Intent(getApplicationContext(), ListingActivity.class);
                intent.putExtra("selected_categories", "");
                intent.putExtra("selected_type", "categories");
                intent.putExtra("selected_date", "");
                intent.putExtra("selected_type", "date");
                startActivity(intent);
            }
        });


        cancel_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                selected_categories = "";
                selected_date = "";
                refinedate();
            }
        });

        categories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                selected_categories = "";
                selected_date = "";
                refinecategory();
            }
        });
        dialog.show();
    }

    //choose category popup
    private void refinecategory() {
        dialogRefineType = new Dialog(ListingActivity.this);
        dialogRefineType.setCanceledOnTouchOutside(true);
        dialogRefineType.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // dialogRefineType.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        dialogRefineType.setContentView(R.layout.categories);

        // dates = (Button) dialogRefineType.findViewById(R.id.dates);
        date_button = (Button) dialogRefineType.findViewById(R.id.button);
        listView1 = (ListView) dialogRefineType.findViewById(R.id.sampleListView);
        confirm_icon = (Button) dialogRefineType.findViewById(R.id.button1);

        stringArray = new ArrayList<String>();
        stringArray.add("Activities");
        stringArray.add("Alternatives");
        stringArray.add("Arts & Crafts");
        stringArray.add("Classes");
        stringArray.add("Comedy");
        stringArray.add("Drinks");
        stringArray.add("Food");
        stringArray.add("Live");
        stringArray.add("Musuems");
        stringArray.add("Sights");
        stringArray.add("Theatre");
        stringArray.add("Sports");


        adapter1 = new CategoriesAdapter(getApplicationContext(), stringArray);
        listView1.setAdapter(adapter1);

        listView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //  Details.getBookingCount(stringArray.get(position));
                //  Details.checkBoxItemPosition(position);
                selected_categories = stringArray.get(position).replace("Arts & Crafts", "arts-crafts");
                category1=stringArray.get(position);

            }
        });

        confirm_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreference_main.setcheckdata("1");
                dialog.dismiss();
                dialogRefineType.dismiss();

                selected_categories = String.valueOf(selected_categories);
                bundle6.putString(selected_categories," " );
                bundle8.putString("Confirm", " ");
                firebaseAnalytics.logEvent("Filter", bundle6);
                firebaseAnalytics.logEvent("Filter", bundle8);
                firebaseAnalytics.setUserProperty("Filter", selected_categories);
                firebaseAnalytics.setUserProperty("Filter", "Confirm");
                Intent intent = new Intent(getApplicationContext(), ListingActivity.class);
                intent.putExtra("selected_categories", String.valueOf(selected_categories));
                intent.putExtra("selected_type", "categories");
                startActivity(intent);
                // swipe_deck.unSwipeCard();//reload stack
            }
        });

        date_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selected_categories = String.valueOf(selected_categories);
                dialogRefineType.dismiss();
                refinedate();

            }
        });

        dialogRefineType.show();

    }

    //date popup
    private void refinedate() {
        dialogRefineDate = new Dialog(ListingActivity.this);
        dialogRefineDate.setCanceledOnTouchOutside(true);
        dialogRefineDate.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogRefineDate.setContentView(R.layout.selectdates);

        listView2 = (ListView) dialogRefineDate.findViewById(R.id.sampledatelist);


        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat def = new SimpleDateFormat("dd MMM yyyy");
        String formattedDate1 = def.format(c);
        System.out.println("Current time => " + formattedDate1);


        Calendar cal = Calendar.getInstance(); //Get the Calendar instance
        cal.add(Calendar.MONTH, 3);//Three months from now
        cal.getTime();// Get the Date object

        Log.d("next 3 monthsasdvcsd", String.valueOf(cal.getTime()));

        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
        String formattedDate2 = df.format(cal.getTime());

        Log.d("next 3 months", formattedDate2);

        formatter = new SimpleDateFormat("dd MMM yyyy");
        formatter1 = new SimpleDateFormat("dd MMM yyyy");


        final List<Date> dates = new ArrayList<Date>();
        Date startDate = null;
        Date endDate = null;
        try {
            startDate = (Date) formatter.parse(formattedDate1);
            endDate = (Date) formatter.parse(formattedDate2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (!formattedDate1.equals("")) {
            long interval = 24 * 1000 * 60 * 60; // 1 hour in millis
            long endTime = endDate.getTime(); // create your endtime here, possibly using Calendar or Date
            long curTime = startDate.getTime();
            while (curTime <= endTime) {
                dates.add(new Date(curTime));
                curTime += interval;
            }
            for (int i = 0; i < dates.size(); i++) {
                Date lDate = (Date) dates.get(i);
                String ds = formatter1.format(lDate);
                System.out.println(ds);
                date_array_list.add(ds);
            }

            date_adapter = new DateAdapter(getApplicationContext(), date_array_list);
            listView2.setAdapter(date_adapter);
            dialogRefineDate.show();

        }

        listView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                sharedPreference_main.setcheckdata("1");
                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                String formattedDate2 = df.format(dates.get(position));
                bundle6.putString(formattedDate2, " ");
                firebaseAnalytics.logEvent("Filter", bundle6);
                firebaseAnalytics.setUserProperty("Filter", formattedDate2);

                dialogRefineDate.dismiss();



                Intent intent = new Intent(getApplicationContext(), ListingActivity.class);
                intent.putExtra("selected_date", selected_date);
                intent.putExtra("selected_type", "date");
                startActivity(intent);
            }
        });


    }


    // ListingJSON AsyncTask
    private class ListingJSON extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            status = "";
            arraylist = new ArrayList<HashMap<String, String>>();
            swipe_deck.setVisibility(View.GONE);
            lisitngList.clear();
            term_recyclerView.setVisibility(View.GONE);
            res_name.setText("");
            progressBar();
        }

        @Override
        protected Void doInBackground(Void... params) {

            OkHttpClient client = new OkHttpClient();
            MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
            RequestBody body;
            Log.d("user_id", sharedPreference_main.getUser_Id().toString());

            if (!selected_categories.equals("") && !selected_date.equals("")) {
                body = RequestBody.create(mediaType, "user_id=" + sharedPreference_main.getUser_Id().toString() + "&categories=" + selected_categories.replace("[", "").replace("]", "") + "&expiry_date=" + selected_date);
            } else if (!selected_categories.equals("")) {
                body = RequestBody.create(mediaType, "user_id=" + sharedPreference_main.getUser_Id().toString() + "&categories=" + selected_categories.replace("[", "").replace("]", ""));
            } else if (!selected_date.equals("")) {
                body = RequestBody.create(mediaType, "user_id=" + sharedPreference_main.getUser_Id().toString() + "&expiry_date=" + selected_date);
            } else {
                body = RequestBody.create(mediaType, "user_id=" + sharedPreference_main.getUser_Id());
            }

            //Log.d("body", String.valueOf(body));
            Request request = new Request.Builder()
                    .url(Home.MAIN_APPLICATION_URL + "listing/swipe")
                    .post(body)
                    .addHeader("content-type", "application/x-www-form-urlencoded")
                    .addHeader("accept", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .build();
            try {
                Response response = client.newCall(request).execute();
                Log.d("responstr", String.valueOf(response.code()));
                String fulljson = response.body().string();
                Log.d("body", fulljson);

                JSONArray jsonArray = new JSONArray(fulljson);
                for (int i = 0; i < jsonArray.length(); i++) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    if (jsonObject.has("status")) {
                        status = jsonObject.getString("status");
                        status_message = jsonObject.getString("message");
                    } else {

                        if (jsonObject.has(EVENT_NAME)) {
                            map.put(EVENT_NAME, jsonObject.getString(EVENT_NAME));
                        } else {
                            map.put(EVENT_NAME, "");
                        }

                        if (jsonObject.has(COMPANY_WEBSITE)) {
                            map.put(COMPANY_WEBSITE, jsonObject.getString(COMPANY_WEBSITE));
                        } else {
                            map.put(COMPANY_WEBSITE, "");
                        }
                        if (jsonObject.has(COMPANY_TAGLINE)) {
                            map.put(COMPANY_TAGLINE, jsonObject.getString(COMPANY_TAGLINE));
                        } else {
                            map.put(COMPANY_TAGLINE, "");
                        }

                        JSONObject jsonobject3 = jsonObject.getJSONObject(geolocation_data);
                        if (jsonobject3.has(LOCATION_ADDRESS)) {
                            map.put(LOCATION_ADDRESS, jsonobject3.getString(LOCATION_ADDRESS));
                        } else {
                            map.put(LOCATION_ADDRESS, "");
                        }
                        if (jsonobject3.has(LOCATION_STREET)) {
                            map.put(LOCATION_STREET, jsonobject3.getString(LOCATION_STREET));
                        } else {
                            map.put(LOCATION_STREET, "");
                        }

                        if (jsonobject3.has(LOCATION_POSTCODE)) {
                            map.put(LOCATION_POSTCODE, jsonobject3.getString(LOCATION_POSTCODE));
                        } else {
                            map.put(LOCATION_POSTCODE, "");
                        }


                        if (jsonobject3.has(LOCATION_LATITUDE)) {
                            map.put(LOCATION_LATITUDE, jsonobject3.getString(LOCATION_LATITUDE));
                        } else {
                            map.put(LOCATION_LATITUDE, "");
                        }

                        if (jsonobject3.has(LOCATION_COUNTRY_SHORT)) {
                            map.put(LOCATION_COUNTRY_SHORT, jsonobject3.getString(LOCATION_COUNTRY_SHORT));
                        } else {
                            map.put(LOCATION_COUNTRY_SHORT, "");
                        }
                        if (jsonobject3.has(LOCATION_CITY)) {
                            map.put(LOCATION_CITY, jsonobject3.getString(LOCATION_CITY));
                        } else {
                            map.put(LOCATION_CITY, "");
                        }

                        if (jsonobject3.has(LOCATION_LONGITUDE)) {
                            map.put(LOCATION_LONGITUDE, jsonobject3.getString(LOCATION_LONGITUDE));
                        } else {
                            map.put(LOCATION_LONGITUDE, "");
                        }

                        if (jsonObject.has(EVENT_IMAGE_URL)) {
                            map.put(EVENT_IMAGE_URL, jsonObject.getString(EVENT_IMAGE_URL));
                        } else {
                            map.put(EVENT_IMAGE_URL, "");
                        }
                        if (jsonObject.has(EVENT_ID)) {
                            map.put(EVENT_ID, jsonObject.getString(EVENT_ID));
                        } else {
                            map.put(EVENT_ID, "");
                        }

                        if (jsonObject.has(EVENT_LINK)) {
                            map.put(EVENT_LINK, jsonObject.getString(EVENT_LINK));
                        } else {
                            map.put(EVENT_LINK, "");
                        }

                        if (jsonObject.has(EVENT_CONTENT)) {
                            map.put(EVENT_CONTENT, jsonObject.getString(EVENT_CONTENT));
                        } else {
                            map.put(EVENT_CONTENT, "");
                        }

                        map.put(EVENT_OWNER, jsonObject.getString(EVENT_OWNER));
                        map.put(TERMS, jsonObject.getString(TERMS));
                        map.put(CUSTOM_FIELDS, jsonObject.getString(CUSTOM_FIELDS));

                        JSONObject jsonobject4 = jsonObject.getJSONObject(CUSTOM_FIELDS);
                        map.put(COST, jsonobject4.getString(COST));

                        map.put(COMMENTS, jsonObject.getString(COMMENTS));
                        map.put(IS_FAVORITE, jsonObject.getString(IS_FAVORITE));
                        map.put(PRODUCT, jsonObject.getString(PRODUCT));

                        JSONObject jsonObject5 = jsonObject.getJSONObject(MEDIA_GALLERY);
                        if (jsonObject5.has(IMAGES)) {
                            map.put(IMAGES, jsonObject5.getString(IMAGES));
                        } else {
                            map.put(IMAGES, "");
                        }

                        if (jsonObject5.has(VIDEO)) {
                            map.put(VIDEO, jsonObject5.getString(VIDEO));
                            Log.d("hdfhsdshd", jsonObject5.getString(VIDEO));
                        } else {
                            map.put(VIDEO, "");
                        }

                        lisitngList.add(map);
                        Log.d("array_list", String.valueOf(lisitngList));
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            progress.dismiss();
            swipe_deck.setVisibility(View.VISIBLE);
            term_recyclerView.setVisibility(View.VISIBLE);
            if (status.equals("0")) {
                arraylist.clear();//clear terms array
                share.setVisibility(View.GONE);
                term_recyclerView.setVisibility(View.GONE);
                no_card_left.setVisibility(View.GONE);
                no_data_found.setVisibility(View.VISIBLE);

            } else {
                share.setVisibility(View.VISIBLE);
                term_recyclerView.setVisibility(View.VISIBLE);
                no_card_left.setVisibility(View.GONE);
                no_data_found.setVisibility(View.GONE);

                listingAdapter = new ListingAdapter(getApplicationContext(), lisitngList,category1 );
                if (swipe_deck != null) {
                    swipe_deck.setAdapter(listingAdapter);
                }
                stableposition = -1;
                termsAdapter();
                res_name.setText(lisitngList.get(0).get(EVENT_NAME));

                listingAdapter.notifyDataSetChanged();

            }
        }
    }

    // SwipeListingJSON AsyncTask
    private class SwipeListingJSON extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            OkHttpClient client = new OkHttpClient();
            MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
            RequestBody body;

            body = RequestBody.create(mediaType, "user_id=" + sharedPreference_main.getUser_Id() + "&listing_id=" + listing_id + "&is_like=" + islike);

            //Log.d("body", String.valueOf(body));
            Request request = new Request.Builder()
                    .url(Home.MAIN_APPLICATION_URL + "listing/like")
                    .post(body)
                    .addHeader("content-type", "application/x-www-form-urlencoded")
                    .addHeader("accept", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .build();
            try {
                Response response = client.newCall(request).execute();
                Log.d("responstr", String.valueOf(response.code()));
                String fulljson = response.body().string();
                Log.d("body", fulljson);

                JSONArray jsonArray = new JSONArray(fulljson);
                for (int i = 0; i < jsonArray.length(); i++) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    if (jsonObject.has("status")) {
                        status1 = jsonObject.getString("status");
                        status_message = jsonObject.getString("message");
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {


        }
    }
}

