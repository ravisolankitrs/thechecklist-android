package com.trs.datenight2;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Sandeep on 1/29/2018.
 */
public class HintsAndTipsAdapter extends RecyclerView.Adapter<HintsAndTipsAdapter.CustomViewHolder> {

    Context context;
    ArrayList<HashMap<String, String>> data;
    HashMap<String, String> resultp = new HashMap<String, String>();
    ImageLoader imageLoader;

    // Provide a suitable constructor (depends on the kind of dataset)
    public HintsAndTipsAdapter(Context context, ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        this.data = arraylist;
        imageLoader = new ImageLoader(context);
    }


    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //View view = inflater.inflate(R.layout.hint_and_tips_list, parent, false);

        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.hint_and_tips_list, parent, false);
        return new CustomViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int position) {

        Typeface hanken_light = Typeface.createFromAsset(context.getAssets(), "fonts/Hanken-Light.ttf");
        holder.tittle.setTypeface(hanken_light);

        resultp = data.get(position);
        holder.tittle.setText(resultp.get(HintsAndTips.POST_TITLE));
        imageLoader.DisplayImage(resultp.get(HintsAndTips.THUMBNAIL_URL), holder.image);

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resultp = data.get(position);

                //Intent intent = new Intent(context, FullHintsAndTips.class);
                // intent.putExtra("url", resultp.get(HintsAndTips.POST_URL));
                // intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                // context.startActivity(intent);

                Bundle b = new Bundle();
                b.putString("POST_TITLE", resultp.get(HintsAndTips.POST_TITLE));
                b.putString("THUMBNAIL_URL", resultp.get(HintsAndTips.THUMBNAIL_URL));
                b.putString("POST_CONTENT", resultp.get(HintsAndTips.POST_CONTENT));

                Intent intent = new Intent(context, FullBlog.class);
                intent.putExtras(b);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        TextView tittle;
        ImageView image;

        public CustomViewHolder(View itemView) {
            super(itemView);
            tittle = (TextView) itemView.findViewById(R.id.tittle);
            image = (ImageView) itemView.findViewById(R.id.image);
        }

    }
    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return data.size();
    }
}

