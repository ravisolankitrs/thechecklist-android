package com.trs.datenight2;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;


public class FindDateAdapter extends BaseAdapter {

    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    HashMap<String, String> resultp = new HashMap<String, String>();
    ImageLoader imageLoader;

    public FindDateAdapter(Context context, ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        this.data = arraylist;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.find_date_list, viewGroup, false);
        TextView event_name = (TextView) itemView.findViewById(R.id.event_name);
        TextView event_address = (TextView) itemView.findViewById(R.id.event_address);
        TextView ticket_price = (TextView) itemView.findViewById(R.id.ticket_price);
        ImageView event_image = (ImageView) itemView.findViewById(R.id.event_image);
        ImageView save_un_save = (ImageView) itemView.findViewById(R.id.save_un_save);

        Typeface hanken_Book = Typeface.createFromAsset(context.getAssets(), "fonts/Hanken-Book.ttf");
        Typeface openSans_Regular = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
        Typeface openSans_Light = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Light.ttf");

        event_name.setTypeface(hanken_Book);
        ticket_price.setTypeface(openSans_Regular);
        event_address.setTypeface(openSans_Light);

        resultp = data.get(position);
        event_name.setText(resultp.get(Events.EVENT_NAME));
        event_address.setText(resultp.get(Events.LOCATION_STREET)+", " + resultp.get(Events.LOCATION_POSTCODE));
        ticket_price.setText(resultp.get(Events.COST));

        String is_fav = resultp.get(Events.IS_FAVORITE);
        Log.d("is_fav", is_fav);

        if (is_fav.equals("1")){
            save_un_save.setImageResource(R.drawable.save);
        }else {
            save_un_save.setImageResource(R.drawable.un_save);
        }

        imageLoader.DisplayImage(resultp.get(Events.EVENT_IMAGE_URL), event_image);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resultp = data.get(position);
                Log.d("EVENT_ID", resultp.get(Events.EVENT_ID));
                Intent intent = new Intent(context, Details.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Bundle bundle = new Bundle();
                bundle.putString(Events.EVENT_IMAGE_URL, resultp.get(Events.EVENT_IMAGE_URL));
                bundle.putString(Events.EVENT_ID, resultp.get(Events.EVENT_ID));
                bundle.putString(Events.EVENT_NAME, resultp.get(Events.EVENT_NAME));
                bundle.putString(Events.COMPANY_TAGLINE, resultp.get(Events.COMPANY_TAGLINE));
                bundle.putString(Events.EVENT_LINK, resultp.get(Events.EVENT_LINK));
                bundle.putString(Events.EVENT_CONTENT, resultp.get(Events.EVENT_CONTENT));
                bundle.putString(Events.EVENT_OWNER, resultp.get(Events.EVENT_OWNER));
                bundle.putString(Events.TERMS, resultp.get(Events.TERMS));
                bundle.putString(Events.COMMENTS, resultp.get(Events.COMMENTS));
                bundle.putString(Events.CUSTOM_FIELDS, resultp.get(Events.CUSTOM_FIELDS));
                bundle.putString(Events.COST, resultp.get(Events.COST));
                bundle.putString(Events.LOCATION_ADDRESS, resultp.get(Events.LOCATION_ADDRESS));
                bundle.putString(Events.LOCATION_STREET, resultp.get(Events.LOCATION_STREET));
                bundle.putString(Events.LOCATION_POSTCODE, resultp.get(Events.LOCATION_POSTCODE));
                bundle.putString(Events.LOCATION_LATITUDE, resultp.get(Events.LOCATION_LATITUDE));
                bundle.putString(Events.LOCATION_LONGITUDE, resultp.get(Events.LOCATION_LONGITUDE));
                bundle.putString(Events.IS_FAVORITE, resultp.get(Events.IS_FAVORITE));
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });

        return itemView;
    }
}


