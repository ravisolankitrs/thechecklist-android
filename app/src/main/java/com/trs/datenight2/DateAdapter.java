package com.trs.datenight2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Sandeep on 4/9/2018.
 */
public class DateAdapter extends BaseAdapter {

    Context context;
    LayoutInflater inflater;
    List<String> data;
    String resultp = new String();


    public DateAdapter(Context context, List<String> arraylist) {
        this.context = context;
        this.data = arraylist;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final TextView Selectdate;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.details_date_list, viewGroup, false);
        Selectdate = (TextView) itemView.findViewById(R.id.date_text);

        resultp = data.get(i);
        //Log.d("resultp", resultp);
        Selectdate.setText(resultp);




      /*  itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Details.getBookingCount(categories.getText().toString());
                view.setSelected(true);
                Details.checkBoxItemPosition(i);
                notifyDataSetChanged();
            }
        });*/

        return itemView;
    }
}
