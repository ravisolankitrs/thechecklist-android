package com.trs.datenight2;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;

import static com.trs.datenight2.NearbyAdapter.EVENT_NAME;


public class Details extends AppCompatActivity implements OnMapReadyCallback {
    public static final String BOTTOM_NAV = "Bottom_Nav";
    public static final String BOOKINGNOW = "bookingNow";
    public static final String SELECTEDCONTENT = "Restaurant_Details";
    public static final String SHARE = "Content_Share";
    public static final String BOOK_CTA = "Book_cta";
    public static final String BOOKINGSTEPS = "Booking_Step";
    public static TextView date_popup;
    public static Button reserve_now;
    static TextView date_text;
    static String TERMS_NAME = "name";
    static Dialog dialogDateMode, dialogBookingCount;
    static String date_data = "", time_data, booking_count = "";
    static String EVENT_START_TIME = "listing_start_time";
    static String EVENT_END_TIME = "listing_end_time";
    static String EVENT_START_DATE = "listing_start_date";
    static String EVENT_END_DATE = "listing_end_date";
    static Integer item_position = -1;
    static String order_date = "";
    static String order_time = "";
    private static int currentPage = 0;
    Bundle bundle0, sharebundle, reservebundle, viewsitebundle, bookingbundle, bundle1;
    //The CTA buttons on the listing page. parameters are based on the woocommerce produce type
    FirebaseAnalytics firebaseAnalytics;
    RecyclerView term_recyclerView, recycler_booking, video_recyclerView;
    SharedPreference_main sharedPreference_main;
    ImageView cancel_icon;
    TextView event_name, event_content, ticket_price, change_date, location_name, event_address;
    RelativeLayout image_layout;
    FrameLayout frameLayout;
    ImageView event_image;
    ImageView save_image;
    Button book_now;
    RelativeLayout music, drinks, food, cultural, top_pics;
    ProgressDialog progress;
    ArrayList<HashMap<String, String>> arraylist;
    List<VideoList> videoLists;
    DetailsTermsAdapter terms_adapter;
    DetailsTimeAdapter time_adapter;
    BookingDetailsAdapter bookingDetailsAdapter;
    DetailsDateAdapter date_adapter;
    ArrayList<HashMap<String, String>> time_array_list;
    ArrayList<HashMap<String, String>> video_array_list;
    MapFragment mapFragment;
    RelativeLayout share_layout, save_layout;
    String share_url = "";
    String is_fav = "";
    String massage = "";
    Dialog dialog, dialog1, dialog2;
    List<String> date_array_list = new ArrayList<>();
    String str_date = "", end_date = "", str_time = "", end_time = "";
    String custom_fields;
    String latitued = "", longitude = "";
    ImageLoader imageLoader;
    Bundle bundle;
    DateFormat formatter;
    DateFormat formatter1;
    DateFormat formatter2;
    TextView title;
    TextView recommended, recommended_text;
    TextView home_text, find_text, nearby_text, save_text, more_text;
    RelativeLayout home_lay, find_lay, nearby_lay, save_lay, more_lay;
    TextView bottom_box1, bottom_box2, bottom_box3, bottom_box4, bottom_box5;
    String booking_type = "", external_booking_url = "", product_id = "", _button_text = "", product_price = "0", description = "";
    String order_status = "";
    String order_message = "";
    ImageView leftNav, rightNav;
    ViewPager viewpager;
    PagerAdapter adapter;
    List<String> list = new ArrayList<String>();
    ArrayList<String> listdata = new ArrayList<String>();
    int imageListsize;
    Typeface openSans_Light, hanken_Book, hanken_Light, openSans_Semibold, openSans_Regular;
    Integer minteger = 0, minteger1 = 0;
    String daysname = "";
    String day, booking_username, booking_phone_no;
    TextView booking_counting1, booking_counting, fill_all_the_details;
    Button pricebutton;
    ArrayList<HashMap<String, String>> booking_arraylist;
    int pos = 0;
    EditText username_edit, phone_no;
    private GoogleMap mMap;
    private FirebaseAnalytics mFirebaseAnalytics;
    ScrollView scrollview;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details);
        bundle0 = new Bundle();
        bundle1 = new Bundle();
        sharebundle = new Bundle();
        reservebundle = new Bundle();
        viewsitebundle = new Bundle();
        bookingbundle = new Bundle();
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        sharedPreference_main = new SharedPreference_main(getApplicationContext());
        init();
        onclick();

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        hanken_Book = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Hanken-Book.ttf");
        hanken_Light = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Hanken-Light.ttf");
        Typeface openSans_Bold = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-Bold.ttf");
        Typeface openSans_BoldItalic = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-BoldItalic.ttf");
        Typeface openSans_ExtraBold = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-ExtraBold.ttf");
        Typeface openSans_ExtraBoldItalic = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-ExtraBoldItalic.ttf");
        Typeface openSans_Italic = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-Italic.ttf");
        openSans_Light = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-Light.ttf");
        Typeface openSans_LightItalic = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-LightItalic.ttf");
        openSans_Regular = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-Regular.ttf");
        openSans_Semibold = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-Semibold.ttf");
        Typeface openSans_SemiboldItalic = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-SemiboldItalic.ttf");

        title.setTypeface(openSans_Light);
        event_name.setTypeface(hanken_Light);
        event_content.setTypeface(openSans_Light);
        ticket_price.setTypeface(hanken_Book);
        // select_time.setTypeface(hanken_Light);
        location_name.setTypeface(openSans_Regular);
        event_address.setTypeface(openSans_Light);
        recommended.setTypeface(hanken_Light);
        recommended_text.setTypeface(openSans_Light);

        home_text.setTypeface(hanken_Light);
        find_text.setTypeface(hanken_Light);
        nearby_text.setTypeface(hanken_Light);
        save_text.setTypeface(hanken_Light);
        more_text.setTypeface(hanken_Light);

        bottom_box1.setTypeface(hanken_Light);
        bottom_box2.setTypeface(hanken_Light);
        bottom_box3.setTypeface(hanken_Light);
        bottom_box4.setTypeface(hanken_Light);
        bottom_box5.setTypeface(hanken_Light);

        video_recyclerView.setHasFixedSize(true);
        video_recyclerView.setItemViewCacheSize(1000);
        LinearLayoutManager horizontalLayoutManagaer1 = new LinearLayoutManager(Details.this, LinearLayoutManager.HORIZONTAL, false);
        video_recyclerView.setLayoutManager(horizontalLayoutManagaer1);

        bundle = getIntent().getExtras();
        //     Log.d("image_url", bundle.getString(Events.IMAGES));
        Log.d("event_id", String.valueOf(bundle.getString("event_id")));
        Log.d("terms", String.valueOf(bundle.getString("terms")));


        Log.d("Event_name", String.valueOf(bundle.get(EVENT_NAME)));
        arraylist = new ArrayList<HashMap<String, String>>();
        time_array_list = new ArrayList<HashMap<String, String>>();
        booking_arraylist = new ArrayList<HashMap<String, String>>();
        video_array_list = new ArrayList<HashMap<String, String>>();
        videoLists = new ArrayList<>();

        imageLoader = new ImageLoader(getApplication());
        formatter = new SimpleDateFormat("yyyyMMdd");
        formatter1 = new SimpleDateFormat("dd MMM yyyy");
        formatter2 = new SimpleDateFormat("yyyy-MM-dd");


        scrollingVideo();

        try {
            JSONArray jsonArray = new JSONArray(bundle.getString(Events.IMAGES));
            if (jsonArray != null) {
                frameLayout.setVisibility(View.VISIBLE);
                image_layout.setVisibility(View.GONE);
                for (int i = 0; i < jsonArray.length(); i++)
                    listdata.add(jsonArray.getString(i));
                imageListsize = listdata.size();
                scrolling(listdata);
                Log.d("Fkgkdghdghdg", String.valueOf(listdata));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONArray jsonArray = new JSONArray(bundle.getString(Events.TERMS));
            for (int i = 0; i < jsonArray.length(); i++) {
                HashMap<String, String> map = new HashMap<String, String>();
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                map.put(TERMS_NAME, jsonObject.getString(TERMS_NAME));
                arraylist.add(map);
            }
            Log.d("array_list", arraylist.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        share_url = bundle.getString(Events.EVENT_LINK);
        custom_fields = bundle.getString(Events.CUSTOM_FIELDS);
        latitued = bundle.getString(Events.LOCATION_LATITUDE);
        Log.d("latitued", String.valueOf(latitued));

        longitude = bundle.getString(Events.LOCATION_LONGITUDE);
        Log.d("longitude", String.valueOf(longitude));
        try {
            JSONObject jsonObject = new JSONObject(custom_fields);
            str_date = jsonObject.getString(EVENT_START_DATE);
            end_date = jsonObject.getString(EVENT_END_DATE);
            str_time = jsonObject.getString(EVENT_START_TIME);
            end_time = jsonObject.getString(EVENT_END_TIME);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(Details.this, LinearLayoutManager.HORIZONTAL, false);
        term_recyclerView.setLayoutManager(horizontalLayoutManagaer);
        terms_adapter = new DetailsTermsAdapter(getApplicationContext(), arraylist);
        term_recyclerView.setAdapter(terms_adapter);


        ArrayList<String> stringArray;
        stringArray = new ArrayList<String>();
        stringArray.add(String.valueOf(R.drawable.splash_image));
        stringArray.add(String.valueOf(R.drawable.logo));
        stringArray.add(String.valueOf(R.drawable.splash_image));
        stringArray.add(String.valueOf(R.drawable.logo));


        event_name.setText(bundle.getString(Events.COMPANY_TAGLINE));
        event_content.setText(bundle.getString(Events.EVENT_CONTENT).replace("&nbsp;", "").replace("@mbps", "").replaceAll("\\n", ""));
        ticket_price.setText(bundle.getString(Events.COST));
        title.setText(bundle.getString(Events.EVENT_NAME));
        Log.d("Listing", bundle.getString(Events.EVENT_NAME) + " " + bundle.getString(Events.COMPANY_TAGLINE) + " " + bundle.getString(Events.LOCATION_ADDRESS));
        String location = bundle.getString(Events.LOCATION_ADDRESS);
        String name = bundle.getString(Events.EVENT_NAME);
        String tagline = bundle.getString(Events.COMPANY_TAGLINE);
        bundle0.putString("Location",location);
        bundle0.putString("Name", name);
        bundle0.putString("Tag",tagline);
        String listofcategories=arraylist.toString().replace("{","").replace("}","").replace("[","").replace("]","").replace("name=","");
        bundle0.putString("Category",listofcategories);
        mFirebaseAnalytics.logEvent("Listing", bundle0);
        mFirebaseAnalytics.setUserProperty("Listing",name+" "+tagline+" "+location);
        location_name.setText(bundle.getString(Events.EVENT_NAME));
        event_address.setText(bundle.getString(Events.LOCATION_ADDRESS));
        date_data = "";
        try {
            Date date = formatter.parse(str_date);
            date_data = formatter1.format(date);

            Calendar c = Calendar.getInstance();
            c.setTime(date);
            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
            daysname = checkDaysName(dayOfWeek);

            System.out.println(date_data);
            date_text.setText("Availability " + date_data);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (bundle.getString(Events.IS_FAVORITE).equals("1")) {
            save_image.setImageResource(R.drawable.save);
        } else {
            save_image.setImageResource(R.drawable.un_save);
        }

        try {
            Picasso.with(getApplicationContext()).load(bundle.getString(Events.EVENT_IMAGE_URL)).into(event_image);
        } catch (IllegalArgumentException e) {
            event_image.setImageResource(R.color.black);
        }

        System.out.println(str_time.matches("(.*):(.*)"));

        if (str_time.matches("(.*):(.*)")) {
            // str_time = str_time;
        } else {

            char c;
            String s = "";
            for (int i = 0; i < str_time.length(); i++) {

                if (i == 2) {
                    s = s + ":";
                    c = str_time.charAt(i);
                    s = s + String.valueOf(c);
                } else {

                    c = str_time.charAt(i);
                    s = s + String.valueOf(c);
                }
            }

            str_time = s;

        }

        if (end_time.matches("(.*):(.*)")) {

        } else {

            char c;
            String s = "";
            for (int i = 0; i < end_time.length(); i++) {

                if (i == 2) {
                    s = s + ":";
                    c = end_time.charAt(i);
                    s = s + String.valueOf(c);
                } else {

                    c = end_time.charAt(i);
                    s = s + String.valueOf(c);
                }
            }

            end_time = s;
        }

        if (!str_time.equals("")) {
            Log.d("str_time", str_time);

            String[] time = str_time.split(":");
            int str_hour = Integer.parseInt(time[0].trim());
            int str_min = Integer.parseInt(time[1].trim());

            DateFormat df = new SimpleDateFormat("HH:mm");
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR_OF_DAY, str_hour);
            cal.set(Calendar.MINUTE, str_min);
            cal.set(Calendar.SECOND, 0);

            while (!(df.format(cal.getTime())).equals(end_time)) {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("time", df.format(cal.getTime()));
                time_array_list.add(map);
                cal.add(Calendar.MINUTE, 30);
            }
            Log.d("time arry list", time_array_list.toString());
        }

        try {
            JSONArray product_jsonArray = new JSONArray(bundle.getString(Events.PRODUCT));
            Log.d("Product all data", String.valueOf(product_jsonArray));

            if (product_jsonArray.length() != 0) {
                Log.d("array type", "Full aary");

                for (int i = 0; i < product_jsonArray.length(); i++) {
                    JSONObject jsonObject = product_jsonArray.getJSONObject(i);
                    HashMap<String, String> map = new HashMap<String, String>();
                    booking_type = jsonObject.getString("type");
                    external_booking_url = jsonObject.getString("product_url");
                    description = jsonObject.getString("description");
                    product_price = jsonObject.getString("price");
                    product_id = jsonObject.getString("id");
                    _button_text = jsonObject.getString("_button_text");

                    map.put("type", booking_type);
                    map.put("product_url", external_booking_url);
                    map.put("description", description);
                    map.put("price", product_price);
                    map.put("id", product_id);
                    map.put("date", daysname + " " + date_data);
                    map.put("name", jsonObject.getString("name"));
                    map.put("_button_text", _button_text);
                    booking_arraylist.add(map);
                }

                if (booking_type.equals("external")) {
                    Log.d("array type", "Empty aray");
                    recycler_booking.setVisibility(View.GONE);
                    book_now.setText(_button_text);
                } else {
                    book_now.setVisibility(View.GONE);

                    LinearLayoutManager horizontalLayoutManagaer2 = new LinearLayoutManager(Details.this, LinearLayoutManager.VERTICAL, false);
                    recycler_booking.setLayoutManager(horizontalLayoutManagaer2);
                    bookingDetailsAdapter = new BookingDetailsAdapter(getApplicationContext(), booking_arraylist);
                    recycler_booking.setAdapter(bookingDetailsAdapter);
                    bookingDetailsAdapter.setClickListener(new OnItemClickListener() {
                        @Override
                        public void onClick(View view, int position) {
                            pos = position;
                            if (!booking_arraylist.get(pos).get("date").equals(" ")) {
                                booking_popoup();
                            } else {
                                after_booking_popoup();
                            }

                        }
                    });
                }

            } else {
                Log.d("array type", "Empty aray");
                recycler_booking.setVisibility(View.GONE);
                book_now.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    /*private void dialogBooking() {
        dialogBookingCount = new Dialog(getApplicationContext());
        dialogBookingCount.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogBookingCount.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        dialogBookingCount.setContentView(R.layout.dilog_booking_count);
        ListView list_view_bookingCount = (ListView) dialogBookingCount.findViewById(R.id.list_view_date);
        Button cancel = (Button) dialogBookingCount.findViewById(R.id.cancel);
        Button confirm = (Button) dialogBookingCount.findViewById(R.id.confirm);

        bookingCount_array_list.clear();
        for (int i = 1; i <= 10; i++) {
            System.out.println(i);
            bookingCount_array_list.add(String.valueOf(i));
        }

        Log.d("date_array_list", String.valueOf(bookingCount_array_list));
        boooking_count_adapter = new DetailsBookingCountAdapter(getApplication(), bookingCount_array_list);
        list_view_bookingCount.setAdapter(boooking_count_adapter);


       *//* list_view_bookingCount.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               // String selectedItem = (String) parent.getItemAtPosition(position);
                // tv.setText("Your selected fruit is : " + selectedItem);
                Log.d("count", String.valueOf( bookingCount_array_list.get(position)));
            }
        });*//*

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBookingCount.dismiss();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBookingCount.dismiss();
                new CreateOrder().execute();
                //Intent intent = new Intent(getApplication(), PaymentStripe.class);
                //intent.putExtra("product_price",product_price);
                // startActivity(intent);
            }
        });

        dialogBookingCount.show();
    }*/

    public void scrollingVideo() {
        try {
            JSONArray jsonArray = new JSONArray(bundle.getString(Events.VIDEO));
            if (jsonArray.length() != 0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    VideoList videoList = new VideoList();
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                    if (jsonObject.getString("Video_type").equals("youtube")) {
                        String id = jsonObject.getString("video_id");
                        String image = jsonObject.getString("video_image");
                        String type = jsonObject.getString("Video_type");

                        videoList.setId(id);
                        videoList.setImage(image);
                        videoList.setType(type);
                    }
                    if (jsonObject1.getString("Video_type").equals("uploaded")) {
                        String type1 = jsonObject1.getString("Video_type");
                        String dataUrl1 = jsonObject1.getString("video_data");

                        videoList.setVideoUrl(dataUrl1);
                        videoList.setType(type1);
                    }

                    videoLists.add(videoList);

                    DetailsVideoAdapter video_adapter = new DetailsVideoAdapter(getApplicationContext(), videoLists);
                    video_recyclerView.setAdapter(video_adapter);
                }
            } else {
                video_recyclerView.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void booking_popoup() {
        minteger = 2;
        dialog = new Dialog(Details.this);
        dialog.setCanceledOnTouchOutside(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        dialog.setContentView(R.layout.booking_popup);

        dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

        TextView heading_top = dialog.findViewById(R.id.heading_top);
        ImageView cross_icon = dialog.findViewById(R.id.cross_icon);
        TextView info_text = dialog.findViewById(R.id.info_text);
        TextView details_text = dialog.findViewById(R.id.details_text);
        TextView bookingfor = dialog.findViewById(R.id.bookingfor);
        booking_counting = dialog.findViewById(R.id.booking_counting);
        TextView people = dialog.findViewById(R.id.people);
        ImageView minus_icon = dialog.findViewById(R.id.minus_icon);
        ImageView plus_icon = dialog.findViewById(R.id.plus_icon);
        TextView time_and_day = dialog.findViewById(R.id.time_and_day);
        date_popup = dialog.findViewById(R.id.date_popup);
        TextView change_date_popup = dialog.findViewById(R.id.change_date_popup);
        RecyclerView recyclerView_time = dialog.findViewById(R.id.recyclerView_time);
        RelativeLayout bottombox = dialog.findViewById(R.id.bottombox);
        reserve_now = dialog.findViewById(R.id.reserve_now);
        scrollview = dialog.findViewById(R.id.scrollview);

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            //  Window window = this.getWindow();
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            dialog.getWindow().setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }

        info_text.setTypeface(hanken_Book);
        heading_top.setTypeface(openSans_Light);
        details_text.setTypeface(openSans_Light);
        booking_counting.setTypeface(hanken_Book);
        bookingfor.setTypeface(hanken_Book);
        people.setTypeface(hanken_Light);
        time_and_day.setTypeface(hanken_Book);
        date_popup.setTypeface(openSans_Light);
        reserve_now.setTypeface(hanken_Book);

        heading_top.setText(booking_arraylist.get(pos).get("name"));
        details_text.setText(booking_arraylist.get(pos).get("description").replace("&nbsp;", "").replace("@mbps", "").replaceAll("\\n", ""));
        scrollview.smoothScrollTo(0, 0);

        GridLayoutManager horizontalLayoutManagaer2 = new GridLayoutManager(Details.this, 5);
        recyclerView_time.setLayoutManager(horizontalLayoutManagaer2);
        time_adapter = new DetailsTimeAdapter(getApplicationContext(), time_array_list);
        recyclerView_time.setAdapter(time_adapter);


        time_and_day.setText("Time & day:");
        date_popup.setText("Availability " + daysname + " " + date_data);

        booking_counting.setText(String.valueOf(minteger));

        cross_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        minus_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (minteger > 0) {
                    minteger = minteger - 1;
                }
                booking_counting.setText(String.valueOf(minteger));
            }
        });
        plus_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (minteger < 500) {
                    minteger = minteger + 1;
                }
                booking_counting.setText(String.valueOf(minteger));
            }
        });
        change_date_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialogDate();
            }
        });

        reserve_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reservebundle.putString("Reserve", " ");
                mFirebaseAnalytics.logEvent(BOOK_CTA, reservebundle);
                firebaseAnalytics.setUserProperty(BOOK_CTA, "Reserve");
                booking_confirmation_popup();
            }
        });
        dialog.show();
    }

    public void booking_confirmation_popup() {
        dialog2 = new Dialog(Details.this);
        dialog2.setCanceledOnTouchOutside(false);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog2.setContentView(R.layout.booking_confirmation_popup);

        dialog2.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

        TextView booking_text = dialog2.findViewById(R.id.booking_text);
        TextView confirm_text = dialog2.findViewById(R.id.confirm_text);
        username_edit = dialog2.findViewById(R.id.username_edit);
        phone_no = dialog2.findViewById(R.id.phone_no);
        TextView cancel_btn = dialog2.findViewById(R.id.cancel_btn);
        TextView confirm_btn = dialog2.findViewById(R.id.confirm_btn);
        fill_all_the_details = dialog2.findViewById(R.id.fill_all_the_details);

        booking_text.setTypeface(openSans_Semibold);
        confirm_text.setTypeface(openSans_Light);
        username_edit.setTypeface(openSans_Light);
        phone_no.setTypeface(openSans_Light);
        cancel_btn.setTypeface(openSans_Regular);
        confirm_btn.setTypeface(openSans_Regular);
        fill_all_the_details.setTypeface(openSans_Regular);

        username_edit.setText(sharedPreference_main.getUser_Name().replace("null", ""));
        username_edit.setSelection(username_edit.getText().length());
        phone_no.setText(sharedPreference_main.getUser_Phone().replace("null", ""));

        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
                dialog.dismiss();
                scrollview.smoothScrollTo(0, 0);
                dialog.show();
            }
        });

        confirm_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                booking_username = username_edit.getText().toString();
                booking_phone_no = phone_no.getText().toString();
                booking_count = booking_counting.getText().toString();
                product_price = booking_arraylist.get(pos).get("price").replace("£", "");

                if (booking_username.length() == 0 && booking_phone_no.length() == 0) {
                    fill_all_the_details.setVisibility(View.VISIBLE);
                } else if (booking_username.length() == 0) {
                    fill_all_the_details.setVisibility(View.VISIBLE);
                } else if (booking_phone_no.length() == 0) {
                    fill_all_the_details.setVisibility(View.VISIBLE);
                } else {
                    dialog.dismiss();
                    dialog2.dismiss();
                    fill_all_the_details.setVisibility(View.GONE);
                    new CreateOrder().execute();
                }
            }
        });

        username_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                booking_username = username_edit.getText().toString();
                booking_phone_no = phone_no.getText().toString();
                // password = pass.getText().toString();
                if (s.length() > 0) {
                    if (booking_username.length() == 0 && booking_phone_no.length() == 0) {
                        fill_all_the_details.setVisibility(View.VISIBLE);
                    } else if (booking_username.length() == 0) {
                        fill_all_the_details.setVisibility(View.VISIBLE);
                    } else if (booking_phone_no.length() == 0) {
                        fill_all_the_details.setVisibility(View.VISIBLE);
                    } else {
                        fill_all_the_details.setVisibility(View.GONE);

                    }
                } else {
                    fill_all_the_details.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        phone_no.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                booking_username = username_edit.getText().toString();
                booking_phone_no = phone_no.getText().toString();
                // password = pass.getText().toString();
                if (s.length() > 0) {
                    if (booking_username.length() == 0 && booking_phone_no.length() == 0) {
                        fill_all_the_details.setVisibility(View.VISIBLE);
                    } else if (booking_username.length() == 0) {
                        fill_all_the_details.setVisibility(View.VISIBLE);
                    } else if (booking_phone_no.length() == 0) {
                        fill_all_the_details.setVisibility(View.VISIBLE);
                    } else {
                        fill_all_the_details.setVisibility(View.GONE);

                    }
                } else {
                    fill_all_the_details.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        dialog2.show();
    }

    public void after_booking_popoup() {
        minteger1 = 2;
        dialog1 = new Dialog(Details.this);
        dialog1.setCanceledOnTouchOutside(false);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog1.setContentView(R.layout.after_booking_popup);

        dialog1.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            //  Window window = this.getWindow();
            dialog1.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            dialog1.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            dialog1.getWindow().setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }


        TextView heading_top1 = dialog1.findViewById(R.id.heading_top1);
        ImageView cross_icon1 = dialog1.findViewById(R.id.cross_icon1);
        TextView info_text1 = dialog1.findViewById(R.id.info_text1);
        TextView details_text1 = dialog1.findViewById(R.id.details_text1);
        final TextView booking = dialog1.findViewById(R.id.booking);
        ImageView minus_icon1 = dialog1.findViewById(R.id.minus_icon1);
        booking_counting1 = dialog1.findViewById(R.id.booking_counting1);
        ImageView plus_icon1 = dialog1.findViewById(R.id.plus_icon1);
        TextView tickets = dialog1.findViewById(R.id.tickets);
        TextView total = dialog1.findViewById(R.id.total);
        pricebutton = dialog1.findViewById(R.id.price);
        Button pay_now = dialog1.findViewById(R.id.pay_now);
        RelativeLayout bottombox1 = dialog1.findViewById(R.id.bottombox1);
        ScrollView scrollview1 = dialog1.findViewById(R.id.scrollview1);

        heading_top1.setTypeface(openSans_Light);
        info_text1.setTypeface(hanken_Book);
        details_text1.setTypeface(openSans_Light);
        booking.setTypeface(hanken_Book);
        booking_counting1.setTypeface(hanken_Book);
        tickets.setTypeface(hanken_Light);
        total.setTypeface(hanken_Book);
        pricebutton.setTypeface(hanken_Book);
        pay_now.setTypeface(hanken_Book);

        booking_counting1.setText(String.valueOf(minteger1));

        heading_top1.setText(booking_arraylist.get(pos).get("name"));
        details_text1.setText(booking_arraylist.get(pos).get("description").replace("&nbsp;", "").replace("@mbps", "").replaceAll("\\n", ""));
        scrollview1.smoothScrollTo(0, 0);

        product_price = booking_arraylist.get(pos).get("price");

        if (product_price != null) {
            pricebutton.setText("£" + product_price);
        } else {
            pricebutton.setText("£" + product_price);
        }
        // product_price = booking_arraylist.get(pos).get("price").replace("£", "");

        //  booking_counting1.setText(booking_counting.getText().toString());

        //   minteger1 = Integer.parseInt(booking_counting.getText().toString());

       /* String pricestart = String.valueOf(Integer.parseInt(product_price) * minteger1);
        pricebutton.setText("£" + pricestart);*/

        cross_icon1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();
            }
        });
        minus_icon1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer.parseInt(booking_counting1.getText().toString());
                if (minteger1 > 0) {
                    minteger1 = minteger1 - 1;
                }

                booking_counting1.setText(String.valueOf(minteger1));
            }
        });
        plus_icon1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (minteger1 < 500) {
                    minteger1 = minteger1 + 1;
                }
                booking_counting1.setText(String.valueOf(minteger1));
            }
        });

        pay_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // dialog1.dismiss();
                booking_username = sharedPreference_main.getUser_Name();
                booking_phone_no = sharedPreference_main.getUser_Phone();
                booking_count = booking_counting1.getText().toString();
                product_price = booking_arraylist.get(pos).get("price").replace("£", "");
                new CreateOrder().execute();
            }
        });


        dialog1.show();
    }

    public void scrolling(ArrayList<String> list) {
        adapter = new ViewPagerAdapter(this, list);
        viewpager.setAdapter(adapter);
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(viewpager);
        viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                //   Toast.makeText(getApplicationContext(), "context changed", Toast.LENGTH_SHORT).show();

               /* if (state == ViewPager.SCROLL_STATE_IDLE) {
                    int pageCount = img.length;
                    if (currentPage == 0) {
                        viewpager.setCurrentItem(pageCount - 1, false);
                    } else if (currentPage == pageCount - 1) {
                        viewpager.setCurrentItem(0, false);
                    }
                }*/
            }
        });
        leftNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = viewpager.getCurrentItem();
                if (tab > 0) {
                    tab--;
                    viewpager.setCurrentItem(tab);
                    rightNav.setVisibility(View.VISIBLE);
                } else if (tab == 0) {
                    viewpager.setCurrentItem(tab);
                    leftNav.setVisibility(View.GONE);
                }
            }
        });
        // Images right navigatin
        rightNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = viewpager.getCurrentItem();
                tab++;
                viewpager.setCurrentItem(tab);
                leftNav.setVisibility(View.VISIBLE);
                if (tab == imageListsize - 1) {
                    viewpager.setCurrentItem(tab);
                    rightNav.setVisibility(View.GONE);
                }
            }

        });
    }

    private void init() {

        leftNav = (ImageView) findViewById(R.id.left_nav);
        rightNav = (ImageView) findViewById(R.id.right_nav);
        viewpager = (ViewPager) findViewById(R.id.pager);


        title = (TextView) findViewById(R.id.title);
        term_recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        video_recyclerView = (RecyclerView) findViewById(R.id.recyclerVideo);
        recycler_booking = (RecyclerView) findViewById(R.id.recycler_booking);
        event_name = (TextView) findViewById(R.id.event_name);
        event_content = (TextView) findViewById(R.id.event_content);
        event_image = (ImageView) findViewById(R.id.event_image);
        ticket_price = (TextView) findViewById(R.id.ticket_price);
        change_date = (TextView) findViewById(R.id.change_date);
        //  date_layout = (RelativeLayout) findViewById(R.id.date_layout);
        image_layout = (RelativeLayout) findViewById(R.id.image_layout);
        frameLayout = (FrameLayout) findViewById(R.id.view_all_sug);
        date_text = (TextView) findViewById(R.id.date);

        location_name = (TextView) findViewById(R.id.location_name);
        event_address = (TextView) findViewById(R.id.event_address);
        book_now = (Button) findViewById(R.id.book_now);

        share_layout = (RelativeLayout) findViewById(R.id.share_layout);
        // comments_layout = (RelativeLayout) findViewById(R.id.comments_layout);
        save_layout = (RelativeLayout) findViewById(R.id.save_layout);
        save_image = (ImageView) findViewById(R.id.save_image);

        music = (RelativeLayout) findViewById(R.id.music);
        drinks = (RelativeLayout) findViewById(R.id.drinks);
        food = (RelativeLayout) findViewById(R.id.food);
        cultural = (RelativeLayout) findViewById(R.id.cultural);
        top_pics = (RelativeLayout) findViewById(R.id.top_pics);

        cancel_icon = (ImageView) findViewById(R.id.cancel_icon);
        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


       /* SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);*/

        // select_time = (TextView) findViewById(R.id.select_time);
        recommended = (TextView) findViewById(R.id.recommended);
        recommended_text = (TextView) findViewById(R.id.recommended_text);

        home_text = (TextView) findViewById(R.id.home_text);
        find_text = (TextView) findViewById(R.id.find_text);
        nearby_text = (TextView) findViewById(R.id.nearby_text);
        save_text = (TextView) findViewById(R.id.save_text);
        more_text = (TextView) findViewById(R.id.more_text);

        home_lay = (RelativeLayout) findViewById(R.id.home_lay);
        find_lay = (RelativeLayout) findViewById(R.id.find_lay);
        nearby_lay = (RelativeLayout) findViewById(R.id.nearby_lay);
        save_lay = (RelativeLayout) findViewById(R.id.save_lay);
        more_lay = (RelativeLayout) findViewById(R.id.more_lay);

        bottom_box1 = (TextView) findViewById(R.id.bottom_box1);
        bottom_box2 = (TextView) findViewById(R.id.bottom_box2);
        bottom_box3 = (TextView) findViewById(R.id.bottom_box3);
        bottom_box4 = (TextView) findViewById(R.id.bottom_box4);
        bottom_box5 = (TextView) findViewById(R.id.bottom_box5);


        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }

    }

    private void onclick() {

        home_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle1.putString("Home", " ");
                firebaseAnalytics.logEvent(BOTTOM_NAV, bundle1);
                firebaseAnalytics.setUserProperty(BOTTOM_NAV, "Home");
                startActivity(new Intent(getApplicationContext(), Home.class));
            }
        });

        find_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle1.putString("The_List", " ");
                firebaseAnalytics.logEvent(BOTTOM_NAV, bundle1);
                firebaseAnalytics.setUserProperty(BOTTOM_NAV, "The_List");
                startActivity(new Intent(getApplicationContext(), Events.class));
            }
        });

        nearby_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle1.putString("NearBy", " ");
                firebaseAnalytics.logEvent(BOTTOM_NAV, bundle1);
                firebaseAnalytics.setUserProperty(BOTTOM_NAV, "NearBy");
                startActivity(new Intent(getApplicationContext(), Nearby.class));
            }
        });

        save_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle1.putString("Save", " ");
                firebaseAnalytics.logEvent(BOTTOM_NAV, bundle1);
                firebaseAnalytics.setUserProperty(BOTTOM_NAV, "Save");
                startActivity(new Intent(getApplicationContext(), More.class));
            }
        });
        more_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle1.putString("More", " ");
                firebaseAnalytics.logEvent(BOTTOM_NAV, bundle1);
                firebaseAnalytics.setUserProperty(BOTTOM_NAV, "More");
                startActivity(new Intent(getApplicationContext(), More.class));
            }
        });


        book_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewsitebundle.putString("ViewSite", " ");
                mFirebaseAnalytics.logEvent(BOOK_CTA, viewsitebundle);
                firebaseAnalytics.setUserProperty(BOOK_CTA, "ViewSite");
                if (booking_type.equals("external")) {
                    Uri uri = Uri.parse(bundle.getString(Events.COMPANY_WEBSITE)); // missing 'http://' will cause crashed
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                } else {
                    //dialogBooking();
                    booking_popoup();//booking popup
                }
            }
        });

        change_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogDate();
            }
        });
        cancel_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        share_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sharebundle.putString("Share", " ");
                mFirebaseAnalytics.logEvent("Share",sharebundle);
                Intent share = new Intent(android.content.Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                share.putExtra(Intent.EXTRA_TEXT, share_url);
                startActivity(Intent.createChooser(share, "Share link!"));
            }
        });
       /* comments_layout.setOnClickListener(new View.OnClickListener() {
            @Overrideselected_location
            public void onClick(View view) {

            }
        });*/

        save_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SavedJSON().execute();
            }
        });

        music.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplication(), Events.class);
                intent.putExtra("selected_categories", "live");
                intent.putExtra("selected_type", "categories");
                startActivity(intent);
            }
        });
        drinks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Events.class);
                intent.putExtra("selected_categories", "drinks");
                intent.putExtra("selected_type", "categories");
                startActivity(intent);

            }
        });
        food.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Events.class);
                intent.putExtra("selected_categories", "food");
                intent.putExtra("selected_type", "categories");
                startActivity(intent);

            }
        });
        cultural.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Events.class);
                intent.putExtra("selected_categories", "art,craft");
                intent.putExtra("selected_type", "categories");
                startActivity(intent);

            }
        });
        top_pics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Events.class);
                intent.putExtra("selected_categories", "top picks");
                intent.putExtra("selected_type", "categories");
                startActivity(intent);

            }
        });
    }

    private void progressBar() {
        progress = new ProgressDialog(Details.this);
        progress.setMessage("Loading...");
        progress.setIndeterminate(true);
        progress.setCanceledOnTouchOutside(false);
        progress.show();
    }

    private void dialogDate() {
        date_array_list = new ArrayList();
        dialogDateMode = new Dialog(Details.this);
        dialogDateMode.setCanceledOnTouchOutside(false);
        dialogDateMode.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogDateMode.setContentView(R.layout.dilog_date);
        ListView list_view_date = (ListView) dialogDateMode.findViewById(R.id.list_view_date);

        List<Date> dates = new ArrayList<Date>();
        Date startDate = null;
        Date endDate = null;
        try {
            startDate = (Date) formatter.parse(str_date);
            endDate = (Date) formatter.parse(end_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (!str_date.equals("")) {
            long interval = 24 * 1000 * 60 * 60; // 1 hour in millis
            long endTime = endDate.getTime(); // create your endtime here, possibly using Calendar or Date
            long curTime = startDate.getTime();
            while (curTime <= endTime) {
                dates.add(new Date(curTime));
                curTime += interval;
            }
            for (int i = 0; i < dates.size(); i++) {
                Date lDate = (Date) dates.get(i);
                String ds = formatter1.format(lDate);
                System.out.println(ds);
                date_array_list.add(ds);
            }
            Log.d("date_array_list", String.valueOf(date_array_list));
            date_adapter = new DetailsDateAdapter(getApplication(), date_array_list);
            list_view_date.setAdapter(date_adapter);
            dialogDateMode.show();
        }

    }

    public String checkDaysName(int days) {
        switch (days) {
            case 1:
                day = "Sunday";
                break;
            case 2:
                day = "Monday";
                break;
            case 3:
                day = "Tuesday";
                break;
            case 4:
                day = "Wednesday";
                break;
            case 5:
                day = "Thursday";
                break;
            case 6:
                day = "Friday";
                break;
            case 7:
                day = "Saturday";
                break;
        }
        return day;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng lat = new LatLng(Double.valueOf(latitued), Double.valueOf(longitude));
        //mMap.getUiSettings().setZoomGesturesEnabled(true);
        mMap.addMarker(new MarkerOptions().position(lat));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(lat));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lat, 18.0f));
    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();

        if (dialogDateMode == null) {
        } else {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
            dialogDateMode.dismiss();
//            dialogRefineType.dismiss();

        }

    }

    // SavedJSON AsyncTask
    public class SavedJSON extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar();
        }

        @Override
        protected Void doInBackground(Void... params) {
            arraylist = new ArrayList<HashMap<String, String>>();
            OkHttpClient client = new OkHttpClient();
            MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
            RequestBody body = RequestBody.create(mediaType, "listing_id=" + bundle.getString(Events.EVENT_ID) + "&user_id=" + sharedPreference_main.getUser_Id().toString());
            Request request = new Request.Builder()
                    .url(Home.MAIN_APPLICATION_URL + "listing/update-favorites")
                    .post(body)
                    .addHeader("content-type", "application/x-www-form-urlencoded")
                    .addHeader("accept", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .build();
            try {
                Response response = client.newCall(request).execute();
                Log.d("responstr", String.valueOf(response.code()));
                String fulljson = response.body().string();
                Log.d("body", fulljson);
                JSONArray jsonArray = new JSONArray(fulljson);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    massage = jsonObject.getString("message");
                    Log.d("massage", massage);
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            progress.dismiss();
            Toast.makeText(getApplicationContext(), massage, Toast.LENGTH_LONG).show();
            if (massage.equals("Favorite has been removed.")) {
                save_image.setImageResource(R.drawable.un_save);
            } else if (massage.equals("Favorite has been inserted.")) {
                save_image.setImageResource(R.drawable.save);
            }
        }
    }

    // CreateOrder AsyncTask
    private class CreateOrder extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar();

        }

        @Override
        protected Void doInBackground(Void... params) {

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Home.MAIN_APPLICATION_URL + "listing/order");

            try {
                JSONObject jsonObject1 = new JSONObject();
                jsonObject1.put("product_id", product_id);
                jsonObject1.put("quantity", booking_count);
                jsonObject1.put("start_date_time", order_date + " " + order_time);
                JSONArray jsonArray1 = new JSONArray();
                jsonArray1.put(jsonObject1);
                JSONObject jsonObject2 = new JSONObject();
                jsonObject2.put("first_name", booking_username);
                jsonObject2.put("phone", booking_phone_no);
                jsonObject2.put("address_1", sharedPreference_main.getUser_Address().toString());
                jsonObject2.put("postcode", sharedPreference_main.getUser_Zip().toString());
                jsonObject2.put("email", sharedPreference_main.getUser_Email().toString());
                JSONObject jsonObject3 = new JSONObject();
                jsonObject3.put("product", jsonObject1);
                jsonObject3.put("user_id", sharedPreference_main.getUser_Id().toString());
                jsonObject3.put("address", jsonObject2);
                Log.d("jasonObjecnt", String.valueOf(jsonObject3));


                httppost.setEntity((new ByteArrayEntity(jsonObject3.toString().getBytes("UTF8"))));
                httppost.setHeader("accept", "application/json");
                httppost.setHeader("cache-control", "no-cache");

                HttpResponse response = httpclient.execute(httppost);
                Log.d("response_code", String.valueOf(response.getStatusLine()));
                HttpEntity resEntityGet = response.getEntity();

                if (resEntityGet != null) {
                    String response_data = EntityUtils.toString(resEntityGet);
                    JSONArray jsonArray = new JSONArray(response_data);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);

                    if (jsonObject.has("status")) {
                        order_status = jsonObject.getString("status");
                        order_message = jsonObject.getString("message");
                        Log.d("statius", order_status);
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            progress.dismiss();
            if (order_status.equals("0")) {
                Toast.makeText(getApplication(), order_message, Toast.LENGTH_SHORT).show();
            } else {
                /*Bundle bundle = new Bundle();
                bundle.putString("user_id", sharedPreference_main.getUser_Id());
                bundle.putString("Booking_product_id", product_id);
                bundle.putString("Booking_quantity", booking_count);
                bundle.putString("Order_Date", order_date + " " + order_time);
                mFirebaseAnalytics.logEvent(BOOKINGNOW, bundle);*/
                bookingbundle.putString("Complete_Booking", " ");
                firebaseAnalytics.logEvent("Complete_Booking", bookingbundle);
                firebaseAnalytics.setUserProperty("CompleteBooking"," ");
                Intent intent = new Intent(getApplication(), PaymentStripe.class);
                intent.putExtra("product_price", product_price);
                startActivity(intent);
            }
        }
    }

    public static void getDate(String date, String day) {
        date_data = date;
        dialogDateMode.dismiss();
        date_text.setText("Availability " + date_data);
        if (date_popup != null) {
            date_popup.setText("Availability " + day + " " + date_data);
        }

        DateFormat formatter3 = new SimpleDateFormat("dd MMM yyyy");
        DateFormat formatter4 = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date3 = formatter3.parse(date_data);
            order_date = formatter4.format(date3);
            System.out.println(order_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public static void getTime(String time) {
        time_data = time;
        order_time = time_data + ":00";
        System.out.println(order_time);

    }

    public static void getchangeButton(int change) {
        if (change == 1) {
            reserve_now.setBackgroundColor(Color.parseColor("#8E90A8"));
            reserve_now.setTextColor(Color.parseColor("#ffffff"));
            reserve_now.setEnabled(true);
        }

    }

    public static void getBookingCount(String count) {
        booking_count = count;
        Log.d("booking_count", booking_count);
    }

    public static void checkBoxItemPosition(Integer item) {
        item_position = item;
    }

}
