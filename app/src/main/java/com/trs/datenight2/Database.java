package com.trs.datenight2;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by trs on 13-Jul-18. 
 */

public class Database extends SQLiteOpenHelper {


    static String DATABASE_NAME = "datenight";
    public static final String TABLE_NAME = "search_history";
    public static final String KEY_ID = "id";
    public static final String PRODUCT_ID = "product_id";
    public static final String Product_NAME = "produc_tname";


    public Database(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String HISTORY_TABLE = "CREATE TABLE " + TABLE_NAME + " (" + KEY_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT, " + Product_NAME + " TEXT)";


        db.execSQL(HISTORY_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }


}
