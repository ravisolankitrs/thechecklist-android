package com.trs.datenight2;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.exception.AuthenticationException;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputWidget;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;


public class PaymentStripe extends Activity {

    public static final String BOOKINGSTEPS = "Final_Booking_Step";
    Bundle finalbundle;
    TextView title;
    CardInputWidget mCardInputWidget;
    Button submit;
    ProgressDialog progress;

    ImageView cancel_icon;

    String stripe_key = "pk_test_cXaLJ6ZCeH5o0TrZoVAELN5x";
    String stripe_token="";
    String stripe_amount="";
    String stripe_currency ="GBP";
    String stripe_description =" ";
    String payment_status="";
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.paymen_stripe);
        mFirebaseAnalytics=FirebaseAnalytics.getInstance(this);
        finalbundle=new Bundle();
        init();
        onclick();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Typeface hanken_light = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Hanken-Light.ttf");
        title.setTypeface(hanken_light);

        Intent i = new Intent();
        if(i.getExtras() != null){
            stripe_amount = i.getStringExtra("product_price");
            Log.d("stripe_amount",stripe_amount);
        }
    }


    private void init() {
        title = (TextView) findViewById(R.id.title);
        cancel_icon = (ImageView) findViewById(R.id.cancel_icon);
        mCardInputWidget = (CardInputWidget) findViewById(R.id.card_input_widget);
        submit = (Button) findViewById(R.id.submit);

    }
    private void onclick() {

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Card card = mCardInputWidget.getCard();
                //Card card = new Card("4242-4242-4242-4242", 12, 2019, "123");
                if (card == null) {
                    Toast.makeText(getApplicationContext(), "Invalid Card details", Toast.LENGTH_LONG).show();
                } else {
                    progressBar();
                    Stripe stripe = null;
                    try {
                        stripe = new Stripe(getApplicationContext(), stripe_key );
                    } catch (AuthenticationException e) {
                        e.printStackTrace();
                    }

                    stripe.createToken(card,
                            new TokenCallback() {
                                public void onSuccess(Token token) {
                                    Log.d("Token id", token.getId());
                                    stripe_token = token.getId();
                                    new DownloadJSON().execute();
                                }
                                public void onError(Exception error) {
                                    Log.d("error", String.valueOf(error));
                                    Log.d("Stripe", error.getLocalizedMessage());
                                    progress.dismiss();
                                    Toast.makeText(getApplicationContext(), "Payment Unsuccessful", Toast.LENGTH_LONG).show();
                                }
                            }
                    );
                }
            }
        });

        cancel_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    // DownloadJSON AsyncTask
   private class DownloadJSON extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            OkHttpClient client = new OkHttpClient();
            MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
            RequestBody body = RequestBody.create(mediaType, "stripe_token="+stripe_token+"&amount="+stripe_amount+"&currency%20="+stripe_currency+"&description%20="+ stripe_description);

            Request request = new Request.Builder()
                    .url(Home.MAIN_APPLICATION_URL  + "listing/stripe-details")
                    .post(body)
                    .addHeader("Content-Type", "application/x-www-form-urlencoded")
                    .addHeader("Cache-Control", "no-cache")
                    .build();
            try {
                Response response = client.newCall(request).execute();
                Log.d("response", String.valueOf(response.code()));
                String fulljson = response.body().string();
                Log.d("body", fulljson);

                try {
                    JSONObject jsonObject = new JSONObject(fulljson);
                    payment_status = jsonObject.getString("status");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            progress.dismiss();

            if (payment_status.equals("successful")){
                finalbundle.putString("Final_Booking_Step","Completed");
                mFirebaseAnalytics.logEvent(BOOKINGSTEPS,finalbundle);
                Toast.makeText(getApplicationContext(), "Payment Successfully Completed", Toast.LENGTH_LONG).show();
                startActivity(new Intent(getApplicationContext(), Home.class));
            }else {
                Toast.makeText(getApplicationContext(), "Payment Unsuccessful", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void progressBar() {
        progress = new ProgressDialog(PaymentStripe.this);
        progress.setMessage("Loading...");
        progress.setIndeterminate(true);
        progress.setCanceledOnTouchOutside(false);
        progress.show();
    }

}
