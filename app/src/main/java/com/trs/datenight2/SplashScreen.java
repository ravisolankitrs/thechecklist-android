package com.trs.datenight2;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SplashScreen extends Activity {

    SharedPreference_main sharedPreference_main;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splashscreen);
        sharedPreference_main = new SharedPreference_main(this);

        // Generate hash key for facebook
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                System.out.print("ishdfs " + Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        Thread logotimer = new Thread() {
            public void run() {
                try {
                    int logotimer = 0;
                    while (logotimer < 3000) {
                        sleep(100);
                        logotimer = logotimer + 100;
                    }

                    if (sharedPreference_main.getLogin().toString().equals("successful")) {
                        Intent _intent = new Intent(SplashScreen.this, Home.class);
                        startActivity(_intent);
                        SplashScreen.this.finish();
                    } else {
                        Intent _intent = new Intent(SplashScreen.this, Login.class);
                        startActivity(_intent);
                        SplashScreen.this.finish();
                    }


                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        };
        logotimer.start();


        sharedPreference_main.settickicon("");
        sharedPreference_main.setcheckdata("");
    }

}
