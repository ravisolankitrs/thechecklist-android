package com.trs.datenight2;

/**
 * Created by trs on 26-Jun-18.
 */

public class VideoList {
    public String image;
    public String text;
    public String id;
    public String type;
    public String VideoUrl;
    public String imageurl;


    public VideoList(String id, String image) {
        this.id = id;
        this.image = image;
    }

    public VideoList() {

    }

    public String getVideoUrl() {
        return VideoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        VideoUrl = videoUrl;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }
}
