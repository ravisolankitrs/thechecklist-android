package com.trs.datenight2;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Sandeep on 1/18/2018.
 */
public class DetailsTermsAdapter extends RecyclerView.Adapter<DetailsTermsAdapter.CustomViewHolder> {

    ArrayList<HashMap<String, String>> data;
    private Context context;
    private LayoutInflater layoutInflater;
    HashMap<String, String> resultp = new HashMap<String, String>();

    // Provide a suitable constructor (depends on the kind of dataset)
    public DetailsTermsAdapter(Context context, ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        this.data = arraylist;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.details_terms_list, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int position) {
        resultp = data.get(position);
        Typeface hanken_Light = Typeface.createFromAsset(context.getAssets(), "fonts/Hanken-Light.ttf");
        holder.term_name.setTypeface(hanken_Light);
        holder.term_name.setText(resultp.get(Details.TERMS_NAME));

    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        TextView term_name;
        public CustomViewHolder(View itemView) {
            super(itemView);
            term_name = (TextView) itemView.findViewById(R.id.term_name);
        }
    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return data.size();
    }
}

