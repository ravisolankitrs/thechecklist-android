package com.trs.datenight2;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;

import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.List;

public class Home extends Activity {
    public static final String USERDETAILS = "user_details";
    public static final String BOTTOMNAV = "Bottom_Nav";
    public static final String HOMEPAGESELECTION = "Home_Nav";
    public final static int REQUEST_CODE = 65635;
    Bundle bundle,bundle1,bundle2;
    private static final int PERMISSION_REQUEST_CODE = 1;

    // application service url
    static String APPLICATION_URL = "http://ec2-52-56-167-11.eu-west-2.compute.amazonaws.com/";
    static String BOOKTICKET_METHO = "events/api/getUserTickets/bookTicket";
    //static String MAIN_APPLICATION_URL = "http://datenight.techchats.online/wp-json/api/v1/";
    static String MAIN_APPLICATION_URL = "http://thechecklist.london/wp-json/api/v1/";
    static String LOGIN_API = "user/login";
    static String REGISTER_API = "user/register";
    SharedPreference_main sharedPreference_main;
    View view;
    RelativeLayout view_all_sug, top_pics, places_eat, live_music, find_bar, hint_tips, get_cultural, groups_event, datenight_group;
    TextView title;
    TextView t1, t2, t3, t4, t5, t6, t7;
    TextView home_text, find_text, nearby_text, save_text, more_text;
    RelativeLayout home_lay, find_lay, nearby_lay, save_lay, more_lay;
    String[] permissions = new String[]{
            android.Manifest.permission.INTERNET,
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.ACCESS_FINE_LOCATION,
            android.Manifest.permission.ACCESS_COARSE_LOCATION,
            android.Manifest.permission.ACCESS_NETWORK_STATE,
            android.Manifest.permission.SYSTEM_ALERT_WINDOW,
    };
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        init();
        onclick();
        sharedPreference_main = new SharedPreference_main(this);
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        bundle = new Bundle();
        bundle1 = new Bundle();
        bundle2 = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, sharedPreference_main.getUser_Id());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, sharedPreference_main.getUser_Name());
        mFirebaseAnalytics.logEvent(USERDETAILS, bundle);

        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(2000);
        mFirebaseAnalytics.setSessionTimeoutDuration(30000);

        //Sets the user ID property.
        mFirebaseAnalytics.setUserId(sharedPreference_main.getUser_Id());

        //Sets a user property to a given value.
        mFirebaseAnalytics.setUserProperty("User_Name", sharedPreference_main.getUser_Name());

        Typeface hanken_light = Typeface.createFromAsset(getApplication().getAssets(), "fonts/Hanken-Light.ttf");
        title.setTypeface(hanken_light);

        // t6.setText("Hints & Tips");
        t5.setText("Arts & Crafts");
        t2.setText("Classes");

        t1.setTypeface(hanken_light);
        t2.setTypeface(hanken_light);
        t3.setTypeface(hanken_light);
        t4.setTypeface(hanken_light);
        t5.setTypeface(hanken_light);
        t6.setTypeface(hanken_light);
        t7.setTypeface(hanken_light);

        home_text.setTypeface(hanken_light);
        find_text.setTypeface(hanken_light);
        nearby_text.setTypeface(hanken_light);
        save_text.setTypeface(hanken_light);
        more_text.setTypeface(hanken_light);


//        if (Build.VERSION.SDK_INT >= 23)
//        {
//            if (checkPermission())
//            {
//                // Code for above or equal 23 API Oriented Device
//                // Your Permission granted already .Do next code
//            } else {
//                requestPermission(); // Code for permission
//            }
//        }
//        else
//        {
//
//            // Code for Below 23 API Oriented Device
//            // Do next code
//        }
        /*Bundle params = new Bundle();
        params.putString("invalid_url", "test");
        mFirebaseAnalytics.logEvent("eventInvalidUrl", params);*/

        checkPermissions();
        checkDrawOverlayPermission();

    }

    public void checkDrawOverlayPermission() {
        /** check if we already  have permission to draw over other apps */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(this)) {
                /** if not construct intent to request permission */
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                /** request permission via start activity for result */
                startActivityForResult(intent, REQUEST_CODE);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        /** check if received result code
         is equal our requested code for draw permission  */
        if (requestCode == REQUEST_CODE) {
            // ** if so check once again if we have permission */
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (Settings.canDrawOverlays(this)) {
                    // continue here - permission was granted
                    // goYourActivity();
                }
            }
        }
    }


    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 100);
            return false;
        }
        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == 100) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // do something
            }
            return;
        }
    }


//    private boolean checkPermission() {
//        int result = ContextCompat.checkSelfPermission(Home.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
//        if (result == PackageManager.PERMISSION_GRANTED) {
//            return true;
//        } else {
//            return false;
//        }
//    }
//
//    private void requestPermission() {
//
//        if (ActivityCompat.shouldShowRequestPermissionRationale(Home.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//            Toast.makeText(Home.this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
//        } else {
//            ActivityCompat.requestPermissions(Home.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
//        }
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
//        switch (requestCode) {
//            case PERMISSION_REQUEST_CODE:
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    Log.e("value", "Permission Granted, Now you can use local drive .");
//                } else {
//                    Log.e("value", "Permission Denied, You cannot use local drive .");
//                }
//                break;
//        }
//    }

    private void init() {
        title = (TextView) findViewById(R.id.title);
        view_all_sug = (RelativeLayout) findViewById(R.id.view_all_sug);
        top_pics = (RelativeLayout) findViewById(R.id.top_pics);
        places_eat = (RelativeLayout) findViewById(R.id.places_eat);
        // live_music = (RelativeLayout) findViewById(R.id.live_music);
        // find_bar = (RelativeLayout) findViewById(R.id.find_bar);
        hint_tips = (RelativeLayout) findViewById(R.id.hint_tips);
        get_cultural = (RelativeLayout) findViewById(R.id.get_cultural);
        groups_event = (RelativeLayout) findViewById(R.id.groups_event);

        t1 = (TextView) findViewById(R.id.t1);
        t2 = (TextView) findViewById(R.id.t2);
        t3 = (TextView) findViewById(R.id.t3);
        t4 = (TextView) findViewById(R.id.t4);
        t5 = (TextView) findViewById(R.id.t5);
        t6 = (TextView) findViewById(R.id.t6);
        t7 = (TextView) findViewById(R.id.t7);

        home_text = (TextView) findViewById(R.id.home_text);
        find_text = (TextView) findViewById(R.id.find_text);
        nearby_text = (TextView) findViewById(R.id.nearby_text);
        save_text = (TextView) findViewById(R.id.save_text);
        more_text = (TextView) findViewById(R.id.more_text);

        home_lay = (RelativeLayout) findViewById(R.id.home_lay);
        find_lay = (RelativeLayout) findViewById(R.id.find_lay);
        nearby_lay = (RelativeLayout) findViewById(R.id.nearby_lay);
        save_lay = (RelativeLayout) findViewById(R.id.save_lay);
        more_lay = (RelativeLayout) findViewById(R.id.more_lay);
        datenight_group = (RelativeLayout) findViewById(R.id.datenight_group);

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    private void onclick() {


        home_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        find_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle1.putString("The_List", " ");
                mFirebaseAnalytics.logEvent(BOTTOMNAV,bundle1);
                mFirebaseAnalytics.setUserProperty(BOTTOMNAV, "The_List");
                startActivity(new Intent(getApplicationContext(), Events.class));
            }
        });


        nearby_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle1.putString("NearBy"," ");
                mFirebaseAnalytics.logEvent(BOTTOMNAV,bundle1);
                mFirebaseAnalytics.setUserProperty(BOTTOMNAV, "NearBy");
                startActivity(new Intent(getApplicationContext(), Nearby.class));
            }
        });

        save_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle1.putString("Save", " ");
                mFirebaseAnalytics.logEvent(BOTTOMNAV,bundle1);
                mFirebaseAnalytics.setUserProperty(BOTTOMNAV, "Save");
                startActivity(new Intent(getApplicationContext(), SavedDate.class));
            }
        });

        more_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle1.putString("More", " ");
                mFirebaseAnalytics.logEvent(BOTTOMNAV,bundle1);
                mFirebaseAnalytics.setUserProperty(BOTTOMNAV, "More");
                startActivity(new Intent(getApplicationContext(), More.class));
            }
        });

        view_all_sug.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle2.putString("Search_List", " ");
                mFirebaseAnalytics.logEvent(HOMEPAGESELECTION,bundle2);
                mFirebaseAnalytics.setUserProperty(HOMEPAGESELECTION, "Search_List");
                //  mFirebaseAnalytics.setUserProperty("home_select_dates", "date");
                startActivity(new Intent(getApplicationContext(), Events.class));

            }
        });
        top_pics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle2.putString("Bar_Pubs", " ");
                mFirebaseAnalytics.logEvent(HOMEPAGESELECTION,bundle2);
                mFirebaseAnalytics.setUserProperty(HOMEPAGESELECTION, "Bars_Pubs");
                //mFirebaseAnalytics.setUserProperty("home_select_top_picks", "top picks");

                Intent intent = new Intent(getApplicationContext(), Events.class);
                intent.putExtra("selected_categories", "classes");//top picks
                intent.putExtra("selected_type", "categories");
                startActivity(intent);

            }
        });
        places_eat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle2.putString("Restaurants", " ");
                mFirebaseAnalytics.logEvent(HOMEPAGESELECTION,bundle2);
                mFirebaseAnalytics.setUserProperty(HOMEPAGESELECTION, "Restaurants");
                //mFirebaseAnalytics.setUserProperty("home_select_food", "food");
                Intent intent = new Intent(getApplicationContext(), Events.class);
                intent.putExtra("selected_categories", "activities");
                intent.putExtra("selected_type", "categories");
                startActivity(intent);
            }
        });
       /* live_music.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFirebaseAnalytics.setUserProperty("home_select_live", "live");

                Intent intent = new Intent(getApplicationContext(), Events.class);
                intent.putExtra("selected_categories", "live");
                intent.putExtra("selected_type", "categories");
                startActivity(intent);
            }
        });*/
       /* find_bar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFirebaseAnalytics.setUserProperty("home_select_drinks", "drinks");

                Intent intent = new Intent(getApplicationContext(), Events.class);
                intent.putExtra("selected_categories", "drinks");
                intent.putExtra("selected_type", "categories");
                startActivity(intent);
            }
        });*/
        hint_tips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle2.putString("Blog", " ");
                mFirebaseAnalytics.logEvent(HOMEPAGESELECTION,bundle2);
                mFirebaseAnalytics.setUserProperty(HOMEPAGESELECTION, "Blog");
                Intent intent = new Intent(getApplicationContext(), Events.class);
                intent.putExtra("selected_categories", "alternatives");
                intent.putExtra("selected_type", "categories");
                startActivity(intent);

            }
        });
        get_cultural.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle2.putString("Arts_Crafts", " ");
                mFirebaseAnalytics.logEvent(HOMEPAGESELECTION,bundle2);
                mFirebaseAnalytics.setUserProperty(HOMEPAGESELECTION, "Arts_Crafts");
                //mFirebaseAnalytics.setUserProperty("home_select_art,craft", "art,craft");

                Intent intent = new Intent(getApplicationContext(), Events.class);
                intent.putExtra("selected_categories", "arts-crafts");
                intent.putExtra("selected_type", "categories");
                startActivity(intent);
            }
        });

        groups_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle2.putString("Group_Events", " ");
                mFirebaseAnalytics.logEvent(HOMEPAGESELECTION,bundle2);
                mFirebaseAnalytics.setUserProperty(HOMEPAGESELECTION, "Group_Events");
                Intent intent = new Intent(getApplicationContext(), Events.class);
                intent.putExtra("selected_group", " ");
                intent.putExtra("selected_type", "group");
                startActivity(intent);
            }
        });
        datenight_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle2.putString("DateNight", " ");
                mFirebaseAnalytics.logEvent(HOMEPAGESELECTION,bundle2);
                mFirebaseAnalytics.setUserProperty(HOMEPAGESELECTION, "DateNight");
                Intent intent = new Intent(getApplicationContext(), Events.class);
                intent.putExtra("selected_group", "datenight");
                intent.putExtra("selected_type", "group");
                startActivity(intent);
            }
        });
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
