package com.trs.datenight2;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class TermsAndConditions extends Activity {

    TextView text1;
    RelativeLayout back;
    TextView home_text, find_text, nearby_text, save_text, more_text;
    RelativeLayout home_lay, find_lay, nearby_lay, save_lay, more_lay;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.terms_and_conditions);

        init();
        onclick();

        Typeface hanken_light = Typeface.createFromAsset(getApplication().getAssets(), "fonts/Hanken-Light.ttf");

        home_text.setTypeface(hanken_light);
        find_text.setTypeface(hanken_light);
        nearby_text.setTypeface(hanken_light);
        save_text.setTypeface(hanken_light);
        more_text.setTypeface(hanken_light);


        text1.setText("Terms and Conditions  \n" +
                "====================\n" +
                "\n" +
                "Last updated: February 12, 2018\n" +
                "\n" +
                "Please read these Terms and Conditions (\"Terms\", \"Terms and Conditions\")\n" +
                "carefully before using the http://datenight.london/ website and the DateNight\n" +
                "mobile application (the \"Service\") operated by Christopher James Digital Ltd\n" +
                "(\"us\", \"we\", or \"our\").\n" +
                "\n" +
                "Your access to and use of the Service is conditioned on your acceptance of and\n" +
                "compliance with these Terms. These Terms apply to all visitors, users and\n" +
                "others who access or use the Service.\n" +
                "\n" +
                "By accessing or using the Service you agree to be bound by these Terms. If you\n" +
                "disagree with any part of the terms then you may not access the Service.\n" +
                "\n" +
                "Accounts  \n" +
                "--------\n" +
                "\n" +
                "When you create an account with us, you must provide us information that is\n" +
                "accurate, complete, and current at all times. Failure to do so constitutes a\n" +
                "breach of the Terms, which may result in immediate termination of your account\n" +
                "on our Service.\n" +
                "\n" +
                "You are responsible for safeguarding the password that you use to access the\n" +
                "Service and for any activities or actions under your password, whether your\n" +
                "password is with our Service or a third-party service.\n" +
                "\n" +
                "You agree not to disclose your password to any third party. You must notify us\n" +
                "immediately upon becoming aware of any breach of security or unauthorized use\n" +
                "of your account.\n" +
                "\n" +
                "Intellectual Property  \n" +
                "---------------------\n" +
                "\n" +
                "The Service and its original content, features and functionality are and will\n" +
                "remain the exclusive property of Christopher James Digital Ltd and its\n" +
                "licensors. The Service is protected by copyright, trademark, and other laws of\n" +
                "both the United Kingdom and foreign countries. Our trademarks and trade dress\n" +
                "may not be used in connection with any product or service without the prior\n" +
                "written consent of Christopher James Digital Ltd.\n" +
                "\n" +
                "Links To Other Web Sites  \n" +
                "------------------------\n" +
                "\n" +
                "Our Service may contain links to third-party web sites or services that are\n" +
                "not owned or controlled by Christopher James Digital Ltd.\n" +
                "\n" +
                "Christopher James Digital Ltd has no control over, and assumes no\n" +
                "responsibility for, the content, privacy policies, or practices of any third\n" +
                "party web sites or services. You further acknowledge and agree that\n" +
                "Christopher James Digital Ltd shall not be responsible or liable, directly or\n" +
                "indirectly, for any damage or loss caused or alleged to be caused by or in\n" +
                "connection with use of or reliance on any such content, goods or services\n" +
                "available on or through any such web sites or services.\n" +
                "\n" +
                "We strongly advise you to read the terms and conditions and privacy policies\n" +
                "of any third-party web sites or services that you visit.\n" +
                "\n" +
                "Termination  \n" +
                "-----------\n" +
                "\n" +
                "We may terminate or suspend your account immediately, without prior notice or\n" +
                "liability, for any reason whatsoever, including without limitation if you\n" +
                "breach the Terms.\n" +
                "\n" +
                "Upon termination, your right to use the Service will immediately cease. If you\n" +
                "wish to terminate your account, you may simply discontinue using the Service.\n" +
                "\n" +
                "All provisions of the Terms which by their nature should survive termination\n" +
                "shall survive termination, including, without limitation, ownership\n" +
                "provisions, warranty disclaimers, indemnity and limitations of liability.\n" +
                "\n" +
                "Indemnification  \n" +
                "---------------\n" +
                "\n" +
                "You agree to defend, indemnify and hold harmless Christopher James Digital Ltd\n" +
                "and its licensee and licensors, and their employees, contractors, agents,\n" +
                "officers and directors, from and against any and all claims, damages,\n" +
                "obligations, losses, liabilities, costs or debt, and expenses (including but\n" +
                "not limited to attorney's fees), resulting from or arising out of a) your use\n" +
                "and access of the Service, by you or any person using your account and\n" +
                "password, or b) a breach of these Terms.\n" +
                "\n" +
                "Limitation Of Liability  \n" +
                "-----------------------\n" +
                "\n" +
                "In no event shall Christopher James Digital Ltd, nor its directors, employees,\n" +
                "partners, agents, suppliers, or affiliates, be liable for any indirect,\n" +
                "incidental, special, consequential or punitive damages, including without\n" +
                "limitation, loss of profits, data, use, goodwill, or other intangible losses,\n" +
                "resulting from (i) your access to or use of or inability to access or use the\n" +
                "Service; (ii) any conduct or content of any third party on the Service; (iii)\n" +
                "any content obtained from the Service; and (iv) unauthorized access, use or\n" +
                "alteration of your transmissions or content, whether based on warranty,\n" +
                "contract, tort (including negligence) or any other legal theory, whether or\n" +
                "not we have been informed of the possibility of such damage, and even if a\n" +
                "remedy set forth herein is found to have failed of its essential purpose.\n" +
                "\n" +
                "Disclaimer  \n" +
                "----------\n" +
                "\n" +
                "Your use of the Service is at your sole risk. The Service is provided on an\n" +
                "\"AS IS\" and \"AS AVAILABLE\" basis. The Service is provided without warranties\n" +
                "of any kind, whether express or implied, including, but not limited to,\n" +
                "implied warranties of merchantability, fitness for a particular purpose, non-\n" +
                "infringement or course of performance.\n" +
                "\n" +
                "Christopher James Digital Ltd its subsidiaries, affiliates, and its licensors\n" +
                "do not warrant that a) the Service will function uninterrupted, secure or\n" +
                "available at any particular time or location; b) any errors or defects will be\n" +
                "corrected; c) the Service is free of viruses or other harmful components; or\n" +
                "d) the results of using the Service will meet your requirements.\n" +
                "\n" +
                "Exclusions  \n" +
                "----------\n" +
                "\n" +
                "Without limiting the generality of the foregoing and notwithstanding any other\n" +
                "provision of these terms, under no circumstances will Christopher James\n" +
                "Digital Ltd ever be liable to you or any other person for any indirect,\n" +
                "incidental, consequential, special, punitive or exemplary loss or damage\n" +
                "arising from, connected with, or relating to your use of the Service, these\n" +
                "Terms, the subject matter of these Terms, the termination of these Terms or\n" +
                "otherwise, including but not limited to personal injury, loss of data,\n" +
                "business, markets, savings, income, profits, use, production, reputation or\n" +
                "goodwill, anticipated or otherwise, or economic loss, under any theory of\n" +
                "liability (whether in contract, tort, strict liability or any other theory or\n" +
                "law or equity), regardless of any negligence or other fault or wrongdoing\n" +
                "(including without limitation gross negligence and fundamental breach) by\n" +
                "Christopher James Digital Ltd or any person for whom Christopher James Digital\n" +
                "Ltd is responsible, and even if Christopher James Digital Ltd has been advised\n" +
                "of the possibility of such loss or damage being incurred.\n" +
                "\n" +
                "Governing Law  \n" +
                "-------------\n" +
                "\n" +
                "These Terms shall be governed and construed in accordance with the laws of\n" +
                "England and Wales, without regard to its conflict of law provisions.\n" +
                "\n" +
                "Our failure to enforce any right or provision of these Terms will not be\n" +
                "considered a waiver of those rights. If any provision of these Terms is held\n" +
                "to be invalid or unenforceable by a court, the remaining provisions of these\n" +
                "Terms will remain in effect. These Terms constitute the entire agreement\n" +
                "between us regarding our Service, and supersede and replace any prior\n" +
                "agreements we might have between us regarding the Service.\n" +
                "\n" +
                "Changes  \n" +
                "-------\n" +
                "\n" +
                "We reserve the right, at our sole discretion, to modify or replace these Terms\n" +
                "at any time. If a revision is material we will try to provide at least 30 days\n" +
                "notice prior to any new terms taking effect. What constitutes a material\n" +
                "change will be determined at our sole discretion.\n" +
                "\n" +
                "By continuing to access or use our Service after those revisions become\n" +
                "effective, you agree to be bound by the revised terms. If you do not agree to\n" +
                "the new terms, you must stop using the service.\n" +
                "\n" +
                "Privacy Policy and Cookie Policy  \n" +
                "--------------------------------\n" +
                "\n" +
                "Please refer to our Privacy Policy and Cookies Policy. You agree that they\n" +
                "constitute part of these terms. You must read our Privacy Policy and Cookies\n" +
                "Policy before you use the Service.\n" +
                "\n" +
                "Contact Us  \n" +
                "----------\n" +
                "\n" +
                "If you have any questions about these Terms, please contact us.\n" +
                "\n");
    }

    private void init() {

        text1 = (TextView) findViewById(R.id.text1);
        back = (RelativeLayout) findViewById(R.id.back);

        home_text = (TextView) findViewById(R.id.home_text);
        find_text = (TextView) findViewById(R.id.find_text);
        nearby_text = (TextView) findViewById(R.id.nearby_text);
        save_text = (TextView) findViewById(R.id.save_text);
        more_text = (TextView) findViewById(R.id.more_text);

        home_lay = (RelativeLayout) findViewById(R.id.home_lay);
        find_lay = (RelativeLayout) findViewById(R.id.find_lay);
        nearby_lay = (RelativeLayout) findViewById(R.id.nearby_lay);
        save_lay = (RelativeLayout) findViewById(R.id.save_lay);
        more_lay = (RelativeLayout) findViewById(R.id.more_lay);
    }


    private void onclick() {

        home_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Home.class));
            }
        });

        find_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Events.class));
            }
        });


        nearby_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Nearby.class));
            }
        });

        save_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), SavedDate.class));
            }
        });

        more_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), More.class));
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }


}
