package com.trs.datenight2;


import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

public class Multidex extends Application {
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        MultiDex.install(this);
    }
}
