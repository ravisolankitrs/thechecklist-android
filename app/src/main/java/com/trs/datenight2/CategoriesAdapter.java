package com.trs.datenight2;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Sandeep on 4/6/2018.
 */
public class CategoriesAdapter extends BaseAdapter {

    Context context;
    LayoutInflater inflater;
    List<String> data;
    String resultp = new String();


    public CategoriesAdapter(Context context, List<String> arraylist) {
        this.context = context;
        this.data = arraylist;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final TextView Categories;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.categories_list, viewGroup, false);
        Categories = (TextView) itemView.findViewById(R.id.Categories);

        resultp = data.get(i);
        //Log.d("resultp", resultp);
        Categories.setText(resultp);




      /*  itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Details.getBookingCount(categories.getText().toString());
                view.setSelected(true);
                Details.checkBoxItemPosition(i);
                notifyDataSetChanged();
            }
        });*/

        return itemView;
    }
}

