package com.trs.datenight2;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.SimpleTimeZone;

/**
 * Created by Sandeep on 1/24/2018.
 */
public class DetailsDateAdapter extends BaseAdapter {

    Context context;
    LayoutInflater inflater;
    List<String> data;
    String resultp = new String();
    String day;

    public DetailsDateAdapter(Context context, List<String> arraylist) {
        this.context = context;
        this.data = arraylist;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final TextView date_text;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.details_date_list, viewGroup, false);
        date_text = (TextView) itemView.findViewById(R.id.date_text);
        resultp = data.get(i);
        Log.d("resultp", resultp);
        date_text.setText(resultp);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("date_text", date_text.getText().toString());
                Calendar c = Calendar.getInstance();
                SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
                try {
                    Date date = formatter.parse(date_text.getText().toString());
                    System.out.println(date);
                    c.setTime(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                String d= checkDaysName(dayOfWeek);
                Details.getDate(date_text.getText().toString(), d);
            }
        });
        return itemView;
    }

    public String checkDaysName(int days) {
        switch (days) {
            case 1:
                day = "Sunday";
                break;
            case 2:
                day = "Monday";
                break;
            case 3:
                day = "Tuesday";
                break;
            case 4:
                day = "Wednesday";
                break;
            case 5:
                day = "Thursday";
                break;
            case 6:
                day = "Friday";
                break;
            case 7:
                day = "Saturday";
                break;
        }
        return day;
    }
}
