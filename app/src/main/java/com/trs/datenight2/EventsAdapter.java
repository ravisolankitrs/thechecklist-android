package com.trs.datenight2;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import static com.facebook.FacebookSdk.getApplicationContext;
import static java.security.AccessController.getContext;

public class EventsAdapter extends BaseAdapter {
    public static final String EVENTNAME = "Event_name";
    public static final String EVENTLOCATION = "Location_street";
    Context context;
    LayoutInflater inflater;
    String itemselectedcategory;
    Bundle bundle,bundle1;
    FirebaseAnalytics firebaseAnalytics;
    ArrayList<HashMap<String, String>> data;
    HashMap<String, String> resultp = new HashMap<String, String>();
    HashMap<String, String> resultp1 = new HashMap<String, String>();
    ImageLoader imageLoader;
    ArrayList<HashMap<String, String>> arraylist;
    SharedPreference_main sharedPreference_main;
    String message = "";
    ProgressDialog progress;
    Button save_un_save;
    int pos = 0;
    String is_fav;
    ArrayList<String> stringarray = new ArrayList<String>();
    View itemView;
    private FirebaseAnalytics mFirebaseAnalytics;

    public EventsAdapter(Context context, ArrayList<HashMap<String, String>> arraylist,String itemselectedcategory) {
        this.context = context;
        this.data = arraylist;
        this.itemselectedcategory = itemselectedcategory;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        itemView = inflater.inflate(R.layout.events_list, viewGroup, false);

        float scale = context.getResources().getDisplayMetrics().density;
        int dpAsPixels = (int) (6 * scale + 0.5f);
        itemView.setPadding(dpAsPixels, dpAsPixels, dpAsPixels, 0);
        bundle=new Bundle();
        bundle1=new Bundle();
        firebaseAnalytics=FirebaseAnalytics.getInstance(context);
        sharedPreference_main = new SharedPreference_main(context);

        RelativeLayout event_list = itemView.findViewById(R.id.event_list);
        TextView event_name = (TextView) itemView.findViewById(R.id.event_name);
        TextView event_address = (TextView) itemView.findViewById(R.id.event_address);
        TextView ticket_price = (TextView) itemView.findViewById(R.id.ticket_price);
        ImageView event_image = (ImageView) itemView.findViewById(R.id.event_image);
        // save_un_save = (Button) itemView.findViewById(R.id.save_un_save);
        final CheckBox checkbutton = (CheckBox) itemView.findViewById(R.id.checkbutton);

        Typeface hanken_Book = Typeface.createFromAsset(context.getAssets(), "fonts/Hanken-Book.ttf");
        Typeface openSans_Regular = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
        Typeface openSans_Light = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Light.ttf");

        event_name.setTypeface(hanken_Book);
        ticket_price.setTypeface(openSans_Regular);
        event_address.setTypeface(openSans_Light);


        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);

        resultp = data.get(position);


        event_name.setText(resultp.get(Events.EVENT_NAME));
       /* if (!resultp.get(Events.LOCATION_STREET).equals("") || !resultp.get(Events.LOCATION_POSTCODE).equals("")) {
            event_address.setText(resultp.get(Events.LOCATION_STREET) + ", " + resultp.get(Events.LOCATION_POSTCODE));
        } else {
            event_address.setText("");
        }*/

        if (!resultp.get(Events.LOCATION_CITY).equals("") || !resultp.get(Events.LOCATION_POSTCODE).equals("")) {
            event_address.setText(resultp.get(Events.LOCATION_CITY) + ", " + resultp.get(Events.LOCATION_POSTCODE));
        } else {
            event_address.setText("");
        }

        ticket_price.setText(resultp.get(Events.COST));

        is_fav = resultp.get(Events.IS_FAVORITE);
        Log.d("is_fav", is_fav);

        // checkbutton.setChecked(Boolean.parseBoolean(is_fav));


        if (is_fav.equals("1")) {
            checkbutton.setButtonDrawable(R.drawable.save);
            checkbutton.setChecked(false);
        } else {
            checkbutton.setButtonDrawable(R.drawable.un_save);
            checkbutton.setChecked(true);
        }

        checkbutton.setTag(position);
        checkbutton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                pos = position;
                resultp = data.get(position);

                if (isChecked) {
                    checkbutton.setButtonDrawable(R.drawable.un_save);
                    checkbutton.setChecked(true);
                    data.get(position).put(Events.IS_FAVORITE, "0");
                } else {
                    checkbutton.setButtonDrawable(R.drawable.save);
                    checkbutton.setChecked(false);
                    data.get(position).put(Events.IS_FAVORITE, "1");
                }
                new SavedJSON().execute();
            }
        });

        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int pxWidth = displayMetrics.widthPixels;
        float dpWidth = pxWidth / displayMetrics.density;
        int pxHeight = displayMetrics.heightPixels;
        float dpHeight = pxHeight / displayMetrics.density;

        String percentageWidth = String.format("%.0f", (pxWidth * .97));
        String percentageheight = String.format("%.0f", (pxHeight * .31));

        Log.d("dimensionhh", String.valueOf(percentageWidth));
        Log.d("dimensionhh", String.valueOf(percentageheight));

        RelativeLayout.LayoutParams parms = new RelativeLayout.LayoutParams(Integer.parseInt(percentageWidth), Integer.parseInt(percentageheight));
        event_list.setLayoutParams(parms);




        /*save_un_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos = position;
                resultp1 = data.get(position);


                Log.d("is_fav", resultp.get(Events.IS_FAVORITE));

               *//* String p = v.getTag().toString();
                int positionInt = Integer.parseInt(p);*//*

                //  int imageicon=R.drawable.un_save;
                if (Integer.valueOf(String.valueOf(v.getTag())) == R.drawable.un_save) {
                    ((Button) v).setBackgroundResource(R.drawable.save);
                    save_un_save.setTag(R.drawable.save);
                    //   save_un_save.setImageResource(R.drawable.un_save);
                } else {
                    ((Button) v).setBackgroundResource(R.drawable.un_save);
                    save_un_save.setTag(R.drawable.un_save);
                    // save_un_save.setImageResource(R.drawable.save);
                }
                //new SavedJSON().execute();

               *//* Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                if(is_fav.equals("1")){
                    if (message.equals("Favorite has been removed.")) {
                        save_un_save.setImageResource(R.drawable.un_save);
                    } else if (message.equals("Favorite has been inserted.")) {
                        save_un_save.setImageResource(R.drawable.save);
                    }
                 //   save_un_save.setImageResource(R.drawable.save);
                }
                if(is_fav.equals("0")){
                    if (message.equals("Favorite has been removed.")) {
                        save_un_save.setImageResource(R.drawable.un_save);
                    } else if (message.equals("Favorite has been inserted.")) {
                        save_un_save.setImageResource(R.drawable.save);
                    }
                }*//*

            }
        });*/
        // Glide.with(context).load(resultp.get(Events.EVENT_IMAGE_URL)).into(event_image);

        try {
            Picasso.with(context).load(resultp.get(Events.EVENT_IMAGE_URL)).into(event_image);
        } catch (IllegalArgumentException e) {
            event_image.setImageResource(R.color.black);
        }
        //   Glide.with(context).load(resultp.get(Events.EVENT_IMAGE_URL)).into(event_image);


        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resultp = data.get(position);

                /*Bundle bundle1 = new Bundle();
                bundle1.putString(EVENTNAME, resultp.get(Events.EVENT_NAME));
                bundle1.putString(EVENTLOCATION, resultp.get(Events.LOCATION_STREET));
                mFirebaseAnalytics.logEvent("RestaurantName", bundle1);*/
                bundle1.putString("Listing"," ");
                bundle1.putString("Listing",resultp.get(Events.EVENT_NAME) );
                firebaseAnalytics.logEvent("Select_Content",bundle1);
                firebaseAnalytics.setUserProperty("Select_Content","Listing");
                view.getParent().requestDisallowInterceptTouchEvent(true);
                // mFirebaseAnalytics.setUserProperty("EVENT_NAME", resultp.get(Events.EVENT_NAME));
                // mFirebaseAnalytics.setUserProperty("LOCATION_STREET", resultp.get(Events.LOCATION_STREET));

                Log.d("EVENT_NAME", resultp.get(Events.EVENT_NAME));
                Intent intent = new Intent(context, Details.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Bundle bundle = new Bundle();
                bundle.putString(Events.COMPANY_WEBSITE, resultp.get(Events.COMPANY_WEBSITE));
                bundle.putString(Events.EVENT_IMAGE_URL, resultp.get(Events.EVENT_IMAGE_URL));
                bundle.putString(Events.EVENT_ID, resultp.get(Events.EVENT_ID));
                bundle.putString(Events.EVENT_NAME, resultp.get(Events.EVENT_NAME));
                bundle.putString(Events.COMPANY_TAGLINE, resultp.get(Events.COMPANY_TAGLINE));
                bundle.putString(Events.EVENT_LINK, resultp.get(Events.EVENT_LINK));
                bundle.putString(Events.EVENT_CONTENT, resultp.get(Events.EVENT_CONTENT));
                bundle.putString(Events.EVENT_OWNER, resultp.get(Events.EVENT_OWNER));
                bundle.putString(Events.TERMS, resultp.get(Events.TERMS));
                bundle.putString(Events.COMMENTS, resultp.get(Events.COMMENTS));
                bundle.putString(Events.CUSTOM_FIELDS, resultp.get(Events.CUSTOM_FIELDS));
                bundle.putString(Events.COST, resultp.get(Events.COST));
                bundle.putString(Events.LOCATION_ADDRESS, resultp.get(Events.LOCATION_ADDRESS));
                bundle.putString(Events.LOCATION_STREET, resultp.get(Events.LOCATION_STREET));
                bundle.putString(Events.LOCATION_POSTCODE, resultp.get(Events.LOCATION_POSTCODE));
                bundle.putString(Events.LOCATION_LATITUDE, resultp.get(Events.LOCATION_LATITUDE));
                bundle.putString(Events.LOCATION_LONGITUDE, resultp.get(Events.LOCATION_LONGITUDE));
                bundle.putString(Events.IS_FAVORITE, resultp.get(Events.IS_FAVORITE));
                bundle.putString(Events.PRODUCT, resultp.get(Events.PRODUCT));
                bundle.putString(Events.IMAGES, resultp.get(Events.IMAGES));
                bundle.putString(Events.VIDEO, resultp.get(Events.VIDEO));
                intent.putExtras(bundle);

                context.startActivity(intent);
            }
        });

        //   save_un_save.setTag(position);


        return itemView;
    }


    // SavedJSON AsyncTask
    private class SavedJSON extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //   progressBar();
            resultp = data.get(pos);
            is_fav = resultp.get(Events.IS_FAVORITE);


        }

        @Override
        protected Void doInBackground(Void... params) {

            Log.d("EVENT_ID", resultp.get(Events.EVENT_ID));
            Log.d("EVENT_NAME", resultp.get(Events.EVENT_NAME));
            Log.d("EventUI", sharedPreference_main.getUser_Id().toString());
            Log.d("position1", String.valueOf(pos));
            arraylist = new ArrayList<HashMap<String, String>>();
            OkHttpClient client = new OkHttpClient();
            MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
            RequestBody body = RequestBody.create(mediaType, "listing_id=" + resultp.get(Events.EVENT_ID) + "&user_id=" + sharedPreference_main.getUser_Id().toString());
            Request request = new Request.Builder()
                    .url(Home.MAIN_APPLICATION_URL + "listing/update-favorites")
                    .post(body)
                    .addHeader("content-type", "application/x-www-form-urlencoded")
                    .addHeader("accept", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .build();
            try {
                Response response = client.newCall(request).execute();
                Log.d("responstr", String.valueOf(response.code()));
                String fulljson = response.body().string();
                Log.d("body", fulljson);
                JSONArray jsonArray = new JSONArray(fulljson);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    message = jsonObject.getString("message");
                    Log.d("massage", message);
                }


            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(Void args) {
            //  progress.dismiss();
            Log.d("position2", String.valueOf(pos));
            /*bundle1.putString("Name",resultp.get(Events.EVENT_NAME));
            bundle.putString("Location",resultp.get(Events.LOCATION_STREET));
            bundle.putString("Tag",resultp.get(Events.COMPANY_TAGLINE));
            bundle.putString("Category",itemselectedcategory);
            firebaseAnalytics.logEvent("Restaurant_Name",bundle1);
            firebaseAnalytics.logEvent("Restaurant_Details",bundle);*/
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();

            /* if (message.equals("Favorite has been removed.")) {
                save_un_save.setImageResource(R.drawable.un_save);
            }
            if (message.equals("Favorite has been inserted.")) {
                save_un_save.setImageResource(R.drawable.save);
            }*/


/*
            if (is_fav.equals("1")) {
                if (message.equals("Favorite has been removed.")) {
                    save_un_save.setImageResource(R.drawable.un_save);
                } else if (message.equals("Favorite has been inserted.")) {
                    save_un_save.setImageResource(R.drawable.save);
                }

            }else if (is_fav.equals("0")) {
                if (message.equals("Favorite has been removed.")) {
                    save_un_save.setImageResource(R.drawable.un_save);
                } else if (message.equals("Favorite has been inserted.")) {
                    save_un_save.setImageResource(R.drawable.save);
                }
            }*/
        }
    }

}
