package com.trs.datenight2;

import android.content.Context;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;


public class DetailsBookingCountAdapter extends BaseAdapter {

    Context context;
    LayoutInflater inflater;
    List<String> data;
    String resultp = new String();
    Integer selected_position = -1;
    String value, value2;

    public DetailsBookingCountAdapter(Context context, List<String> arraylist) {
        this.context = context;
        this.data = arraylist;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final TextView count;
        final ImageView select_image;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.details_bookin_count_list, viewGroup, false);
        count = (TextView) itemView.findViewById(R.id.count);
        select_image = (ImageView) itemView.findViewById(R.id.select_image);

        resultp = data.get(i);
        Log.d("resultp", resultp);
        count.setText(resultp);

        if (i == Details.item_position) {
            select_image.setImageResource(R.drawable.selected);
            /*if (select_image.getVisibility() == 0) {
                select_image.setImageResource(R.drawable.selected);
            } else {
                select_image.setImageResource(0);
            }*/

        } else {
            select_image.setImageResource(0);
        }

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Details.getBookingCount(count.getText().toString());
                Details.checkBoxItemPosition(i);
                notifyDataSetChanged();
            }
        });

        return itemView;
    }
}
