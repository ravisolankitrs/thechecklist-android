package com.trs.datenight2;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandeep on 4/9/2018.
 */
public class SearchListviewAdapter extends BaseAdapter {

    Context context;
    LayoutInflater inflater;
    ArrayList<String> data;
    String resultp = new String();
    Database database;
    SQLiteDatabase db;
    TextView search_text;

    public SearchListviewAdapter(Context context, ArrayList<String> arraylist) {
        this.context = context;
        this.data = arraylist;
        database = new Database(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
       // db = database.getReadableDatabase();

      //  Restauantlist restauantlist= data.get(i);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.searchview_list, viewGroup, false);
        RelativeLayout cross;
        search_text = (TextView) itemView.findViewById(R.id.search_text);
        cross=(RelativeLayout) itemView.findViewById(R.id.layout_cross);

      //  resultp = data.get(i);
        //Log.d("resultp", resultp);

       /* cross.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                return false;
            }
        });*/

        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                db = database.getWritableDatabase();
                String close= String.valueOf(data.get(i));
                Log.d("closeclose",close);
                db.delete(Database.TABLE_NAME, Database.Product_NAME + "=?", new String[]{close});
                db.close();
                data.remove(close);
                notifyDataSetChanged();
            }
        });

        search_text.setText(data.get(i));

       /* search_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Details.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                try {
                    String name= String.valueOf(data.get(i));
                    Bundle bundle = new Bundle();
                    bundle.putString(Events.EVENT_NAME,name);
                    intent.putExtras(bundle);
                    context.startActivity(intent);

                    } catch (Exception ex) {

                }

            }
        });*/


      /*  itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Details.getBookingCount(categories.getText().toString());
                view.setSelected(true);
                Details.checkBoxItemPosition(i);
                notifyDataSetChanged();
            }
        });*/

        return itemView;
    }
   /* public void removeItem(int position){
        db = database.getWritableDatabase();
        String close= String.valueOf(search_text.getText());
        Log.d("closeclose",close);
        db.delete(Database.TABLE_NAME, Database.Product_NAME + "=?", new String[]{close});
        db.close();
        notifyDataSetChanged();
    }*/

}
