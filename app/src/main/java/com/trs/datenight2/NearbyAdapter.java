package com.trs.datenight2;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.picasso.Picasso;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by trs on 10-Jul-18.
 */

public class NearbyAdapter extends BaseAdapter {

    private FirebaseAnalytics mFirebaseAnalytics;
    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    HashMap<String, String> resultp = new HashMap<String, String>();
    ImageLoader imageLoader;
    SharedPreference_main sharedPreference_main;

    static String EVENT_ID = "listing_id";
    static String EVENT_LINK = "listing_link";
    static String EVENT_CONTENT = "listing_content";
    static String EVENT_OWNER = "listing_owner";
    static String EVENT_NAME = "listing_name";

    static String COMPANY_WEBSITE = "company_website";
    static String LOCATION_ADDRESS = "geolocation_formatted_address";
    static String EVENT_IMAGE_URL = "listing_gallery_images";
    static String COMPANY_TAGLINE = "company_tagline";

    static String geolocation_data = "geolocation_data";

    static String LOCATION_STREET = "geolocation_street";
    static String LOCATION_POSTCODE = "geolocation_postcode";
    static String LOCATION_LATITUDE = "geolocation_lat";
    static String LOCATION_LONGITUDE = "geolocation_long";

    static String CUSTOM_FIELDS = "custom_fields";
    static String COST = "cost";
    static String IS_FAVORITE = "is_favorite";
    static String PRODUCT = "product";

    static String MEDIA_GALLERY = "media_gallery";
    static String IMAGES = "images";
    static String VIDEO = "video_url";

    static String TERMS = "listing_tags";
    static String COMMENTS = "comments";
    private ProgressDialog mProgressDialog;
    ProgressDialog progress;

    public NearbyAdapter(Context context, ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        this.data = arraylist;
        Log.d("arraylistarraylist", String.valueOf(arraylist));
        imageLoader = new ImageLoader(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        sharedPreference_main = new SharedPreference_main(context);

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.nearby_list, viewGroup, false);
        TextView event_name = (TextView) itemView.findViewById(R.id.event_name);
        TextView event_address = (TextView) itemView.findViewById(R.id.event_address);
        ImageView event_image = (ImageView) itemView.findViewById(R.id.event_image);
        TextView milesaway = (TextView) itemView.findViewById(R.id.milesaway);

        Typeface hanken_Book = Typeface.createFromAsset(context.getAssets(), "fonts/Hanken-Book.ttf");
        Typeface openSans_Regular = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
        Typeface openSans_Light = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Light.ttf");

        event_name.setTypeface(hanken_Book);
        event_address.setTypeface(openSans_Light);

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);

        resultp = data.get(position);

        milesaway.setText(resultp.get("radius") + " miles away");
        event_name.setText(resultp.get(Nearby.NAME));
        event_address.setText(resultp.get(Nearby.ADDRESS) + ", " + resultp.get(Nearby.POSTCODE));
        //imageLoader.DisplayImage(resultp.get(Nearby.MAINIMAGE), event_image);

        try {
            Picasso.with(context).load(resultp.get(Nearby.MAINIMAGE)).into(event_image);
        } catch (IllegalArgumentException e) {
            event_image.setImageResource(R.color.black);
        }

        //imageLoader.DisplayImage(resultp.get(Events.EVENT_IMAGE_URL), event_image);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resultp = data.get(position);
                showRestaurantDetails(resultp.get(Nearby.ID), sharedPreference_main.getUser_Id().toString());

            }
        });

        return itemView;
    }
    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    private void progressBar() {
        progress = new ProgressDialog(context);
        progress.setMessage("Loading...");
        progress.setIndeterminate(true);
        progress.setCanceledOnTouchOutside(false);
        progress.show();
    }

    public void showRestaurantDetails(final String rest_id, final String user_id) {
        // String URL = "http://datenight.london/wp-json/api/v1/listing/userNearbyLocationdata";

        try {
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            String URL = "http://datenight.london/wp-json/api/v1/listing/userNearbyLocationdata";
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("listing_id", rest_id);
            jsonBody.put("user_id", user_id);
            final String requestBody = jsonBody.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        final JSONObject jsonObject = new JSONObject(String.valueOf(response));
                      //  Log.d("JSONObject", String.valueOf(jsonObject));

                        Intent intent = new Intent(context, Details.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                        Bundle bundle = new Bundle();

                        bundle.putString(EVENT_NAME, jsonObject.getString(EVENT_NAME));

                        if (jsonObject.has(EVENT_NAME)) {
                            bundle.putString(EVENT_NAME, jsonObject.getString(EVENT_NAME));
                        } else {
                            bundle.putString(EVENT_NAME, "");
                        }

                        if (jsonObject.has(EVENT_ID)) {
                            bundle.putString(EVENT_ID, jsonObject.getString(EVENT_ID));
                        } else {
                            bundle.putString(EVENT_ID, "");
                        }
                        if (jsonObject.has(EVENT_LINK)) {
                            bundle.putString(EVENT_LINK, jsonObject.getString(EVENT_LINK));
                        } else {
                            bundle.putString(EVENT_LINK, "");
                        }

                        if (jsonObject.has(EVENT_CONTENT)) {
                            bundle.putString(EVENT_CONTENT, jsonObject.getString(EVENT_CONTENT));
                        } else {
                            bundle.putString(EVENT_CONTENT, "");
                        }

                        bundle.putString(EVENT_OWNER, jsonObject.getString(EVENT_OWNER));

                        if (jsonObject.has(COMPANY_TAGLINE)) {
                            bundle.putString(COMPANY_TAGLINE, jsonObject.getString(COMPANY_TAGLINE));
                        } else {
                            bundle.putString(COMPANY_TAGLINE, "");
                        }
                        if (jsonObject.has(COMPANY_WEBSITE)) {
                            bundle.putString(COMPANY_WEBSITE, jsonObject.getString(COMPANY_WEBSITE));
                        } else {
                            bundle.putString(COMPANY_WEBSITE, "");
                        }
                        bundle.putString(PRODUCT, jsonObject.getString(PRODUCT));

                        JSONObject jsonObject5 = jsonObject.getJSONObject(MEDIA_GALLERY);
                        if (jsonObject5.has(IMAGES)) {
                            bundle.putString(IMAGES, jsonObject5.getString(IMAGES));
                        } else {
                            bundle.putString(IMAGES, "");
                        }

                        if (jsonObject5.has(VIDEO)) {
                            bundle.putString(VIDEO, jsonObject5.getString(VIDEO));
                        } else {
                            bundle.putString(VIDEO, "");
                        }
                        JSONObject jsonobject3 = jsonObject.getJSONObject(geolocation_data);

                        if (jsonobject3.has(LOCATION_LATITUDE)) {
                            bundle.putString(LOCATION_LATITUDE, jsonobject3.getString(LOCATION_LATITUDE));
                        } else {
                            bundle.putString(LOCATION_LATITUDE, "");
                        }

                        if (jsonobject3.has(LOCATION_LONGITUDE)) {
                            bundle.putString(LOCATION_LONGITUDE, jsonobject3.getString(LOCATION_LONGITUDE));
                        } else {
                            bundle.putString(LOCATION_LONGITUDE, "");
                        }

                        if (jsonobject3.has(LOCATION_ADDRESS)) {
                            bundle.putString(LOCATION_ADDRESS, jsonobject3.getString(LOCATION_ADDRESS));
                        } else {
                            bundle.putString(LOCATION_ADDRESS, "");
                        }
                        if (jsonobject3.has(LOCATION_STREET)) {
                            bundle.putString(LOCATION_STREET, jsonobject3.getString(LOCATION_STREET));
                        } else {
                            bundle.putString(LOCATION_STREET, "");
                        }

                        if (jsonobject3.has(LOCATION_POSTCODE)) {
                            bundle.putString(LOCATION_POSTCODE, jsonobject3.getString(LOCATION_POSTCODE));
                        } else {
                            bundle.putString(LOCATION_POSTCODE, "");
                        }

                        if (jsonObject.has(EVENT_IMAGE_URL)) {
                            bundle.putString(EVENT_IMAGE_URL, jsonObject.getString(EVENT_IMAGE_URL));
                        } else {
                            bundle.putString(EVENT_IMAGE_URL, "");
                        }

                        bundle.putString(CUSTOM_FIELDS, jsonObject.getString(CUSTOM_FIELDS));
                        bundle.putString(TERMS, jsonObject.getString(TERMS));

                        JSONObject jsonobject4 = jsonObject.getJSONObject(CUSTOM_FIELDS);
                        bundle.putString(COST, jsonobject4.getString(COST));
                        bundle.putString(COMMENTS, jsonObject.getString(COMMENTS));
                        bundle.putString(IS_FAVORITE, jsonObject.getString(IS_FAVORITE));

                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

            };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}