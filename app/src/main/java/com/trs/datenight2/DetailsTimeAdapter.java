package com.trs.datenight2;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sandeep on 1/18/2018.
 */
public class DetailsTimeAdapter extends RecyclerView.Adapter<DetailsTimeAdapter.CustomViewHolder> {

    ArrayList<HashMap<String, String>> data;
    private Context context;
    private LayoutInflater layoutInflater;
    HashMap<String, String> resultp = new HashMap<String, String>();
    int row_index = -1;

    // Provide a suitable constructor (depends on the kind of dataset)
    public DetailsTimeAdapter(Context context, ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        this.data = arraylist;
        this.layoutInflater = LayoutInflater.from(context);
    }


    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.details_time_list, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int position) {
        resultp = data.get(position);

        Typeface openSans_Light = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Light.ttf");
        holder.time.setTypeface(openSans_Light);
        holder.time.setText(resultp.get("time"));

        holder.time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("time_text", holder.time.getText().toString());
                Details.getTime(holder.time.getText().toString());
                row_index = position;

                notifyDataSetChanged();
            }
        });

        if(row_index == position){
            holder.time.setBackgroundColor(Color.parseColor("#8E90A8"));
            Details.getchangeButton(1);
        }
        else
        {
            holder.time.setBackgroundColor(Color.parseColor("#b7b7b7"));
        }
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        Button time;
        public CustomViewHolder(View itemView) {
            super(itemView);
            time = (Button) itemView.findViewById(R.id.time);
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return data.size();
    }
}

