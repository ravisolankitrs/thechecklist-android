package com.trs.datenight2;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Sandeep on 1/18/2018.
 */
public class DetailsVideoAdapter extends RecyclerView.Adapter<DetailsVideoAdapter.CustomViewHolder> {

    List<VideoList> data;
    private Context context;
    private LayoutInflater layoutInflater;
    HashMap<String, String> resultp = new HashMap<String, String>();

    // Provide a suitable constructor (depends on the kind of dataset)
    public DetailsVideoAdapter(Context context, List<VideoList> arraylist) {
        this.context = context;
        this.data = arraylist;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.details_video_list, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int position) {
        final VideoList video = data.get(position);
        Typeface hanken_Light = Typeface.createFromAsset(context.getAssets(), "fonts/Hanken-Light.ttf");
        //  holder.term_name.setTypeface(hanken_Light);
        //  holder.term_name.setText(resultp.get(Details.TERMS_NAME));
        //    holder.text.setTypeface(hanken_Light);
        //  holder.image.setImageResource(R.drawable.splash_image);
        //  holder.text.setText("Video");

        if (video.getImage() != null && !video.getImage().isEmpty()) {
            Picasso.with(context).load(video.getImage()).fit().into(holder.image);
            //  Picasso.with(context).load(RestApiInterface.RESTAURANT_ICONS_URLS + item.getmVideoImage()).fit().into(holder.logo);
        } else
            holder.image.setImageResource(R.drawable.splash_image);

        holder.videoplay.setTag(position);
        holder.videoplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(video.getType().equals("youtube")){
                    Intent intent = new Intent(context, YoutubeActivity.class);
                    intent.putExtra("ID", video.getId());
                    context.startActivity(intent);
                }else if(video.getType().equals("uploaded")){
                    Intent intent = new Intent(context, VideoviewActivity.class);
                    intent.putExtra("DATAURL", video.getVideoUrl());
                    context.startActivity(intent);
                }

            }
        });
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        //  Button term_name;
        ImageView image;
        TextView text;
        ImageView  videoplay;
        public CustomViewHolder(View itemView) {
            super(itemView);
            //  term_name = (Button) itemView.findViewById(R.id.term_name);
            image=(ImageView) itemView.findViewById(R.id.imagevideo);
            //  text=(TextView) itemView.findViewById(R.id.videotext);
            videoplay = (ImageView) itemView.findViewById(R.id.video_play);
        }
    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return data.size();
    }
}

