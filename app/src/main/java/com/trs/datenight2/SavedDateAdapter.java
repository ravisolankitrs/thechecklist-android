package com.trs.datenight2;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class SavedDateAdapter extends BaseAdapter {

    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    HashMap<String, String> resultp = new HashMap<String, String>();
    ImageLoader imageLoader;
    ArrayList<HashMap<String, String>> arraylist;
    String massage = "",status="";
    String itemselectedcategory="";
    Bundle bundle1,bundle2;
    FirebaseAnalytics firebaseAnalytics;
    SharedPreference_main sharedPreference_main;
    int pos;
    String is_fav, is_like;
    private FirebaseAnalytics mFirebaseAnalytics;

    public SavedDateAdapter(Context context, ArrayList<HashMap<String, String>> arraylist,String itemselectedcategory) {
        this.context = context;
        this.data = arraylist;
        imageLoader = new ImageLoader(context);
        this.itemselectedcategory =itemselectedcategory;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.saved_date_list, viewGroup, false);
        TextView event_name = (TextView) itemView.findViewById(R.id.event_name);
        TextView event_address = (TextView) itemView.findViewById(R.id.event_address);
        TextView ticket_price = (TextView) itemView.findViewById(R.id.ticket_price);
        ImageView event_image = (ImageView) itemView.findViewById(R.id.event_image);
        ImageView save_un_save = (ImageView) itemView.findViewById(R.id.save_un_save);

        Typeface hanken_Book = Typeface.createFromAsset(context.getAssets(), "fonts/Hanken-Book.ttf");
        Typeface openSans_Regular = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
        Typeface openSans_Light = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Light.ttf");

        event_name.setTypeface(hanken_Book);
        ticket_price.setTypeface(openSans_Regular);
        event_address.setTypeface(openSans_Light);
        sharedPreference_main = new SharedPreference_main(context);

        resultp = data.get(position);


        event_name.setText(resultp.get(Events.EVENT_NAME));
        event_address.setText(resultp.get(Events.LOCATION_STREET) + ", " + resultp.get(Events.LOCATION_POSTCODE));
        ticket_price.setText(resultp.get(Events.COST));

        is_fav = resultp.get(Events.IS_FAVORITE);
        is_like = resultp.get(SavedDate.IS_LIKE);
        Log.d("is_fav", is_fav);

        /*Bundle bundle = new Bundle();
        bundle.putString("tab_is_fav", is_fav);
        bundle.putString("saved_event_name", resultp.get(Events.EVENT_NAME));
        bundle.putString("saved_location_street", resultp.get(Events.LOCATION_STREET));
        mFirebaseAnalytics.logEvent("RestaurantDetails", bundle);*/

        if (is_fav.equals("1") || is_like.equals("1")) {
            save_un_save.setImageResource(R.drawable.save);
        } else {
            save_un_save.setImageResource(R.drawable.un_save);
        }
        save_un_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos = position;
                resultp = data.get(position);
                new SavedJSON().execute();
            }
        });

        Picasso.with(context).load(resultp.get(Events.EVENT_IMAGE_URL)).into(event_image);
        // imageLoader.DisplayImage(resultp.get(Events.EVENT_IMAGE_URL), event_image);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resultp = data.get(position);
                Log.d("EVENT_ID", resultp.get(Events.EVENT_ID));
                Intent intent = new Intent(context, Details.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Bundle bundle = new Bundle();
                bundle.putString(Events.COMPANY_WEBSITE, resultp.get(Events.COMPANY_WEBSITE));
                bundle.putString(Events.EVENT_IMAGE_URL, resultp.get(Events.EVENT_IMAGE_URL));
                bundle.putString(Events.EVENT_ID, resultp.get(Events.EVENT_ID));
                bundle.putString(Events.EVENT_NAME, resultp.get(Events.EVENT_NAME));
                bundle.putString(Events.COMPANY_TAGLINE, resultp.get(Events.COMPANY_TAGLINE));
                bundle.putString(Events.EVENT_LINK, resultp.get(Events.EVENT_LINK));
                bundle.putString(Events.EVENT_CONTENT, resultp.get(Events.EVENT_CONTENT));
                bundle.putString(Events.EVENT_OWNER, resultp.get(Events.EVENT_OWNER));
                bundle.putString(Events.TERMS, resultp.get(Events.TERMS));
                bundle.putString(Events.COMMENTS, resultp.get(Events.COMMENTS));
                bundle.putString(Events.CUSTOM_FIELDS, resultp.get(Events.CUSTOM_FIELDS));
                bundle.putString(Events.COST, resultp.get(Events.COST));
                bundle.putString(Events.LOCATION_ADDRESS, resultp.get(Events.LOCATION_ADDRESS));
                bundle.putString(Events.LOCATION_STREET, resultp.get(Events.LOCATION_STREET));
                bundle.putString(Events.LOCATION_POSTCODE, resultp.get(Events.LOCATION_POSTCODE));
                bundle.putString(Events.LOCATION_LATITUDE, resultp.get(Events.LOCATION_LATITUDE));
                bundle.putString(Events.LOCATION_LONGITUDE, resultp.get(Events.LOCATION_LONGITUDE));
                bundle.putString(Events.IS_FAVORITE, resultp.get(Events.IS_FAVORITE));
                bundle.putString(Events.PRODUCT, resultp.get(Events.PRODUCT));
                bundle.putString(Events.IMAGES, resultp.get(Events.IMAGES));
                bundle.putString(Events.VIDEO, resultp.get(Events.VIDEO));
                bundle1=new Bundle();
                bundle2=new Bundle();
                firebaseAnalytics=FirebaseAnalytics.getInstance(context);
                bundle1.putString("Name",resultp.get(Events.EVENT_NAME));
                bundle2.putString("Location",resultp.get(Events.LOCATION_STREET));
                bundle2.putString("Tag",resultp.get(Events.COMPANY_TAGLINE));
                bundle2.putString("Category",itemselectedcategory);
                firebaseAnalytics.logEvent("Restaurant_Name",bundle1);
                firebaseAnalytics.logEvent("Restaurant_Details",bundle2);
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });

        return itemView;
    }

    // SavedJSON AsyncTask
    public class SavedJSON extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            arraylist = new ArrayList<HashMap<String, String>>();
            OkHttpClient client = new OkHttpClient();
            MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
            RequestBody body = RequestBody.create(mediaType, "listing_id=" + resultp.get(SavedDate.EVENT_ID) + "&user_id=" + sharedPreference_main.getUser_Id() + "&is_favorite=" + resultp.get(SavedDate.IS_FAVORITE) + "&is_like=" + resultp.get(SavedDate.IS_LIKE));
            Request request = new Request.Builder()
                    .url(Home.MAIN_APPLICATION_URL + "listing/update-favorite-like")
                    .post(body)
                    .addHeader("content-type", "application/x-www-form-urlencoded")
                    .addHeader("accept", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .build();
            try {
                Response response = client.newCall(request).execute();
                Log.d("responstr", String.valueOf(response.code()));
                String fulljson = response.body().string();
                Log.d("body", fulljson);
                JSONArray jsonArray = new JSONArray(fulljson);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    massage = jsonObject.getString("message");
                    status = jsonObject.getString("status");
                    Log.d("massage", massage);
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            Toast.makeText(context, massage, Toast.LENGTH_LONG).show();
            if(status.equals("200")){
                data.remove(pos);
                notifyDataSetChanged();
            }
           /* if (massage.equals("Favorite has been removed.")) {

            }*/
        }
    }

}
