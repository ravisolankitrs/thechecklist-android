package com.trs.datenight2;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Sandeep on 1/22/2018.
 */
public class SharedPreference_main {

    public Context context;
    public SharedPreferences.Editor editor;
    private SharedPreferences sharedPrefrences;
    public static final String PREFS_READ = "PREFS_READ";
    int PRIVATE_MODE = 0;
    private final String CHECK_LOGIN = "check_login";
    private final String USER_ID = "user_id";
    private final String USER_NAME = "user_name";
    private final String LONGITUDE = "longitude";
    private final String LATITUDE = "latitude";
    private final String USER_EMAIL = "user_email";
    private final String USER_PHONE = "user_phone";
    private final String USER_ADDRESS = "user_address";
    private final String USER_ZIP = "user_zip";
    private final String TICKICON = "tickicon";
    private final String CHECKDATA = "checkdata";




    public SharedPreference_main(Context context) {
        this.context = context;
        sharedPrefrences = context.getSharedPreferences(PREFS_READ, PRIVATE_MODE);
        editor = sharedPrefrences.edit();
    }

    public void logoutUser() {
        // Clearing all user data from Shared Preferences
        editor = sharedPrefrences.edit();
        editor.clear();
        editor.commit();

    }

    public void setLogin(String check_login) {
        editor = sharedPrefrences.edit();
        editor.putString(CHECK_LOGIN, check_login);
        editor.commit();
    }
    public String getLogin() {
        String check_login;
        check_login = sharedPrefrences.getString(CHECK_LOGIN, "");
        return check_login;
    }
///for user id
    public void setUser_Id(String user_id) {
        editor = sharedPrefrences.edit();
        editor.putString(USER_ID, user_id);
        editor.commit();
    }
    public String getUser_Id() {
        String user_id;
        user_id = sharedPrefrences.getString(USER_ID, "");
        return user_id;
    }

// for user details
    public void setUser_Details(String user_email,String user_name,String user_phone,String user_address,String user_zip) {
        editor = sharedPrefrences.edit();
        editor.putString(USER_EMAIL, user_email);
        editor.putString(USER_NAME, user_name);
        editor.putString(USER_PHONE, user_phone);
        editor.putString(USER_ADDRESS, user_address);
        editor.putString(USER_ZIP, user_zip);
        editor.commit();
    }

    public String getUser_Email() {
        String user_email;
        user_email = sharedPrefrences.getString(USER_EMAIL, "");
        return user_email;
    }
    public String getUser_Name() {
        String user_name;
        user_name = sharedPrefrences.getString(USER_NAME, "");
        return user_name;
    }

    public String getUser_Phone() {
        String user_phone;
        user_phone = sharedPrefrences.getString(USER_PHONE, "");
        return user_phone;
    }

    public String getUser_Address() {
        String user_address;
        user_address = sharedPrefrences.getString(USER_ADDRESS, "");
        return user_address;
    }

    public String getUser_Zip() {
        String user_zip;
        user_zip = sharedPrefrences.getString(USER_ZIP, "");
        return user_zip;
    }

    public void setcheckdata(String checkdata) {
        editor = sharedPrefrences.edit();
        editor.putString(CHECKDATA, checkdata);
        editor.commit();
    }
    public String getcheckdata() {
        String checkdata;
        checkdata = sharedPrefrences.getString(CHECKDATA, "");
        return checkdata;
    }


    public void settickicon(String tickicon) {
        editor = sharedPrefrences.edit();
        editor.putString(TICKICON, tickicon);
        editor.commit();
    }
    public String gettickicon() {
        String tickicon;
        tickicon = sharedPrefrences.getString(TICKICON, "");
        return tickicon;
    }

    public void setUser_Name(String name) {
        editor = sharedPrefrences.edit();
        editor.putString(USER_NAME, name);
        editor.commit();
    }

    public String getlongitude() {
        String tickicon;
        tickicon = sharedPrefrences.getString(LONGITUDE, "");
        return tickicon;
    }

    public void setlongitude(String name) {
        editor = sharedPrefrences.edit();
        editor.putString(LONGITUDE, name);
        editor.commit();
    }

    public String getlatitude() {
        String tickicon;
        tickicon = sharedPrefrences.getString(LATITUDE, "");
        return tickicon;
    }

    public void setlatitude(String name) {
        editor = sharedPrefrences.edit();
        editor.putString(LATITUDE, name);
        editor.commit();
    }

}
