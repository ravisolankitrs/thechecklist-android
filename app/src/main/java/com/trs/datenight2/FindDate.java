package com.trs.datenight2;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Sandeep on 1/5/2018.
 */
public class FindDate extends Activity {

    static String EVENT_NAME = "listing_name";
    static String LOCATION_ADDRESS = "geolocation_formatted_address";
    static String EVENT_IMAGE_URL = "listing_gallery_images";
    static String COMPANY_TAGLINE = "company_tagline";
    static String EVENT_ID = "listing_id";
    static String EVENT_LINK = "listing_link";
    static String EVENT_CONTENT = "listing_content";
    static String EVENT_OWNER = "listing_owner";
    static String TERMS = "listing_tags";
    static String COMMENTS = "comments";
    static String geolocation_data = "geolocation_data";
    static String LOCATION_STREET = "geolocation_street";
    static String LOCATION_POSTCODE = "geolocation_postcode";
    static String LOCATION_LATITUDE = "geolocation_lat";
    static String LOCATION_LONGITUDE = "geolocation_long";
    static String CUSTOM_FIELDS = "custom_fields";
    static String COST = "cost";
    static String IS_FAVORITE = "is_favorite";
    View view;
    RelativeLayout back;
    ListView listView;
    FindDateAdapter adapter;
    ArrayList<HashMap<String, String>> arraylist;
    ProgressDialog progress;
    SharedPreference_main sharedPreference_main;
    TextView title;

    TextView home_text, find_text, nearby_text, save_text, more_text;
    RelativeLayout home_lay, find_lay, nearby_lay, save_lay, more_lay;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.find_date);

        sharedPreference_main = new SharedPreference_main(getApplication());
        init();
        onclick();

        Typeface hanken_light = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Hanken-Light.ttf");
        title.setTypeface(hanken_light);

        home_text.setTypeface(hanken_light);
        find_text.setTypeface(hanken_light);
        nearby_text.setTypeface(hanken_light);
        save_text.setTypeface(hanken_light);
        more_text.setTypeface(hanken_light);

        new DownloadJSON().execute();
    }


    private void init() {
        title = (TextView) findViewById(R.id.title);
        listView = (ListView) findViewById(R.id.list_view);
        back = (RelativeLayout) findViewById(R.id.back);

        home_text = (TextView) findViewById(R.id.home_text);
        find_text = (TextView) findViewById(R.id.find_text);
        nearby_text = (TextView) findViewById(R.id.nearby_text);
        save_text = (TextView) findViewById(R.id.save_text);
        more_text = (TextView) findViewById(R.id.more_text);

        home_lay = (RelativeLayout) findViewById(R.id.home_lay);
        find_lay = (RelativeLayout) findViewById(R.id.find_lay);
        nearby_lay = (RelativeLayout) findViewById(R.id.nearby_lay);
        save_lay = (RelativeLayout) findViewById(R.id.save_lay);
        more_lay = (RelativeLayout) findViewById(R.id.more_lay);

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    private void onclick() {

        home_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Home.class));
            }
        });

        find_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        nearby_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Nearby.class));
            }
        });

        save_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), SavedDate.class));
            }
        });

        more_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), More.class));
            }
        });
    }

    private void progressBar() {
        progress = new ProgressDialog(this);
        progress.setMessage("Loading...");
        progress.setIndeterminate(true);
        progress.setCanceledOnTouchOutside(false);
        progress.show();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(), Home.class));
    }

    // DownloadJSON AsyncTask
    private class DownloadJSON extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar();
        }

        @Override
        protected Void doInBackground(Void... params) {
            arraylist = new ArrayList<HashMap<String, String>>();
            OkHttpClient client = new OkHttpClient();
            MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
            RequestBody body;
            body = RequestBody.create(mediaType, "user_id=" + sharedPreference_main.getUser_Id().toString());

            Request request = new Request.Builder()
                    .url(Home.MAIN_APPLICATION_URL + "listing")
                    .post(body)
                    .addHeader("content-type", "application/x-www-form-urlencoded")
                    .addHeader("accept", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .build();
            try {
                Response response = client.newCall(request).execute();
                Log.d("responstr", String.valueOf(response.code()));
                String fulljson = response.body().string();
                Log.d("body", fulljson);

                JSONArray jsonArray = new JSONArray(fulljson);
                for (int i = 0; i < jsonArray.length(); i++) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    map.put(EVENT_NAME, jsonObject.getString(EVENT_NAME));
                    map.put(COMPANY_TAGLINE, jsonObject.getString(COMPANY_TAGLINE));
                    JSONObject jsonobject3 = jsonObject.getJSONObject(geolocation_data);
                    if (jsonobject3.has(LOCATION_ADDRESS)) {
                        map.put(LOCATION_ADDRESS, jsonobject3.getString(LOCATION_ADDRESS));
                    } else {
                        map.put(LOCATION_ADDRESS, "");
                    }

                    if (jsonobject3.has(LOCATION_STREET)) {
                        map.put(LOCATION_STREET, jsonobject3.getString(LOCATION_STREET));
                    } else {
                        map.put(LOCATION_STREET, jsonobject3.getString("geolocation_city"));
                    }

                    map.put(LOCATION_POSTCODE, jsonobject3.getString(LOCATION_POSTCODE));
                    map.put(LOCATION_LATITUDE, jsonobject3.getString(LOCATION_LATITUDE));
                    map.put(LOCATION_LONGITUDE, jsonobject3.getString(LOCATION_LONGITUDE));
                    map.put(LOCATION_LONGITUDE, jsonobject3.getString(LOCATION_LONGITUDE));
                    map.put(EVENT_IMAGE_URL, jsonObject.getString(EVENT_IMAGE_URL));
                    map.put(EVENT_ID, jsonObject.getString(EVENT_ID));
                    map.put(EVENT_LINK, jsonObject.getString(EVENT_LINK));
                    map.put(EVENT_CONTENT, jsonObject.getString(EVENT_CONTENT));
                    map.put(EVENT_OWNER, jsonObject.getString(EVENT_OWNER));
                    map.put(TERMS, jsonObject.getString(TERMS));
                    map.put(CUSTOM_FIELDS, jsonObject.getString(CUSTOM_FIELDS));
                    JSONObject jsonobject4 = jsonObject.getJSONObject(CUSTOM_FIELDS);
                    map.put(COST, jsonobject4.getString(COST));
                    map.put(COMMENTS, jsonObject.getString(COMMENTS));
                    map.put(IS_FAVORITE, jsonObject.getString(IS_FAVORITE));
                    arraylist.add(map);
                    Log.d("array_list", arraylist.toString());
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            progress.dismiss();
            adapter = new FindDateAdapter(getApplicationContext(), arraylist);
            listView.setAdapter(adapter);
        }
    }
}
