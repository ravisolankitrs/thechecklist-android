package com.trs.datenight2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ListingAdapter extends BaseAdapter {

    Context context;
    private final static int AVATAR_WIDTH = 150;
    private final static int AVATAR_HEIGHT = 300;
    ArrayList<HashMap<String, String>> data;
    String itemselectedcategory = "";

    HashMap<String, String> resultp = new HashMap<String, String>();
    ArrayList<HashMap<String, String>> mDataDeleted = new ArrayList<HashMap<String, String>>();

    public ListingAdapter(Context context, ArrayList<HashMap<String, String>> data, String itemselectedcategory) {
        this.data = data;
        this.context = context;
        this.itemselectedcategory = itemselectedcategory;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    public void next() {
        mDataDeleted.add(data.get(0));
        data.remove(0);
        notifyDataSetChanged();
    }

    public void back() {
        if (mDataDeleted.size() > 0) {
            data.add(0, mDataDeleted.get(mDataDeleted.size() - 1));
            mDataDeleted.remove(mDataDeleted.size() - 1);
            notifyDataSetChanged();
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        resultp = data.get(position);
        final ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        // If holder not exist then locate all view from UI file.
        if (convertView == null) {
            // inflate UI from XML file
            convertView = inflater.inflate(R.layout.item_listing, parent, false);
            // get all UI view
            holder = new ViewHolder(convertView);
            // set tag for holder
            convertView.setTag(holder);
        } else {
            // if holder created, get tag from view
            holder = (ViewHolder) convertView.getTag();
        }


        Typeface hanken_light = Typeface.createFromAsset(context.getAssets(), "fonts/Hanken-Light.ttf");
        Typeface helveticaNeue_Medium = Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaNeue-Medium.ttf");
        Typeface openSans_Regular = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
        Typeface openSans_Light = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Light.ttf");

        holder.event_name.setTypeface(hanken_light);
        holder.event_address.setTypeface(openSans_Light);
        holder.ticket_price.setTypeface(helveticaNeue_Medium);

        //setting data to views
        holder.event_name.setText(resultp.get(ListingActivity.COMPANY_TAGLINE));

        if (!resultp.get(ListingActivity.LOCATION_STREET).equals("") || !resultp.get(ListingActivity.LOCATION_POSTCODE).equals("")) {
            holder.event_address.setText(resultp.get(ListingActivity.LOCATION_STREET) + ", " + resultp.get(ListingActivity.LOCATION_POSTCODE));
        } else {
            holder.event_address.setText(resultp.get(ListingActivity.LOCATION_CITY) + ", " + resultp.get(ListingActivity.LOCATION_COUNTRY_SHORT));
        }

        holder.ticket_price.setText(resultp.get(Events.COST));


        try {
            Picasso.with(context).load(resultp.get(ListingActivity.EVENT_IMAGE_URL)).into(holder.event_image);
        } catch (IllegalArgumentException e) {
            holder.event_image.setImageResource(R.color.black);
        }

        Log.d("image_url", resultp.get(ListingActivity.EVENT_IMAGE_URL));

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resultp = data.get(position);

                view.getParent().requestDisallowInterceptTouchEvent(true);

                Log.d("EVENT_NAME", resultp.get(ListingActivity.EVENT_NAME));
                Intent intent = new Intent(context, Details.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Bundle bundle = new Bundle();
                bundle.putString(ListingActivity.COMPANY_WEBSITE, resultp.get(ListingActivity.COMPANY_WEBSITE));
                bundle.putString(ListingActivity.EVENT_IMAGE_URL, resultp.get(ListingActivity.EVENT_IMAGE_URL));
                bundle.putString(ListingActivity.EVENT_ID, resultp.get(ListingActivity.EVENT_ID));
                bundle.putString(ListingActivity.EVENT_NAME, resultp.get(ListingActivity.EVENT_NAME));
                bundle.putString(ListingActivity.COMPANY_TAGLINE, resultp.get(ListingActivity.COMPANY_TAGLINE));
                bundle.putString(ListingActivity.EVENT_LINK, resultp.get(ListingActivity.EVENT_LINK));
                bundle.putString(ListingActivity.EVENT_CONTENT, resultp.get(ListingActivity.EVENT_CONTENT));
                bundle.putString(ListingActivity.EVENT_OWNER, resultp.get(ListingActivity.EVENT_OWNER));
                bundle.putString(ListingActivity.TERMS, resultp.get(ListingActivity.TERMS));
                bundle.putString(ListingActivity.COMMENTS, resultp.get(ListingActivity.COMMENTS));
                bundle.putString(ListingActivity.CUSTOM_FIELDS, resultp.get(ListingActivity.CUSTOM_FIELDS));
                bundle.putString(ListingActivity.COST, resultp.get(ListingActivity.COST));
                bundle.putString(ListingActivity.LOCATION_ADDRESS, resultp.get(ListingActivity.LOCATION_ADDRESS));
                bundle.putString(ListingActivity.LOCATION_STREET, resultp.get(ListingActivity.LOCATION_STREET));
                bundle.putString(ListingActivity.LOCATION_POSTCODE, resultp.get(ListingActivity.LOCATION_POSTCODE));
                bundle.putString(ListingActivity.LOCATION_LATITUDE, resultp.get(ListingActivity.LOCATION_LATITUDE));
                bundle.putString(ListingActivity.LOCATION_LONGITUDE, resultp.get(ListingActivity.LOCATION_LONGITUDE));
                bundle.putString(ListingActivity.IS_FAVORITE, resultp.get(ListingActivity.IS_FAVORITE));
                bundle.putString(ListingActivity.PRODUCT, resultp.get(ListingActivity.PRODUCT));
                bundle.putString(ListingActivity.IMAGES, resultp.get(ListingActivity.IMAGES));
                bundle.putString(ListingActivity.VIDEO, resultp.get(ListingActivity.VIDEO));
                /*bundle1=new Bundle();
                bundle2=new Bundle();
                firebaseAnalytics=FirebaseAnalytics.getInstance(context);
                bundle1.putString("Name",resultp.get(Events.EVENT_NAME));
                bundle2.putString("Location",resultp.get(Events.LOCATION_STREET));
                bundle2.putString("Tag",resultp.get(Events.COMPANY_TAGLINE));
                bundle2.putString("Category",itemselectedcategory);
                firebaseAnalytics.logEvent("Restaurant_Name",bundle1);
                firebaseAnalytics.logEvent("Restaurant_Details",bundle2);*/
                Bundle bundle2 = new Bundle();
                FirebaseAnalytics firebaseAnalytics;
                firebaseAnalytics = FirebaseAnalytics.getInstance(context);
                bundle2.putString("Swipe", " ");
                bundle2.putString("Swipe",resultp.get(Events.EVENT_NAME) );
                firebaseAnalytics.logEvent("Select_Content", bundle2);
                firebaseAnalytics.setUserProperty("Select_Content", "Swipe");
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });


        return convertView;
    }

    private class ViewHolder {
        private ImageView event_image;
        private TextView event_name;
        private TextView event_address, ticket_price;
        RecyclerView term_recyclerView;
        CardView main_cardview;


        public ViewHolder(View view) {
            event_image = (ImageView) view.findViewById(R.id.event_image);
            event_name = (TextView) view.findViewById(R.id.event_name);
            event_address = (TextView) view.findViewById(R.id.event_address);
            ticket_price = (TextView) view.findViewById(R.id.ticket_price);
            term_recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
            main_cardview = view.findViewById(R.id.main_cardview);

        }
    }

   /* public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }*/
}
