package com.trs.datenight2;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ksoichiro.android.observablescrollview.ObservableListView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.nineoldandroids.view.ViewHelper;
import com.nineoldandroids.view.ViewPropertyAnimator;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static java.security.AccessController.getContext;


public class Events extends Activity implements ObservableScrollViewCallbacks {

    private static final String TAG = Events.class.getSimpleName();
    public static final String BOTTOMNAV = "Bottom_Nav";
    public static final String LISTINGFILTERCLICKED = "FilterSelected";
    public static final String LISTINGFILTERSSELECTED = "Listing_Filters_selected";
    public static final String categoryselected = "SelectedCategory";
    public static final String confirmselected = "SelectedConfirm";
    public static final String dateselected = "SelectedDate";
    public static final String locationselected = "SelectedLocation";
    public static final String SEARCHEDINFIND = "Search";
    public static final String LISTINGCLICKEDITEMCATEGORY = "Listing_Clicked_Item_Category";
    static String EVENT_NAME = "listing_name";
    static String COMPANY_WEBSITE = "company_website";
    static String LOCATION_ADDRESS = "geolocation_formatted_address";
    static String EVENT_IMAGE_URL = "listing_gallery_images";
    static String COMPANY_TAGLINE = "company_tagline";
    static String EVENT_ID = "listing_id";
    static String EVENT_LINK = "listing_link";
    static String EVENT_CONTENT = "listing_content";
    static String EVENT_OWNER = "listing_owner";
    static String TERMS = "listing_tags";
    static String COMMENTS = "comments";
    static String geolocation_data = "geolocation_data";
    static String LOCATION_STREET = "geolocation_street";
    static String LOCATION_POSTCODE = "geolocation_postcode";
    static String LOCATION_LATITUDE = "geolocation_lat";
    static String LOCATION_LONGITUDE = "geolocation_long";
    static String LOCATION_CITY = "geolocation_city";
    static String CUSTOM_FIELDS = "custom_fields";
    static String COST = "cost";
    static String IS_FAVORITE = "is_favorite";

    //for next activity
    static String PRODUCT = "product";
    static String MEDIA_GALLERY = "media_gallery";
    static String IMAGES = "images";
    static String VIDEO = "video_url";
    static Dialog dialogRefineType, dialogRefineDate;
    View view;
    RelativeLayout back;
    ImageView refine_search, refine_search1;
    ObservableListView observeList_view;
    Button cancel_icon, confirm_icon, category, dates, save, swipe_button;
    CategoriesAdapter adapter1;
    EventsAdapter adapter;
    ArrayList<HashMap<String, String>> arraylist;
    ArrayList<String> stringArray, stringArrayList;
    ProgressDialog progress;
    TextView no_data;
    ListView listView1, listView2;
    TextView header;
    RelativeLayout relativeLayout;
    SharedPreference_main sharedPreference_main;
    String selected_date = "", selected_type = "", selected_categories = "", selected_search_data = "", selected_location = "", selected_group = "";
    TextView title;
    TextView home_text, find_text, nearby_text, save_text, more_text;
    RelativeLayout home_lay, find_lay, nearby_lay, save_lay, more_lay, midLayout, layout1, layout2, layout3;
    LinearLayout bottom_layout, search_bar, search_layout, headerID, bottombox;
    EditText simpleSearchView;
    Button button, reset, Cross, search_icon;
    ImageView tickicon, tickicon1, tickicon2;
    String status = "", status_message = "";
    String selected_refinecategory = "";
    DateFormat formatter;
    DateFormat formatter1;
    List<String> date_array_list = new ArrayList<>();
    List<String> loaction = new ArrayList<>();
    DateAdapter date_adapter;
    TextView categorytext, datetitle, Locationtext;
    SQLiteDatabase SQLITEDATABASE;
    Database database;
    SQLiteDatabase db, db1, db2;
    Cursor c, c1, c2;
    String massage = "";
    ListView Search_listview;
    String val;
    SearchListviewAdapter searchListviewAdapter;
    String pos = "";
    Bundle bundle1,bundle2,bundle3,bundle,bundle4,bundle5;
    private FirebaseAnalytics mFirebaseAnalytics;
    private int mBaseTranslationY;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.events);
        bundle=new Bundle();
        bundle1=new Bundle();
        bundle2=new Bundle();
        bundle3=new Bundle();
        bundle4=new Bundle();
        bundle5=new Bundle();

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        sharedPreference_main = new SharedPreference_main(getApplication());

        init();
        onclick();


        stringArrayList = new ArrayList<String>();

        loaction.add("Central");
        loaction.add("North");
        loaction.add("East");
        loaction.add("South");
        loaction.add("West");
        loaction.add("Greater London");

        Typeface hanken_light = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Hanken-Light.ttf");
        title.setTypeface(hanken_light);


        categorytext.setTypeface(hanken_light);
        datetitle.setTypeface(hanken_light);
        Locationtext.setTypeface(hanken_light);

        home_text.setTypeface(hanken_light);
        find_text.setTypeface(hanken_light);
        nearby_text.setTypeface(hanken_light);
        save_text.setTypeface(hanken_light);
        more_text.setTypeface(hanken_light);
        reset.setTypeface(hanken_light);
        save.setTypeface(hanken_light);
        swipe_button.setTypeface(hanken_light);

        simpleSearchView.setCursorVisible(false);

        simpleSearchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                simpleSearchView.setCursorVisible(true);
                Search_listview.setVisibility(View.VISIBLE);
                Cross.setVisibility(View.VISIBLE);
                stringArrayList.clear();
                midLayout.setVisibility(View.GONE);
                latestData();
                search_icon.setVisibility(View.GONE);
            }
        });


        Intent intent = getIntent();

        if (intent.getExtras() != null) {
            //selected_type = intent.getStringExtra("selected_type");
            if (intent.getStringExtra("selected_type").equals("date")) {
                selected_date = intent.getStringExtra("selected_date");

            } else if (intent.getStringExtra("selected_type").equals("location")) {
                selected_location = intent.getStringExtra("selected_location");
            } else if (intent.getStringExtra("selected_type").equals("group")) {
                selected_group = intent.getStringExtra("selected_group");
            } else {
                selected_categories = intent.getStringExtra("selected_categories");
            }
            Log.d("selected_date", selected_date);
            Log.d("selected_categories", selected_categories);
            Log.d("selected_group", selected_group);

        }

        new DownloadJSON().execute();


        // stop opening keyboard in search bar aand hide the tablayout below keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        if (sharedPreference_main.gettickicon().equals("categories")) {
            tickicon.setVisibility(view.VISIBLE);
            tickicon1.setVisibility(view.GONE);
            tickicon2.setVisibility(view.GONE);
        } else if (sharedPreference_main.gettickicon().equals("Date")) {
            tickicon1.setVisibility(view.VISIBLE);
            tickicon.setVisibility(view.GONE);
            tickicon2.setVisibility(view.GONE);
        } else if (sharedPreference_main.gettickicon().equals("Location")) {
            tickicon2.setVisibility(view.VISIBLE);
            tickicon1.setVisibility(view.GONE);
            tickicon.setVisibility(view.GONE);
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    private void init() {
        arraylist = new ArrayList<HashMap<String, String>>();
        save = (Button) findViewById(R.id.save);
        Locationtext = (TextView) findViewById(R.id.Locationtext);
        datetitle = (TextView) findViewById(R.id.Datetext);
        categorytext = (TextView) findViewById(R.id.Categories_text);
        Search_listview = (ListView) findViewById(R.id.Search_listview);
        database = new Database(getApplicationContext());
        title = (TextView) findViewById(R.id.title);
        observeList_view = (ObservableListView) findViewById(R.id.observeList_view);
        search_layout = (LinearLayout) findViewById(R.id.listlay);
        headerID = (LinearLayout) findViewById(R.id.header_id);

        refine_search = (ImageView) findViewById(R.id.refine_search);
        refine_search1 = (ImageView) findViewById(R.id.refine_search1);
        back = (RelativeLayout) findViewById(R.id.back);
        no_data = (TextView) findViewById(R.id.no_data);

        home_text = (TextView) findViewById(R.id.home_text);
        find_text = (TextView) findViewById(R.id.find_text);
        nearby_text = (TextView) findViewById(R.id.nearby_text);
        save_text = (TextView) findViewById(R.id.save_text);
        more_text = (TextView) findViewById(R.id.more_text);

        bottom_layout = (LinearLayout) findViewById(R.id.bottom_layout);
        bottombox = (LinearLayout) findViewById(R.id.bottombox);
        reset = (Button) findViewById(R.id.reset);

        home_lay = (RelativeLayout) findViewById(R.id.home_lay);
        find_lay = (RelativeLayout) findViewById(R.id.find_lay);
        nearby_lay = (RelativeLayout) findViewById(R.id.nearby_lay);
        save_lay = (RelativeLayout) findViewById(R.id.save_lay);
        more_lay = (RelativeLayout) findViewById(R.id.more_lay);

        midLayout = (RelativeLayout) findViewById(R.id.mid_lay);
        layout1 = (RelativeLayout) findViewById(R.id.layout1);
        layout2 = (RelativeLayout) findViewById(R.id.layout2);
        layout3 = (RelativeLayout) findViewById(R.id.laypout3);

        tickicon = (ImageView) findViewById(R.id.tickicon);
        tickicon1 = (ImageView) findViewById(R.id.tickicon1);
        tickicon2 = (ImageView) findViewById(R.id.tickicon2);

        simpleSearchView = (EditText) findViewById(R.id.edittext);
        final String str = simpleSearchView.getText().toString();
        Cross = (Button) findViewById(R.id.button);
        search_icon = (Button) findViewById(R.id.search_image);
        swipe_button = findViewById(R.id.swipe_button);

        Cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (str.length() == 0) {
                    Cross.setVisibility(View.GONE);
                    Search_listview.setVisibility(View.GONE);
                    search_icon.setVisibility(View.VISIBLE);
                    simpleSearchView.setText(null);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(Cross.getWindowToken(),
                            InputMethodManager.RESULT_UNCHANGED_SHOWN);
                    observeList_view.setVisibility(View.VISIBLE);
                    no_data.setVisibility(View.GONE);
                }
            }
        });


        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }

        // ObservableListView uses setOnScrollListener, but it still works.
        observeList_view.setScrollViewCallbacks(Events.this);


        ViewCompat.setElevation(headerID, getResources().getDimension(R.dimen.toolbar_elevation));
        LayoutInflater inflater = LayoutInflater.from(Events.this);
        observeList_view.addHeaderView(inflater.inflate(R.layout.padding, observeList_view, false)); // toolbar


        swipe_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), ListingActivity.class));
            }
        });

    }

    private void onclick() {


        home_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle1.putString("Home", " ");
                mFirebaseAnalytics.logEvent(BOTTOMNAV,bundle1);
                mFirebaseAnalytics.setUserProperty(BOTTOMNAV, "Home");
                startActivity(new Intent(getApplicationContext(), Home.class));
            }
        });

        find_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        nearby_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle1.putString("NearBy", " ");
                mFirebaseAnalytics.logEvent(BOTTOMNAV,bundle1);
                mFirebaseAnalytics.setUserProperty(BOTTOMNAV, "Home");
                startActivity(new Intent(getApplicationContext(), Nearby.class));
            }
        });

        save_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle1.putString("Save", " ");
                mFirebaseAnalytics.logEvent(BOTTOMNAV,bundle1);
                mFirebaseAnalytics.setUserProperty(BOTTOMNAV, "Home");
                startActivity(new Intent(getApplicationContext(), SavedDate.class));
            }
        });

        more_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle1.putString("More", " ");
                mFirebaseAnalytics.logEvent(BOTTOMNAV,bundle1);
                mFirebaseAnalytics.setUserProperty(BOTTOMNAV, "Home");
                startActivity(new Intent(getApplicationContext(), More.class));
            }
        });

        refine_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*bundle2.putString("FilterIcon", "Clicked");
                mFirebaseAnalytics.logEvent(LISTINGFILTERCLICKED,bundle2);*/

                if (midLayout.getVisibility() == View.VISIBLE) {
                    midLayout.setVisibility(view.GONE);
                    bottom_layout.setVisibility(view.VISIBLE);
                    bottombox.setVisibility(view.GONE);
                    // refine_search.setVisibility(View.VISIBLE);
                    //  refine_search1.setVisibility(View.GONE);
                    //refine_search.setImageResource(R.drawable.cross_mark);
                    // listView.setPadding(0,0,0,0);
                } else {
                    midLayout.setVisibility(view.VISIBLE);
                    bottom_layout.setVisibility(view.GONE);
                    bottombox.setVisibility(view.VISIBLE);
                    refine_search.setVisibility(View.GONE);
                    refine_search1.setVisibility(View.VISIBLE);
                    // listView.setPadding(0,0,0,140);
                    // listView.addFooterView(bottombox);

                }

            }
        });

        refine_search1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (midLayout.getVisibility() == View.VISIBLE) {
                    midLayout.setVisibility(view.GONE);
                    bottom_layout.setVisibility(view.VISIBLE);
                    bottombox.setVisibility(view.GONE);
                    refine_search.setVisibility(View.VISIBLE);
                    refine_search1.setVisibility(View.GONE);
                    //refine_search.setImageResource(R.drawable.cross_mark);
                    // listView.setPadding(0,0,0,0);
                } else {
                    midLayout.setVisibility(view.VISIBLE);
                    bottom_layout.setVisibility(view.GONE);
                    bottombox.setVisibility(view.VISIBLE);
                    // refine_search.setVisibility(View.GONE);
                    // refine_search1.setVisibility(View.VISIBLE);
                }
            }
        });


        layout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreference_main.settickicon("categories");
                refinecategory();
                tickicon.setVisibility(view.VISIBLE);
                tickicon1.setVisibility(view.GONE);
                tickicon2.setVisibility(view.GONE);
               // mFirebaseAnalytics.setUserProperty("select_category", "category");
            }
        });

        layout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreference_main.settickicon("Date");
                refinedate();
                tickicon.setVisibility(view.GONE);
                tickicon1.setVisibility(view.VISIBLE);
                tickicon2.setVisibility(view.GONE);
                //mFirebaseAnalytics.setUserProperty("select_date", "Date");
            }
        });

        layout3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreference_main.settickicon("Location");
                location();
                tickicon.setVisibility(view.GONE);
                tickicon1.setVisibility(view.GONE);
                tickicon2.setVisibility(view.VISIBLE);
                //mFirebaseAnalytics.setUserProperty("select_location", "Location");
            }
        });


        simpleSearchView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // Search_listview.setVisibility(View.GONE);
                if (event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    if (simpleSearchView.length() != 0) {
                        selected_search_data = simpleSearchView.getText().toString();

                         bundle5 = new Bundle();
                        bundle5.putString(selected_search_data," " );
                        mFirebaseAnalytics.logEvent(SEARCHEDINFIND, bundle5);
                        mFirebaseAnalytics.setUserProperty(SEARCHEDINFIND, selected_search_data);
                        // mFirebaseAnalytics.setUserProperty("tab_search", selected_search_data);


                        // Search_listview.setVisibility(View.VISIBLE);
                        SQLITEDATABASE = database.getWritableDatabase();
                        ContentValues values = new ContentValues();
                        values.put(Database.Product_NAME, simpleSearchView.getText().toString());

                        if (dataExists(simpleSearchView.getText().toString())) {
                            Log.d("checkQuery", "fgfgd");
                        } else {
                            Log.d("checkQuery", "fgdgfdgdddd");
                            db1 = database.getReadableDatabase();
                            c1 = db1.rawQuery("SELECT * FROM " + Database.TABLE_NAME + " order by id ASC LIMIT 5", null);

                            if (c1.getCount() < 5) {
                                Log.d("checkQueryc1", String.valueOf(values));
                                SQLITEDATABASE.insert(Database.TABLE_NAME, null, values);
                            } else {
                                if (c1.moveToFirst()) {
                                    String rowId = c1.getString(c1.getColumnIndex(Database.KEY_ID));

                                    db1.delete(Database.TABLE_NAME, Database.KEY_ID + "=?", new String[]{rowId});
                                }

                                SQLITEDATABASE.insert(Database.TABLE_NAME, null, values);
                                //db.close();
                            }
                            //stringArrayList.clear();
                            //latestData();

                            SQLITEDATABASE.close();
                        }

                        //  SELECT * FROM (SELECT * FROM employees ORDER BY employee_id DESC LIMIT 10) ORDER BY employee_id ASC;
                        //String ALTER_TBL ="delete from " + Database.TABLE_NAME + " where "+Database.KEY_ID+" in (select "+ Database.KEY_ID +" from "+ Database.TABLE_NAME+" order by _id LIMIT 3);";
                        // +" ORDER BY(" + Database.KEY_ID + ") DESC"
                        //  SQLiteDatabase database = getReadableDatabase()

                        selected_date = "";
                        selected_type = "";
                        selected_categories = "";
                        selected_group = "";
                        status = "";
                        status_message = "";
                        simpleSearchView.setCursorVisible(false);
                        new DownloadJSON().execute();

                        //open the keyboard focused in the edtSearch
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(simpleSearchView.getWindowToken(), 0);
                        return false;
                    }
                }
                return false;

            }
        });

        simpleSearchView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if ((event != null) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    // TODO do something
                    Search_listview.setVisibility(View.GONE);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(simpleSearchView.getWindowToken(), 0);
                    handled = true;
                }

                return handled;
            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tickicon.setVisibility(view.GONE);
                tickicon1.setVisibility(view.GONE);
                tickicon2.setVisibility(view.GONE);
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selected_date = "";
                selected_type = "";
                selected_categories = "";
                status = "";
                status_message = "";
                simpleSearchView.setCursorVisible(false);
                Search_listview.setVisibility(View.GONE);
                new DownloadJSON().execute();

                //open the keyboard focused in the edtSearch
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(simpleSearchView.getWindowToken(), 0);
            }
        });

        Search_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("stringarraylist", stringArrayList.get(position));


                selected_date = "";
                selected_type = "";
                selected_categories = "";
                status = "";
                status_message = "";
                selected_search_data = stringArrayList.get(position);
                bundle5 = new Bundle();
                bundle5.putString(selected_search_data," " );
                mFirebaseAnalytics.logEvent(SEARCHEDINFIND, bundle5);
                mFirebaseAnalytics.setUserProperty(SEARCHEDINFIND, selected_search_data);
                Search_listview.setVisibility(View.GONE);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(simpleSearchView.getWindowToken(), 0);
                new DownloadJSON().execute();

            }
        });
    }

    public void latestData() {
        try {
            Database dbHelper = new Database(this.getApplicationContext());
            db1 = dbHelper.getReadableDatabase();
            c = db1.rawQuery("SELECT * FROM " + Database.TABLE_NAME + " order by id DESC LIMIT 5", null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        val = c.getString(c.getColumnIndex(Database.Product_NAME));
                        stringArrayList.add(val);
                    } while (c.moveToNext());
                }
            }
        } catch (SQLiteException se) {
            Log.e(getClass().getSimpleName(), "Could not create or Open the database");
        }
        /*finally {
            if (SQLITEDATABASE != null)
                SQLITEDATABASE.execSQL("DELETE FROM " + Database.TABLE_NAME+" where id='"+5+"'");
            SQLITEDATABASE.close();
        }*/


        Log.d("stringArraylist", String.valueOf(stringArrayList));
        searchListviewAdapter = new SearchListviewAdapter(getApplicationContext(), stringArrayList);
        Search_listview.setAdapter(searchListviewAdapter);
        searchListviewAdapter.notifyDataSetChanged();
        c.close();
        db1.close();
    }

    public boolean dataExists(String name) {
        db = database.getReadableDatabase();
        Cursor cursor = null;
        String checkQuery = "SELECT * FROM " + Database.TABLE_NAME + " WHERE " + Database.Product_NAME + "='" + name + "'";
        Log.d("checkQuery", checkQuery);
        cursor = db.rawQuery(checkQuery, null);
        // cursor.moveToFirst();
        boolean exists = (cursor.getCount() > 0);
        cursor.close();
        return exists;
    }

    private void refinedate() {
        dialogRefineDate = new Dialog(Events.this);
        dialogRefineDate.setCanceledOnTouchOutside(true);
        dialogRefineDate.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogRefineDate.setContentView(R.layout.selectdates);

        listView2 = (ListView) dialogRefineDate.findViewById(R.id.sampledatelist);


        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat def = new SimpleDateFormat("dd MMM yyyy");
        String formattedDate1 = def.format(c);
        System.out.println("Current time => " + formattedDate1);


        Calendar cal = Calendar.getInstance(); //Get the Calendar instance
        cal.add(Calendar.MONTH, 3);//Three months from now
        cal.getTime();// Get the Date object

        Log.d("next 3 monthsasdvcsd", String.valueOf(cal.getTime()));

        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
        String formattedDate2 = df.format(cal.getTime());

        Log.d("next 3 months", formattedDate2);

        formatter = new SimpleDateFormat("dd MMM yyyy");
        formatter1 = new SimpleDateFormat("dd MMM yyyy");


        final List<Date> dates = new ArrayList<Date>();
        Date startDate = null;
        Date endDate = null;
        try {
            startDate = (Date) formatter.parse(formattedDate1);
            endDate = (Date) formatter.parse(formattedDate2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (!formattedDate1.equals("")) {
            long interval = 24 * 1000 * 60 * 60; // 1 hour in millis
            long endTime = endDate.getTime(); // create your endtime here, possibly using Calendar or Date
            long curTime = startDate.getTime();
            while (curTime <= endTime) {
                dates.add(new Date(curTime));
                curTime += interval;
            }
            for (int i = 0; i < dates.size(); i++) {
                Date lDate = (Date) dates.get(i);
                String ds = formatter1.format(lDate);
                System.out.println(ds);
                date_array_list.add(ds);
            }
            Log.d("date_array_list", String.valueOf(date_array_list));
            date_adapter = new DateAdapter(getApplicationContext(), date_array_list);
            listView2.setAdapter(date_adapter);
            dialogRefineDate.show();

        }

        listView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                String formattedDate2 = df.format(dates.get(position));

                Log.d("selected", String.valueOf(formattedDate2));
                midLayout.setVisibility(View.GONE);
                dialogRefineDate.dismiss();

                 bundle4 = new Bundle();
                bundle4.putString(formattedDate2, " ");
                mFirebaseAnalytics.logEvent("Filter", bundle4);
                mFirebaseAnalytics.setUserProperty("Filter", formattedDate2);


                //  mFirebaseAnalytics.setUserProperty("select_date", formattedDate2);

                Intent intent = new Intent(getApplicationContext(), Events.class);
                intent.putExtra("selected_date", formattedDate2);
                intent.putExtra("selected_type", "date");
                startActivity(intent);
            }
        });


    }

    private void refinecategory() {
        dialogRefineType = new Dialog(Events.this);
        dialogRefineType.setCanceledOnTouchOutside(true);
        dialogRefineType.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // dialogRefineType.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        dialogRefineType.setContentView(R.layout.categories);

        // dates = (Button) dialogRefineType.findViewById(R.id.dates);
        cancel_icon = (Button) dialogRefineType.findViewById(R.id.button);
        listView1 = (ListView) dialogRefineType.findViewById(R.id.sampleListView);
        confirm_icon = (Button) dialogRefineType.findViewById(R.id.button1);

        stringArray = new ArrayList<String>();
        stringArray.add("Activities");
        stringArray.add("Alternatives");
        stringArray.add("Arts & Crafts");
        stringArray.add("Classes");
        stringArray.add("Comedy");
        stringArray.add("Drinks");
        stringArray.add("Food");
        stringArray.add("Live");
        stringArray.add("Musuems");
        stringArray.add("Sights");
        stringArray.add("Theatre");

        adapter1 = new CategoriesAdapter(getApplicationContext(), stringArray);
        listView1.setAdapter(adapter1);

        listView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                // Log.d("details ", stringArray.get(position));
                //Object o = parent.getItemAtPosition(position);
                Details.getBookingCount(stringArray.get(position));
                Details.checkBoxItemPosition(position);
                selected_categories = stringArray.get(position);

                // String str_text = (listView1.getItemAtPosition(position).toString());

                //Log.d("details ", str_text);
            }
        });


        onTypeListener();
        dialogRefineType.show();

    }

    private void location() {
        dialogRefineDate = new Dialog(Events.this);
        dialogRefineDate.setCanceledOnTouchOutside(false);
        dialogRefineDate.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogRefineDate.setContentView(R.layout.selectlocation);
        listView2 = (ListView) dialogRefineDate.findViewById(R.id.sampledatelistloaction);
        //header = (TextView) dialogRefineDate.findViewById(R.id.header);
        //relativeLayout = (RelativeLayout)dialogRefineDate.findViewById(R.id.real);

        // relativeLayout.getLayoutParams().height=1050;

        // header.setText("Select Location");

        Log.d("loaction_list", String.valueOf(loaction));
        date_adapter = new DateAdapter(getApplicationContext(), loaction);
        listView2.setAdapter(date_adapter);
        dialogRefineDate.show();


        listView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                midLayout.setVisibility(View.GONE);
                dialogRefineDate.dismiss();

                Intent intent = new Intent(getApplicationContext(), Events.class);
                intent.putExtra("selected_location", loaction.get(position));
                intent.putExtra("selected_type", "location");
                startActivity(intent);

               bundle = new Bundle();
                bundle.putString(loaction.get(position)," ");
                mFirebaseAnalytics.logEvent("Filter", bundle);
                mFirebaseAnalytics.setUserProperty("Filter", loaction.get(position));

                //mFirebaseAnalytics.setUserProperty("select_Location", loaction.get(position));
            }
        });
    }

    private void onTypeListener() {
        /* date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialogRefineType.dismiss();
                dialogRefineDate = new Dialog(Events.this);
                dialogRefineDate.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogRefineDate.setContentView(R.layout.refine_search2);
                dialogRefineDate.show();
          }
        });*/
        confirm_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                bundle.putString("Confirm", " ");
                mFirebaseAnalytics.logEvent("Filter", bundle);
                bundle3.putString(selected_categories, " ");
                mFirebaseAnalytics.logEvent("Filter",bundle3);
                mFirebaseAnalytics.setUserProperty("Filter", "Confirm");
                mFirebaseAnalytics.setUserProperty("Filter", selected_categories);

                //  mFirebaseAnalytics.setUserProperty("select_Confirm", selected_categories);

                midLayout.setVisibility(View.GONE);
                dialogRefineType.dismiss();
                Intent intent = new Intent(getApplicationContext(), Events.class);
                intent.putExtra("selected_categories", String.valueOf(selected_categories));
                intent.putExtra("selected_type", "categories");
                startActivity(intent);
            }
        });

        cancel_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selected_categories = String.valueOf(selected_categories);
                dialogRefineType.dismiss();
                refinedate();

            }
        });

      /*  apply_changes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogRefineType.dismiss();


            }
        });*/
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        if (dragging) {
            int toolbarHeight = search_layout.getHeight();
            if (firstScroll) {
                float currentHeaderTranslationY = ViewHelper.getTranslationY(headerID);
                if (-toolbarHeight < currentHeaderTranslationY) {
                    mBaseTranslationY = scrollY;
                }
            }
            float headerTranslationY = ScrollUtils.getFloat(-(scrollY - mBaseTranslationY), -toolbarHeight, 0);
            ViewPropertyAnimator.animate(headerID).cancel();
            ViewHelper.setTranslationY(headerID, headerTranslationY);
        }
    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
        mBaseTranslationY = 0;

        if (scrollState == ScrollState.DOWN) {
            showToolbar();
        } else if (scrollState == ScrollState.UP) {
            int toolbarHeight = search_layout.getHeight();
            int scrollY = observeList_view.getCurrentScrollY();
            if (toolbarHeight <= scrollY) {
                hideToolbar();
            } else {
                showToolbar();
            }
        } else {
            // Even if onScrollChanged occurs without scrollY changing, toolbar should be adjusted
            if (!toolbarIsShown() && !toolbarIsHidden()) {
                // Toolbar is moving but doesn't know which to move:
                // you can change this to hideToolbar()
                showToolbar();
            }
        }
    }

    private boolean toolbarIsShown() {
        return ViewHelper.getTranslationY(headerID) == 0;
    }

    private boolean toolbarIsHidden() {
        return ViewHelper.getTranslationY(headerID) == -search_layout.getHeight();
    }

    private void showToolbar() {
        float headerTranslationY = ViewHelper.getTranslationY(headerID);
        if (headerTranslationY != 0) {
            ViewPropertyAnimator.animate(headerID).cancel();
            ViewPropertyAnimator.animate(headerID).translationY(0).setDuration(200).start();
        }
    }

    private void hideToolbar() {
        float headerTranslationY = ViewHelper.getTranslationY(headerID);
        int toolbarHeight = search_layout.getHeight();
        if (headerTranslationY != -toolbarHeight) {
            ViewPropertyAnimator.animate(headerID).cancel();
            ViewPropertyAnimator.animate(headerID).translationY(-toolbarHeight).setDuration(200).start();
        }
    }

    public void progressBar() {
        progress = new ProgressDialog(this);
        progress.setMessage("Loading...");
        progress.setIndeterminate(true);
        progress.setCanceledOnTouchOutside(false);
        progress.show();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(), Home.class));

    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();

        if (dialogRefineDate == null) {

        } else if (dialogRefineType != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
            dialogRefineType.dismiss();
        } else {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
            dialogRefineDate.dismiss();
//            dialogRefineType.dismiss();

        }

    }

    public class SavedJSON extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // progressBar();
        }

        @Override
        protected Void doInBackground(Void... params) {
            OkHttpClient client = new OkHttpClient();
            MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
            RequestBody body = RequestBody.create(mediaType, "listing_id=" + pos + "&user_id=" + sharedPreference_main.getUser_Id().toString());
            Request request = new Request.Builder()
                    .url(Home.MAIN_APPLICATION_URL + "listing/update-favorites")
                    .post(body)
                    .addHeader("content-type", "application/x-www-form-urlencoded")
                    .addHeader("accept", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .build();
            try {
                Response response = client.newCall(request).execute();
                Log.d("responstr", String.valueOf(response.code()));
                String fulljson = response.body().string();
                Log.d("body", fulljson);
                JSONArray jsonArray = new JSONArray(fulljson);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    massage = jsonObject.getString("message");
                    Log.d("massage", massage);
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            //new DownloadJSON().execute();
        }
    }

    // DownloadJSON AsyncTask
    private class DownloadJSON extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            arraylist.clear();
            progressBar();
        }

        @Override
        protected Void doInBackground(Void... params) {

            OkHttpClient client = new OkHttpClient();
            MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
            RequestBody body;
            Log.d("user_id", sharedPreference_main.getUser_Id().toString());
            if (!selected_date.equals("")) {
                body = RequestBody.create(mediaType, "user_id=" + sharedPreference_main.getUser_Id().toString() + "&expiry_date=" + selected_date);
            } else if (!selected_categories.equals("")) {
                body = RequestBody.create(mediaType, "user_id=" + sharedPreference_main.getUser_Id().toString() + "&categories=" + selected_categories.replace("[", "").replace("]", ""));
            } else if (!selected_group.equals("")) {
                body = RequestBody.create(mediaType, "user_id=" + sharedPreference_main.getUser_Id().toString() + "&groups=" + selected_group.replace("[", "").replace("]", ""));
            } else if (!selected_categories.equals("") && !selected_date.equals("")) {
                body = RequestBody.create(mediaType, "user_id=" + sharedPreference_main.getUser_Id().toString() + "&categories=" + selected_categories.replace("[", "").replace("]", "") + "&expiry_date=" + selected_date);
            } else if (!selected_search_data.equals("")) {
                body = RequestBody.create(mediaType, "user_id=" + sharedPreference_main.getUser_Id().toString() + "&search=" + selected_search_data);
            } else if (!selected_location.equals("")) {
                body = RequestBody.create(mediaType, "user_id=" + sharedPreference_main.getUser_Id().toString() + "&location=" + selected_location);
            } else {
                body = RequestBody.create(mediaType, "user_id=" + sharedPreference_main.getUser_Id().toString());
            }


            //Log.d("body", String.valueOf(body));
            Request request = new Request.Builder()
                    .url(Home.MAIN_APPLICATION_URL + "listing")
                    .post(body)
                    .addHeader("content-type", "application/x-www-form-urlencoded")
                    .addHeader("accept", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .build();
            try {
                Response response = client.newCall(request).execute();
                Log.d("responstr", String.valueOf(response.code()));
                String fulljson = response.body().string();
                Log.d("body", fulljson);

                JSONArray jsonArray = new JSONArray(fulljson);
                for (int i = 0; i < jsonArray.length(); i++) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    if (jsonObject.has("status")) {
                        status = jsonObject.getString("status");
                        status_message = jsonObject.getString("message");
                    } else {

                        if (jsonObject.has(EVENT_NAME)) {
                            map.put(EVENT_NAME, jsonObject.getString(EVENT_NAME));
                        } else {
                            map.put(EVENT_NAME, "");
                        }

                        // map.put(EVENT_NAME, jsonObject.getString(EVENT_NAME));
                        if (jsonObject.has(COMPANY_WEBSITE)) {
                            map.put(COMPANY_WEBSITE, jsonObject.getString(COMPANY_WEBSITE));
                        } else {
                            map.put(COMPANY_WEBSITE, "");
                        }
                        //  map.put(COMPANY_WEBSITE, jsonObject.getString(COMPANY_WEBSITE));
                        if (jsonObject.has(COMPANY_TAGLINE)) {
                            map.put(COMPANY_TAGLINE, jsonObject.getString(COMPANY_TAGLINE));
                        } else {
                            map.put(COMPANY_TAGLINE, "");
                        }
                        //  map.put(COMPANY_TAGLINE, jsonObject.getString(COMPANY_TAGLINE));

                        JSONObject jsonobject3 = jsonObject.getJSONObject(geolocation_data);
                        if (jsonobject3.has(LOCATION_ADDRESS)) {
                            map.put(LOCATION_ADDRESS, jsonobject3.getString(LOCATION_ADDRESS));
                        } else {
                            map.put(LOCATION_ADDRESS, "");
                        }
                        if (jsonobject3.has(LOCATION_STREET)) {
                            map.put(LOCATION_STREET, jsonobject3.getString(LOCATION_STREET));
                        } else {
                            map.put(LOCATION_STREET, "");
                        }

                        if (jsonobject3.has(LOCATION_POSTCODE)) {
                            map.put(LOCATION_POSTCODE, jsonobject3.getString(LOCATION_POSTCODE));
                        } else {
                            map.put(LOCATION_POSTCODE, "");
                        }

                        if (jsonobject3.has(LOCATION_CITY)) {
                            map.put(LOCATION_CITY, jsonobject3.getString(LOCATION_CITY));
                        } else {
                            map.put(LOCATION_CITY, "");
                        }

                        /*if (jsonobject3.has(LOCATION_LATITUDE)) {
                            map.put(LOCATION_LATITUDE, jsonobject3.getString(LOCATION_LATITUDE));
                        } else {
                            map.put(LOCATION_LATITUDE, "");
                        }

                        if (jsonobject3.has(LOCATION_LONGITUDE)) {
                            map.put(LOCATION_LATITUDE, jsonobject3.getString(LOCATION_LONGITUDE));
                        } else {
                            map.put(LOCATION_LONGITUDE, "");
                        }*/
                        // map.put(LOCATION_POSTCODE, jsonobject3.getString(LOCATION_POSTCODE));
                        if (jsonobject3.has(LOCATION_LATITUDE)) {
                            map.put(LOCATION_LATITUDE, jsonobject3.getString(LOCATION_LATITUDE));
                        } else {
                            map.put(LOCATION_LATITUDE, "");
                        }

                        //  map.put(LOCATION_LATITUDE, jsonobject3.getString(LOCATION_LATITUDE));
                        if (jsonobject3.has(LOCATION_LONGITUDE)) {
                            map.put(LOCATION_LONGITUDE, jsonobject3.getString(LOCATION_LONGITUDE));
                        } else {
                            map.put(LOCATION_LONGITUDE, "");
                        }
                        // map.put(LOCATION_LONGITUDE, jsonobject3.getString(LOCATION_LONGITUDE));
                        if (jsonObject.has(EVENT_IMAGE_URL)) {
                            map.put(EVENT_IMAGE_URL, jsonObject.getString(EVENT_IMAGE_URL));
                        } else {
                            map.put(EVENT_IMAGE_URL, "");
                        }
                        //  map.put(EVENT_IMAGE_URL, jsonObject.getString(EVENT_IMAGE_URL));
                        if (jsonObject.has(EVENT_ID)) {
                            map.put(EVENT_ID, jsonObject.getString(EVENT_ID));
                        } else {
                            map.put(EVENT_ID, "");
                        }
                        //  map.put(EVENT_ID, jsonObject.getString(EVENT_ID));
                        if (jsonObject.has(EVENT_LINK)) {
                            map.put(EVENT_LINK, jsonObject.getString(EVENT_LINK));
                        } else {
                            map.put(EVENT_LINK, "");
                        }
                        // map.put(EVENT_LINK, jsonObject.getString(EVENT_LINK));

                        if (jsonObject.has(EVENT_CONTENT)) {
                            map.put(EVENT_CONTENT, jsonObject.getString(EVENT_CONTENT));
                        } else {
                            map.put(EVENT_CONTENT, "");
                        }

                        // map.put(EVENT_CONTENT, jsonObject.getString(EVENT_CONTENT));
                        map.put(EVENT_OWNER, jsonObject.getString(EVENT_OWNER));
                        map.put(TERMS, jsonObject.getString(TERMS));
                        map.put(CUSTOM_FIELDS, jsonObject.getString(CUSTOM_FIELDS));

                        JSONObject jsonobject4 = jsonObject.getJSONObject(CUSTOM_FIELDS);
                        map.put(COST, jsonobject4.getString(COST));

                        map.put(COMMENTS, jsonObject.getString(COMMENTS));
                        map.put(IS_FAVORITE, jsonObject.getString(IS_FAVORITE));
                        map.put(PRODUCT, jsonObject.getString(PRODUCT));

                        JSONObject jsonObject5 = jsonObject.getJSONObject(MEDIA_GALLERY);
                        if (jsonObject5.has(IMAGES)) {
                            map.put(IMAGES, jsonObject5.getString(IMAGES));
                        } else {
                            map.put(IMAGES, "");
                        }

                        if (jsonObject5.has(VIDEO)) {
                            map.put(VIDEO, jsonObject5.getString(VIDEO));
                            Log.d("hdfhsdshd", jsonObject5.getString(VIDEO));
                        } else {
                            map.put(VIDEO, "");
                        }

                        arraylist.add(map);
                        Log.d("array_list", String.valueOf(arraylist));
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            progress.dismiss();
            if (status.equals("0")) {
                no_data.setVisibility(View.VISIBLE);
                observeList_view.setVisibility(View.GONE);
            } else {
                no_data.setVisibility(View.GONE);
                observeList_view.setVisibility(View.VISIBLE);

                if (adapter == null) {
                    adapter = new EventsAdapter(getApplicationContext(), arraylist,selected_categories);
                    observeList_view.setAdapter(adapter);
                } else {
                    adapter.notifyDataSetChanged();
                }


                observeList_view.setOnScrollListener(new AbsListView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(AbsListView view, int scrollState) {
                        // Log.v(TAG, "onScrollStateChanged: " + scrollState);
                    }

                    @Override
                    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                        //    Log.v(TAG, "onScroll: firstVisibleItem: " + firstVisibleItem + " visibleItemCount: " + visibleItemCount + " totalItemCount: " + totalItemCount);
                    }
                });

            }
        }
    }

}
