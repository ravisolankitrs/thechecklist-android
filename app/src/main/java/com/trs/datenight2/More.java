package com.trs.datenight2;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.firebase.analytics.FirebaseAnalytics;


public class More extends Activity {
    public static final String BOTTOMNAV = "Bottom_Nav";
    public static final String TERMSANDCONDITIONS = "Terms_and_Conditions";
    public static final String PRIVACYPOLICY = "Privacy_Policy";
    public static final String LOGOUT = "Log_out";
    SharedPreference_main sharedPreference_main;
    RelativeLayout top_layout2, top_layout3, top_layout5, top_layout_blog;
    View view;
    TextView title;
    Bundle bundle1,bundle2,bundle3,bundle4;
    TextView home_text, find_text, nearby_text, save_text, more_text;
    RelativeLayout home_lay, find_lay, nearby_lay, save_lay, more_lay;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.more);
        bundle1=new Bundle();
        bundle2=new Bundle();
        bundle3=new Bundle();
        bundle4=new Bundle();
        sharedPreference_main = new SharedPreference_main(getApplicationContext());
        init();
        onclick();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        Typeface hanken_light = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Hanken-Light.ttf");
        title.setTypeface(hanken_light);
        home_text.setTypeface(hanken_light);
        find_text.setTypeface(hanken_light);
        nearby_text.setTypeface(hanken_light);
        save_text.setTypeface(hanken_light);
        more_text.setTypeface(hanken_light);
    }


    private void init() {
        title = (TextView) findViewById(R.id.title);
        top_layout5 = (RelativeLayout) findViewById(R.id.top_layout5);
        top_layout2 = (RelativeLayout) findViewById(R.id.top_layout2);
        top_layout3 = (RelativeLayout) findViewById(R.id.top_layout3);
        top_layout_blog = (RelativeLayout) findViewById(R.id.top_layout_blog);

        home_text = (TextView) findViewById(R.id.home_text);
        find_text = (TextView) findViewById(R.id.find_text);
        nearby_text = (TextView) findViewById(R.id.nearby_text);
        save_text = (TextView) findViewById(R.id.save_text);
        more_text = (TextView) findViewById(R.id.more_text);

        home_lay = (RelativeLayout) findViewById(R.id.home_lay);
        find_lay = (RelativeLayout) findViewById(R.id.find_lay);
        nearby_lay = (RelativeLayout) findViewById(R.id.nearby_lay);
        save_lay = (RelativeLayout) findViewById(R.id.save_lay);
        more_lay = (RelativeLayout) findViewById(R.id.more_lay);

    }

    private void onclick() {

        home_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle1.putString("Home", " ");
                mFirebaseAnalytics.logEvent(BOTTOMNAV,bundle1);
                mFirebaseAnalytics.setUserProperty(BOTTOMNAV, "Home");
                startActivity(new Intent(getApplicationContext(), Home.class));
            }
        });

        find_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle1.putString("The_List", " ");
                mFirebaseAnalytics.logEvent(BOTTOMNAV,bundle1);
                mFirebaseAnalytics.setUserProperty(BOTTOMNAV, "The_List");
                startActivity(new Intent(getApplicationContext(), Events.class));
            }
        });


        nearby_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle1.putString("NearBy", " ");
                mFirebaseAnalytics.logEvent(BOTTOMNAV,bundle1);
                mFirebaseAnalytics.setUserProperty(BOTTOMNAV, "NearBy");
                startActivity(new Intent(getApplicationContext(), Nearby.class));
            }
        });

        save_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle1.putString("Save", " ");
                mFirebaseAnalytics.logEvent(BOTTOMNAV,bundle1);
                mFirebaseAnalytics.setUserProperty(BOTTOMNAV, "Save");
                // mFirebaseAnalytics.setUserProperty("select_logout", "logout");
                startActivity(new Intent(getApplicationContext(), SavedDate.class));

            }
        });

        more_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        top_layout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle2.putString("Terms_and_Conditions", " ");
                mFirebaseAnalytics.logEvent("Terms_and_Conditions",bundle2);
                mFirebaseAnalytics.setUserProperty("Terms_and_Conditions", " ");
                Intent _intent = new Intent(getApplicationContext(), TermsAndConditions.class);
                startActivity(_intent);
            }
        });
        top_layout3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle3.putString("Privacy_Policy", " ");
                mFirebaseAnalytics.logEvent("Privacy_Policy",bundle3);
                mFirebaseAnalytics.setUserProperty("Privacy_Policy", " ");
                Intent _intent = new Intent(getApplicationContext(), PrivacyPolicy.class);
                startActivity(_intent);
            }
        });
        top_layout5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle4.putString("Logged_Out", " ");
                mFirebaseAnalytics.logEvent(LOGOUT,bundle4);
                mFirebaseAnalytics.setUserProperty("Log_out", " ");
                sharedPreference_main.logoutUser();
                Log.d("socialtrue", "social");
                LoginManager.getInstance().logOut();
                AccessToken.setCurrentAccessToken(null);
                Intent _intent = new Intent(getApplicationContext(), Login.class);
                startActivity(_intent);
            }
        });

        top_layout_blog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), HintsAndTips.class));
            }
        });
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(), Home.class));
    }
}
