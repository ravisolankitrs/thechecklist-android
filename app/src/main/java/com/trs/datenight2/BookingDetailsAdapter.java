package com.trs.datenight2;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

public class BookingDetailsAdapter extends RecyclerView.Adapter<BookingDetailsAdapter.CustomViewHolder> {

    public static TextView date_popup;
    ArrayList<HashMap<String, String>> data;
    HashMap<String, String> resultp = new HashMap<String, String>();
    Typeface openSans_Light, hanken_Book, hanken_Light;
    OnItemClickListener clickListener;
    private Context context;
    private LayoutInflater layoutInflater;


    // Provide a suitable constructor (depends on the kind of dataset)
    public BookingDetailsAdapter(Context context, ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        this.data = arraylist;
        this.layoutInflater = LayoutInflater.from(context);

    }

    public void setClickListener(OnItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.booking_details_adapter, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int position) {
        resultp = data.get(position);

        openSans_Light = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Light.ttf");
        hanken_Book = Typeface.createFromAsset(context.getAssets(), "fonts/Hanken-Book.ttf");
        Typeface openSans_Italic = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Italic.ttf");
        hanken_Light = Typeface.createFromAsset(context.getAssets(), "fonts/Hanken-Light.ttf");
        Typeface openSans_Regular = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");

        holder.booking_title.setTypeface(hanken_Book);
        holder.date_with_day.setTypeface(openSans_Italic);
        holder.booking_details.setTypeface(openSans_Light);
        holder.ticket_price.setTypeface(hanken_Book);
        holder.book_now.setTypeface(hanken_Book);

        holder.booking_details.setText(resultp.get("description"));
        holder.booking_title.setText(resultp.get("name"));
        holder.ticket_price.setText("£" + resultp.get("price"));
        if (!resultp.get("date").equals(" ")) {
            holder.date_with_day.setText("On " + resultp.get("date"));
        } else {
            holder.date_with_day.setText("Available");
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return data.size();
    }

    class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        Button book_now;
        TextView booking_title, date_with_day, booking_details, ticket_price;

        public CustomViewHolder(View itemView) {
            super(itemView);
            booking_title = itemView.findViewById(R.id.booking_title);
            date_with_day = itemView.findViewById(R.id.date_with_day);
            booking_details = itemView.findViewById(R.id.booking_details);
            ticket_price = itemView.findViewById(R.id.ticket_price);
            book_now = itemView.findViewById(R.id.book_now);
            book_now.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null)
                clickListener.onClick(v, getAdapterPosition());
            resultp = data.get(getPosition());
        }
    }
}

