package com.trs.datenight2;

import android.app.Activity;
import android.content.Intent;

import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ImageView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class RefineSearch2 extends Activity {

    Button type;
    ImageView cancel_icon;
    Button apply_changes;
    CalendarView simpleCalendarView;

    String selected_date;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.refine_search2);
        init();
        onclick();

        long selectedDate = simpleCalendarView.getDate();
        Log.d("selectedDate", String.valueOf(selectedDate));
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        selected_date =  df.format(c);
        System.out.println("Current date => " + selected_date);
    }

    private void init() {
        type = (Button) findViewById(R.id.type);
        cancel_icon = (ImageView) findViewById(R.id.cancel_icon);
        apply_changes = (Button) findViewById(R.id.apply_changes);
        simpleCalendarView = (CalendarView) findViewById(R.id.simpleCalendarView); // get the reference of CalendarView

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    private void onclick() {

        type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), RefineSearch.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            }
        });

        cancel_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent intent = new Intent(getApplicationContext(), Events.class);
                startActivity(intent);

                /*android.support.v4.app.Fragment fragment2 = new Events();
                android.app.FragmentManager fragmentManager = getFragmentManager();
                android.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_layout, fragment2);
                fragmentTransaction.commit();*/

                /*setContentView(R.layout.activity_main);
                Events selectedFragment = Events.newInstance();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, selectedFragment);
                transaction.commit();*/
            }
        });

        apply_changes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                 Intent intent = new Intent(getApplicationContext(), Events.class);
                 intent.putExtra("selected_date", selected_date);
                 intent.putExtra("selected_type", "date");
                 startActivity(intent);


              /*  setContentView(R.layout.activity_main);
                Bundle bundle = new Bundle();
                bundle.putString("selected_date", selected_date);
                bundle.putString("selected_type", "date");

                Events myObj = new Events();
                myObj.setArguments(bundle);

                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, myObj);
                transaction.commit();*/
            }
        });


        simpleCalendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                // TODO Auto-generated method stub
                //selected_date = year + "-" + (month + 1) + "-" + dayOfMonth;
                selected_date = year + "-" + String.format("%02d", month + 1) + "-" + dayOfMonth;
                Log.d("Date is : ", selected_date);
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), Events.class);
        startActivity(intent);
       /* setContentView(R.layout.activity_main);
        Events selectedFragment = Events.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, selectedFragment);
        transaction.commit();*/
    }
}
