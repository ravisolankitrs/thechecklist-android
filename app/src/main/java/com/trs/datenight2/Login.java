package com.trs.datenight2;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;


public class Login extends Activity {

    private CallbackManager callbackManager;
    LoginButton facebook_button;
    TextView register_with_email, sing_with_email;
    SharedPreference_main sharedPreference_main;
    String user_id,user_name;
    private FirebaseAnalytics mFirebaseAnalytics;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.login);
        sharedPreference_main = new SharedPreference_main(this);
        init();
        onclick();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        Typeface hanken_Book = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Hanken-Book.ttf");
        Typeface hanken_Light = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Hanken-Light.ttf");
        Typeface openSans_Bold = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-Bold.ttf");
        Typeface openSans_BoldItalic = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-BoldItalic.ttf");
        Typeface openSans_ExtraBold = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-ExtraBold.ttf");
        Typeface openSans_ExtraBoldItalic = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-ExtraBoldItalic.ttf");
        Typeface openSans_Italic= Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-Italic.ttf");
        Typeface openSans_Light = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-Light.ttf");
        Typeface openSans_LightItalic = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-LightItalic.ttf");
        Typeface openSans_Regular = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-Regular.ttf");
        Typeface openSans_Semibold= Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-Semibold.ttf");
        Typeface openSans_SemiboldItalic = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-SemiboldItalic.ttf");
        register_with_email.setTypeface(openSans_Light);
        sing_with_email.setTypeface(openSans_Light);

    }


    private void init() {
        facebook_button = (LoginButton) findViewById(R.id.facebook_button);
        register_with_email = (TextView) findViewById(R.id.register_with_email);
        sing_with_email = (TextView) findViewById(R.id.sing_with_email);

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    private void onclick() {
        register_with_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), RegisterWithEmail.class);
                startActivity(intent);
            }
        });
        sing_with_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SignWithEmail.class);
                startActivity(intent);
            }
        });

        callbackManager = CallbackManager.Factory.create();
        facebook_button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("Response_Data from fb", response.toString());
                                // Application code
                                try {
                                    String fulljson = response.toString();
                                    Log.d("body", fulljson);
                                    JSONObject jsonObject = new JSONObject(fulljson);
                                    String response_code = jsonObject.getString("responseCode");
                                    Log.d("response_code", response_code);
                                    JSONObject jsonObject2 = jsonObject.getJSONObject("graphObject");
                                    user_id = jsonObject2.getString("id");
                                    user_name = jsonObject2.getString("name");

                                    sharedPreference_main.setUser_Name(user_name);
                                    Log.d("hjdfhsdfhsk",user_name);
                                    Log.d("hjdfhsdfhsk1",user_id);

                                    Bundle bundle = new Bundle();
                                    bundle.putString("facebook", user_id);
                                    mFirebaseAnalytics.logEvent("Signin_Register", bundle);
                                    mFirebaseAnalytics.setUserProperty("Signin_Register","Facebook");
                                   // mFirebaseAnalytics.setUserProperty("Facebook_id", user_id);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
                Log.v("LoginActivity", "cancel");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Toast.makeText(getApplicationContext(), exception.getMessage().toString(), Toast.LENGTH_SHORT).show();
                Log.v("LoginActivity", exception.getMessage().toString());
                Log.v("LoginActivity", "error");
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.d("asfhjsafhsdf", String.valueOf(requestCode) + "  " + String.valueOf(resultCode));
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (resultCode == 0) {

        } else {
            sharedPreference_main.setLogin("successful");
            //sharedPreference_main.setUser_Id("519710221721406");
            sharedPreference_main.setUser_Id("296");
            Toast.makeText(getApplicationContext(), "Successfully login.", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(getApplicationContext(), Home.class);
            startActivity(intent);
        }
    }

}
