package com.trs.datenight2;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Sandeep on 1/22/2018.
 */
public class RefineSearch extends Activity {

    RelativeLayout drinks,food,activities,theatre,sports,comedy,music,arts,sights,exclusive,romantic,alternatives;
    Button date;
    ImageView cancel_icon;

    ArrayList cat =  new ArrayList();
    Button apply_changes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.refine_search);
        init();
        onclick();
    }

    private void init(){
        date =  (Button) findViewById (R.id.date);
        cancel_icon = (ImageView) findViewById (R.id.cancel_icon);

        drinks =  (RelativeLayout) findViewById (R.id.drinks);
        food =  (RelativeLayout) findViewById(R.id.food);
        activities =  (RelativeLayout) findViewById(R.id.activities);
        theatre =  (RelativeLayout) findViewById(R.id.theatre);
        sports =  (RelativeLayout) findViewById(R.id.sports);
        comedy =  (RelativeLayout) findViewById(R.id.comedy);
        music =  (RelativeLayout) findViewById(R.id.music);
        arts =  (RelativeLayout) findViewById(R.id.arts);
        sights =  (RelativeLayout) findViewById(R.id.sights);
        exclusive =  (RelativeLayout) findViewById(R.id.exclusive);
        romantic =  (RelativeLayout) findViewById(R.id.romantic);
        alternatives =  (RelativeLayout) findViewById(R.id.alternatives);

        apply_changes = (Button) findViewById(R.id.apply_changes);


        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }
    }
    private void onclick(){

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), RefineSearch2.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            }
        });


        cancel_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Events.class);
                startActivity(intent);

            }
        });


        drinks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ColorDrawable corItem = (ColorDrawable) drinks.getBackground();
                if(corItem.getColor() == Color.parseColor("#373853")){
                    drinks.setBackgroundColor(Color.parseColor("#6C6D80"));
                    cat.remove("drinks");
                }else{
                    drinks.setBackgroundColor(Color.parseColor("#373853"));
                    cat.add("drinks");
                }
                Log.d("categories", String.valueOf(cat));
            }
        });

        food.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ColorDrawable corItem = (ColorDrawable) food.getBackground();
                if(corItem.getColor() == Color.parseColor("#373853")){
                    food.setBackgroundColor(Color.parseColor("#6C6D80"));
                    cat.remove("food");
                }else{
                    food.setBackgroundColor(Color.parseColor("#373853"));
                    cat.add("food");
                }
                Log.d("categories", String.valueOf(cat));
            }
        });

        activities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ColorDrawable corItem = (ColorDrawable) activities.getBackground();
                if(corItem.getColor() == Color.parseColor("#373853")){
                    activities.setBackgroundColor(Color.parseColor("#6C6D80"));
                    cat.remove("activities");
                }else{
                    activities.setBackgroundColor(Color.parseColor("#373853"));
                    cat.add("activities");
                }
                Log.d("categories", String.valueOf(cat));
            }
        });

        theatre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ColorDrawable corItem = (ColorDrawable) theatre.getBackground();
                if(corItem.getColor() == Color.parseColor("#373853")){
                    theatre.setBackgroundColor(Color.parseColor("#6C6D80"));
                    cat.remove("theatre");
                }else{
                    theatre.setBackgroundColor(Color.parseColor("#373853"));
                    cat.add("theatre");
                }
                Log.d("categories", String.valueOf(cat));
            }
        });

        sports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ColorDrawable corItem = (ColorDrawable) sports.getBackground();
                if(corItem.getColor() == Color.parseColor("#373853")){
                    sports.setBackgroundColor(Color.parseColor("#6C6D80"));
                    cat.remove("sports");
                }else{
                    sports.setBackgroundColor(Color.parseColor("#373853"));
                    cat.add("sports");
                }
                Log.d("categories", String.valueOf(cat));
            }
        });

        comedy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ColorDrawable corItem = (ColorDrawable) comedy.getBackground();
                if(corItem.getColor() == Color.parseColor("#373853")){
                    comedy.setBackgroundColor(Color.parseColor("#6C6D80"));
                    cat.remove("comedy");
                }else{
                    comedy.setBackgroundColor(Color.parseColor("#373853"));
                    cat.add("comedy");
                }
                Log.d("categories", String.valueOf(cat));
            }
        });

        music.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ColorDrawable corItem = (ColorDrawable) music.getBackground();
                if(corItem.getColor() == Color.parseColor("#373853")){
                    music.setBackgroundColor(Color.parseColor("#6C6D80"));
                    cat.remove("music");
                }else{
                    music.setBackgroundColor(Color.parseColor("#373853"));
                    cat.add("music");
                }
                Log.d("categories", String.valueOf(cat));
            }
        });

        arts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ColorDrawable corItem = (ColorDrawable) arts.getBackground();
                if(corItem.getColor() == Color.parseColor("#373853")){
                    arts.setBackgroundColor(Color.parseColor("#6C6D80"));
                    cat.remove("arts-crafts");
                }else{
                    arts.setBackgroundColor(Color.parseColor("#373853"));
                    cat.add("arts-crafts");
                }
                Log.d("categories", String.valueOf(cat));
            }
        });

        sights.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ColorDrawable corItem = (ColorDrawable) sights.getBackground();
                if(corItem.getColor() == Color.parseColor("#373853")){
                    sights.setBackgroundColor(Color.parseColor("#6C6D80"));
                    cat.remove("sights");
                }else{
                    sights.setBackgroundColor(Color.parseColor("#373853"));
                    cat.add("sights");
                }
                Log.d("categories", String.valueOf(cat));
            }
        });

        exclusive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ColorDrawable corItem = (ColorDrawable) exclusive.getBackground();
                if(corItem.getColor() == Color.parseColor("#373853")){
                    exclusive.setBackgroundColor(Color.parseColor("#6C6D80"));
                    cat.remove("exclusive");
                }else{
                    exclusive.setBackgroundColor(Color.parseColor("#373853"));
                    cat.add("exclusive");
                }
                Log.d("categories", String.valueOf(cat));
            }
        });

        romantic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ColorDrawable corItem = (ColorDrawable) romantic.getBackground();
                if(corItem.getColor() == Color.parseColor("#373853")){
                    romantic.setBackgroundColor(Color.parseColor("#6C6D80"));
                    cat.remove("romantic");
                }else{
                    romantic.setBackgroundColor(Color.parseColor("#373853"));
                    cat.add("romantic");
                }
                Log.d("categories", String.valueOf(cat));
            }
        });

        alternatives.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ColorDrawable corItem = (ColorDrawable) alternatives.getBackground();
                if(corItem.getColor() == Color.parseColor("#373853")){
                    alternatives.setBackgroundColor(Color.parseColor("#6C6D80"));
                    cat.remove("alternatives");
                }else{
                    alternatives.setBackgroundColor(Color.parseColor("#373853"));
                    cat.add("alternatives");
                }
                Log.d("categories", String.valueOf(cat));
            }
        });


        apply_changes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Events.class);
                intent.putExtra("selected_categories", String.valueOf(cat));
                intent.putExtra("selected_type", "categories");
                startActivity(intent);

            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), Events.class);
        startActivity(intent);
    }
}
